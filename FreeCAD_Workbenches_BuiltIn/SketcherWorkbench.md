# FreeCAD: ![](SketcherWorkbench_files/img_h16/Workbench_Sketcher_h16.png) [Sketcher Workbench][t]
[t]:https://wiki.freecad.org/Sketcher_Workbench

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

**FreeCAD ![](SketcherWorkbench_files/img_h16/Workbench_Sketcher_h16.png) Sketcher Workbench**

* creates 2D geometries for use in the ![](SketcherWorkbench_files/img_h16/Workbench_PartDesign_h16.png) [PartDesign Workbench](https://wiki.freecad.org/PartDesign_Workbench), ![](SketcherWorkbench_files/img_h16/Workbench_Arch_h16.png) [Arch Workbench](https://wiki.freecad.org/Arch_Workbench), and other workbenches. 
* typically, a 2D drawing is used as the starting point for CAD models
* 2D sketch can be "extruded" to create a 3D shape
* 2D sketches can be used to create attached features like pockets, ridges, or extrusions
* Together with ![](SketcherWorkbench_files/img_h16/Workbench_Part_h16.png) [Part Workbench](https://wiki.freecad.org/Part_Workbench) Boolean operations, the Sketcher forms the basis of the [constructive solid geometry](https://wiki.freecad.org/Constructive_solid_geometry) (CSG) method of building solids. 
* Together with the ![](SketcherWorkbench_files/img_h16/Workbench_PartDesign_h16.png) [PartDesign Workbench](https://wiki.freecad.org/PartDesign_Workbench) operations, the Sketcher also forms the basis of the [feature editing](https://wiki.freecad.org/Feature_editing) methodology of creating solids.
* provides "constraints" to allow precise geometrical definitions in terms of length, angles, and relationships (horizontality, verticality, perpendicularity, etc.).
* provides a constraint solver which 
    * calculates the constrained-extent of 2D geometry
    * allows interactive exploration of degrees-of-freedom of the sketch.
* not intended for 2D blueprints

> Note: A sketch on a data plane, instead of a sketch on a face, can avoid the internal face re-name issue.

![](SketcherWorkbench_files/450px-FC_ConstrainedSketch.png)

A fully constrained sketch highlighted as green.

## Basics of constraint sketching

Objects can be drawn loosely and be modified while unconstrained. Uncontrained objects are in effect "floating" and can be moved, stretched, rotated, scaled, and so on.

Constraints are used to limit the degrees of freedom of an object. For example, a line without constraints has 4 Degrees Of Freedom (abbreviated as): it can be moved horizontally or vertically, it can be stretched, and it can be rotated.

Applying a horizontal or vertical constraint, or an angle constraint (relative to another line or to one of the axes), will limit its capacity to rotate, thus leaving it with 3 degrees of freedom. Locking one of its points in relation to the origin will remove another 2 degrees of freedom. And applying a dimension constraint will remove the last degree of freedom. The line is then considered **fully-constrained**.

Multiple objects can be constrained between one another. Two lines can be joined through one of their points with the coincident point constraint. An angle can be set between them, or they can be set perpendicular. A line can be tangent to an arc or a circle, and so on. A complex Sketch with multiple objects will have a number of different solutions, and making it **fully-constrained** means that just one of these possible solutions has been reached based on the applied constraints.

There are two kinds of constraints ['Tools'](#Tools) 
    * geometric
    * dimensional
    
#### Sketcher vs Draft

* Once sketches are used to generate a solid feature, they are automatically hidden.
* Constraints are only visible in Sketch edit mode.

If you only need to produce 2D views for print, and don't want to create 3D models, check out the [Draft workbench](https://wiki.freecad.org/Draft_Workbench). Draft objects don't use constraints; they are simple shapes defined at the moment of creation. Both Draft and Sketcher can be used for 2D geometry drawing, and 3D solid creation, although their preferred use is different; the Sketcher is normally used together with [Part](https://wiki.freecad.org/Part_Workbench) and [PartDesign](https://wiki.freecad.org/PartDesign_Workbench) to create solids; Draft is normally used for simple planar drawings over a grid, as when drawing an architectural floor plan; in these situations Draft is mostly used together with the [Arch Workbench](https://wiki.freecad.org/Arch_Workbench). The tool [Draft2Sketch](https://wiki.freecad.org/Draft_Draft2Sketch) converts a Draft object to a Sketch object, and vice versa; many tools that require a 2D element as input work with either type of object as an internal conversion is done automatically.

## Sketching Workflow

A Sketch is always 2-dimensional (2D). To create a solid, a 2D Sketch of a single enclosed area is created and then either Padded or Revolved to add the 3rd dimension, creating a 3D solid from the 2D Sketch.

If a Sketch has segments that cross one another, places where a Point is not directly on a segment, or places where there are gaps between endpoints of adjacent segments, Pad or Revolve won't create a solid. Sometimes a Sketch which contains lines which cross one another will work for a simple operation such as Pad, but later operations such as Linear Pattern will fail. It is best to avoid crossing lines. The exception to this rule is that it doesn't apply to Construction (blue) Geometry.

Inside the enclosed area we can have smaller non-overlapping areas. These will become voids when the 3D solid is created.

Once a Sketch is fully constrained, the Sketch features will turn green; Construction Geometry will remain blue. It is usually "finished" at this point and suitable for use in creating a 3D solid. However, once the Sketch dialog is closed it may be worthwhile going to ![](SketcherWorkbench_files/img_h16/Workbench_Part_h16.png) [Part Workbench](https://wiki.freecad.org/Part_Workbench) and running ![](SketcherWorkbench_files/img_h16/Part_CheckGeometry_h16.png) [Check geometry](https://wiki.freecad.org/Part_CheckGeometry) to ensure there are no features in the Sketch which may cause later problems.

## Tools

The `Sketch` menu appears when the Sketcher Workbench is loaded.

### General

* ![](SketcherWorkbench_files/img_h16/Sketcher_NewSketch_h16.png) [New sketch](https://wiki.freecad.org/Sketcher_NewSketch): Creates‎ a new sketch on a selected face or plane. If no face is selected while this tool is executed the user is prompted to select a plane from a pop-up window.
* ![](SketcherWorkbench_files/img_h16/Sketcher_EditSketch_h16.png) [Edit sketch](https://wiki.freecad.org/Sketcher_EditSketch): Edit the selected Sketch. This will open the [Sketcher Dialog](https://wiki.freecad.org/Sketcher_Dialog).
* ![](SketcherWorkbench_files/img_h16/Sketcher_LeaveSketch_h16.png) [Leave sketch](https://wiki.freecad.org/Sketcher_LeaveSketch): Leave the Sketch editing mode.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ViewSketch_h16.png) [View sketch](https://wiki.freecad.org/Sketcher_ViewSketch): Sets the model view perpendicular to the sketch plane.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ViewSection_h16.png) [View section](https://wiki.freecad.org/Sketcher_ViewSection): Creates a section plane that temporarily hides any matter in front of the sketch plane.
* ![](SketcherWorkbench_files/img_h16/Sketcher_MapSketch_h16.png) [Map sketch to face](https://wiki.freecad.org/Sketcher_MapSketch): Maps a sketch to the previously selected face of a solid.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ReorientSketch_h16.png) [Reorient sketch](https://wiki.freecad.org/Sketcher_ReorientSketch): Allows you to attach the sketch to one of the main planes.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ValidateSketch_h16.png) [Validate sketch](https://wiki.freecad.org/Sketcher_ValidateSketch): Verify the tolerance of different points and adjust them.
* ![](SketcherWorkbench_files/img_h16/Sketcher_MergeSketches_h16.png) [Merge sketches](https://wiki.freecad.org/Sketcher_MergeSketches): Merge two or more sketches.
* ![](SketcherWorkbench_files/img_h16/Sketcher_MirrorSketch_h16.png) [Mirror sketch](https://wiki.freecad.org/Sketcher_MirrorSketch): Mirror a sketch along the x-axis, the y-axis or the origin.
* ![](SketcherWorkbench_files/img_h16/Sketcher_StopOperation_h16.png) [Stop operation](https://wiki.freecad.org/Sketcher_StopOperation): When in edit mode, stop the current operation, whether that is drawing, setting constraints, etc.

### Sketcher geometries

These are tools for creating objects.

* ![][Sketcher_CreatePoint] [Point](https://wiki.freecad.org/Sketcher_CreatePoint): Draws a point.
    * default: construction geometry visible in Sketch editing mode
    * Preferences > Display > Marker Size

[Sketcher_CreatePoint]:SketcherWorkbench_files/img_h16/Sketcher_CreatePoint_h16.png

* ![](SketcherWorkbench_files/img_h16/Sketcher_CreateLine_h16.png) [Line](https://wiki.freecad.org/Sketcher_CreateLine): Draws a line segment between 2 points. Lines are infinite regarding certain constraints.
* [Create Arc Commands](https://wiki.freecad.org/Sketcher_CompCreateArc)
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateArc_h16.png) [Arc](https://wiki.freecad.org/Sketcher_CreateArc): Draws an arc segment from center, radius, start angle and end angle.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_Create3PointArc_h16.png) [Arc by 3 points](https://wiki.freecad.org/Sketcher_Create3PointArc): Draws an arc segment from two endpoints and another point on the circumference.
* [Create Circle Commands](https://wiki.freecad.org/Sketcher_CompCreateCircle)
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateCircle_h16.png) [Circle](https://wiki.freecad.org/Sketcher_CreateCircle): Draws a circle from center and radius.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_Create3PointCircle_h16.png) [Circle by 3 points](https://wiki.freecad.org/Sketcher_Create3PointCircle): Draws a circle from three points on the circumference.
* Create conical sections. Unlike B-splines they can be used with all sorts of constraints such as [Tangent](https://wiki.freecad.org/Sketcher_ConstrainTangent), [Point On Object](https://wiki.freecad.org/Sketcher_ConstrainPointOnObject), or [Perpendicular](https://wiki.freecad.org/Sketcher_ConstrainPerpendicular).
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateEllipseByCenter_h16.png) [`Sketcher_CreateEllipseByCenter`](https://wiki.freecad.org/Sketcher_CreateEllipseByCenter) Draws an ellipse by center point, major radius point and minor radius point.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateEllipseBy3Points_h16.png) [`Sketcher_CreateEllipseBy3Points`](https://wiki.freecad.org/Sketcher_CreateEllipseBy3Points) Draws an ellipse by major diameter (2 points) and minor radius point.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateArcOfEllipse_h16.png) [`Sketcher_CreateArcOfEllipse`](https://wiki.freecad.org/Sketcher_CreateArcOfEllipse) Draws an arc of ellipse by center point, major radius point, starting point and ending point.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateArcOfHyperbola_h16.png) [`Sketcher_CreateArcOfHyperbola`](https://wiki.freecad.org/Sketcher_CreateArcOfHyperbola) Draws an arc of hyperbola.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateArcOfParabola_h16.png) [`Sketcher_CreateArcOfParabola`](https://wiki.freecad.org/Sketcher_CreateArcOfParabola) Draws an arc of parabola.
* Create B-spline commands:
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateBSpline_h16.png) [`Sketcher_CreateBSpline`](https://wiki.freecad.org/Sketcher_CreateBSpline) Create B-spline from its controls
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreatePeriodicBSpline_h16.png) [`Sketcher_CreatePeriodicBSpline`](https://wiki.freecad.org/Sketcher_CreatePeriodicBSpline) Create periodic B-Spline
* ![](SketcherWorkbench_files/img_h16/Sketcher_CreatePolyline_h16.png) [Polyline (multiple-point line)](https://wiki.freecad.org/Sketcher_CreatePolyline): Draws a line made of multiple line segments. Pressing the M key while drawing a Polyline toggles between the different polyline modes.
* Create rectangle commands:
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateRectangle_h16.png) [Rectangle](https://wiki.freecad.org/Sketcher_CreateRectangle): Draws a rectangle from 2 opposite points.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateRectangle_Center_h16.png) [Centered Rectangle](https://wiki.freecad.org/Sketcher_CreateRectangle_Center): Draws a rectangle from a central point and an edge point. 
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateOblong_h16.png) [Rounded Rectangle](https://wiki.freecad.org/Sketcher_CreateOblong): Draws a rounded rectangle from 2 opposite points. 
* Create regular polygon commands:
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateTriangle_h16.png) [Triangle](https://wiki.freecad.org/Sketcher_CreateTriangle): Draws a regular triangle inscribed in a construction geometry circle.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateSquare_h16.png) [Square](https://wiki.freecad.org/Sketcher_CreateSquare): Draws a regular square inscribed in a construction geometry circle.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreatePentagon_h16.png) [Pentagon](https://wiki.freecad.org/Sketcher_CreatePentagon): Draws a regular pentagon inscribed in a construction geometry circle.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateHexagon_h16.png) [Hexagon](https://wiki.freecad.org/Sketcher_CreateHexagon): Draws a regular hexagon inscribed in a construction geometry circle.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateHeptagon_h16.png) [Heptagon](https://wiki.freecad.org/Sketcher_CreateHeptagon): Draws a regular heptagon inscribed in a construction geometry circle.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateOctagon_h16.png) [Octagon](https://wiki.freecad.org/Sketcher_CreateOctagon): Draws a regular octagon inscribed in a construction geometry circle.
    * ![](SketcherWorkbench_files/img_h16/Sketcher_CreateRegularPolygon_h16.png) [Create Regular Polygon](https://wiki.freecad.org/Sketcher_CreateRegularPolygon) : Draws a regular polygon by selecting the number of sides and picking two points: the center and one corner.
* ![](SketcherWorkbench_files/img_h16/Sketcher_CreateSlot_h16.png) [Slot](https://wiki.freecad.org/Sketcher_CreateSlot): Draws an oval by selecting the center of one semicircle and an endpoint of the other semicircle.

_Modification_

* ![](SketcherWorkbench_files/img_h16/Sketcher_CreateFillet_h16.png) [Fillet](https://wiki.freecad.org/Sketcher_CreateFillet): Makes a fillet between two lines joined at one point. Select both lines or click on the corner point, then activate the tool.
* ![](SketcherWorkbench_files/img_h16/Sketcher_Trimming_h16.png) [Trimming](https://wiki.freecad.org/Sketcher_Trimming): Trims clicked segment from between interceptions of objects such as a line, circle or arc.
* ![](SketcherWorkbench_files/img_h16/Sketcher_Extend_h16.png) [Extend](https://wiki.freecad.org/Sketcher_Extend): Extends a line or an arc to a boundary line, arc, ellipse, arc of ellipse or a point in space. Click select + click target location|edge|point. (uncontrained elements may move)
* ![](SketcherWorkbench_files/img_h16/Sketcher_Split_h16.png) [Split](https://wiki.freecad.org/Sketcher_Split): Splits a line or an arc into two, converts a circle into an arc while keeping most of the constraints. 

_Reference Geometries_

* ![](SketcherWorkbench_files/img_h16/Sketcher_External_h16.png) `Sketcher_External` [External Geometry](https://wiki.freecad.org/Sketcher_External): Creates an edge linked to external geometry by inserting a linked _construction geometry_ into the sketch.
    1. Create or open _target_ sketch
        * create any new sketch via PartDesign_NewSketch after an applicable body face.
    2. Press `Sketcher_External` button
    3. 
* ![](SketcherWorkbench_files/img_h16/Sketcher_CarbonCopy_h16.png) [CarbonCopy](https://wiki.freecad.org/Sketcher_CarbonCopy): copies all the geometry and constraints from another sketch into the active sketch
    1. Have the _target sketch_ in edit mode
    2. Press `Sketcher_CarbonCopy` button
    3. Click an edge on a visible _source sketch_ 
    4. Press `esc` to terminate the operation
    * Normally the _source sketch_ and _target sketch_ are in the same `PartBody`.
    * Normally the _source sketch_ is in a plane parallel to the _target sketch_ … there are more details here.
    * If the _target sketch_ is in construction mode, then all copied geometries are created in the construction mode.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ToggleConstruction_h16.png) [Construction Mode](https://wiki.freecad.org/Sketcher_ToggleConstruction) Toggles sketch geometry from/to construction mode.
    * Construction (reference-like) geometry is shown in blue
    * Construction (reference-like) geometry is discarded outside of Sketch editing mode.
    * Construction geometries provide Sketcher level reference constructs. See also `datum` PartDesign level references.

### Sketcher constraints

Constraints are used to define lengths, set rules between sketch elements, and to lock the sketch along the vertical and horizontal axes. Some constraints require use of [Helper constraints](https://wiki.freecad.org/Sketcher_helper_constraint).

#### Geometric constraints

These constraints are not associated with numeric data.

> _Note: may need to `Auto remove redundant` constraints_

* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainCoincident_h16.png) [Coincident](https://wiki.freecad.org/Sketcher_ConstrainCoincident): Affixes a point onto (coincident with) one or more other points.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainPointOnObject_h16.png) [Point On Object](https://wiki.freecad.org/Sketcher_ConstrainPointOnObject): Affixes a point onto one or more objects such as a line, arc, or axis.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainVertical_h16.png) [Vertical](https://wiki.freecad.org/Sketcher_ConstrainVertical): Constrains the selected lines or polyline elements to a true vertical orientation. More than one object can be selected before applying this constraint.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainHorizontal_h16.png) [Horizontal](https://wiki.freecad.org/Sketcher_ConstrainHorizontal): Constrains the selected lines or polyline elements to a true horizontal orientation. More than one object can be selected before applying this constraint.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainParallel_h16.png) [Parallel](https://wiki.freecad.org/Sketcher_ConstrainParallel): Constrains two or more lines parallel to one another.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainPerpendicular_h16.png) [Perpendicular](https://wiki.freecad.org/Sketcher_ConstrainPerpendicular): Constrains two lines perpendicular to one another, or constrains a line perpendicular to an arc endpoint.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainTangent_h16.png) [Tangent](https://wiki.freecad.org/Sketcher_ConstrainTangent): Creates a tangent constraint between two selected entities, or a co-linear constraint between two line segments. A line segment does not have to lie directly on an arc or circle to be constrained tangent to that arc or circle.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainEqual_h16.png) [Equal](https://wiki.freecad.org/Sketcher_ConstrainEqual): Constrains two selected entities equal to one another. If used on circles or arcs their radii will be set equal.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainSymmetric_h16.png) [Symmetric](https://wiki.freecad.org/Sketcher_ConstrainSymmetric): Constrains two points symmetrically about a line, or constrains the first two selected points symmetrically about a third selected point.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainBlock_h16.png) [Block](https://wiki.freecad.org/Sketcher_ConstrainBlock): it blocks an edge from moving, that is, it prevents its vertices from changing their current positions. It should be particularly useful to fix the position of B-Splines. See the <a href="https://forum.freecadweb.org/viewtopic.php?f=9&amp;t=26572" rel="nofollow">Block Constraint forum topic</a>.

#### Dimensional constraints

These are constraints associated with numeric data, for which you can use the [expressions](https://wiki.freecad.org/Expressions). The data may be taken from a [spreadsheet](https://wiki.freecad.org/Spreadsheet_Workbench).

* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainLock_h16.png) [Lock](https://wiki.freecad.org/Sketcher_ConstrainLock): Constrains the selected item by setting vertical and horizontal distances relative to the origin, thereby locking the location of that item. These constraint distances can be edited later.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainDistanceX_h16.png) [Horizontal distance](https://wiki.freecad.org/Sketcher_ConstrainDistanceX): Fixes the horizontal distance between two points or line endpoints. If only one item is selected, the distance is set to the origin.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainDistanceY_h16.png) [Vertical distance](https://wiki.freecad.org/Sketcher_ConstrainDistanceY): Fixes the vertical distance between 2 points or line endpoints. If only one item is selected, the distance is set to the origin.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainDistance_h16.png) [Distance](https://wiki.freecad.org/Sketcher_ConstrainDistance): Defines the distance of a selected line by constraining its length, or defines the distance between two points by constraining the distance between them.

#### Special constraints

* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainSnellsLaw_h16.png) [Snell's Law](https://wiki.freecad.org/Sketcher_ConstrainSnellsLaw): Constrains two lines to obey a refraction law to simulate the light going through an interface.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConstrainInternalAlignment_h16.png) [Internal alignment](https://wiki.freecad.org/Sketcher_ConstrainInternalAlignment): Aligns selected elements to selected shape (e.g. a line to become major axis of an ellipse).

#### Constraint tools

The following tools can be used the change the effect of constraints:

* ![](SketcherWorkbench_files/img_h16/Sketcher_ToggleDrivingConstraint_h16.png) [Toggle driving/reference constraint](https://wiki.freecad.org/Sketcher_ToggleDrivingConstraint): Toggles the toolbar or the selected constraints to/from reference mode.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ToggleActiveConstraint_h16.png) [Activate/Deactivate constraint](https://wiki.freecad.org/Sketcher_ToggleActiveConstraint): Enable or disable an already placed constraint. 

### Sketcher tools

* ![](SketcherWorkbench_files/img_h16/Sketcher_SelectElementsWithDoFs_h16.png) [Select solver DOFs](https://wiki.freecad.org/Sketcher_SelectElementsWithDoFs): Highlights in green the geometry with degrees of freedom (DOFs), i.e. not fully constrained.
* ![](SketcherWorkbench_files/img_h16/Sketcher_CloseShape_h16.png) [Close Shape](https://wiki.freecad.org/Sketcher_CloseShape): Creates a closed shape by applying coincident constraints to endpoints.
* ![](SketcherWorkbench_files/img_h16/Sketcher_ConnectLines_h16.png) [Connect Edges](https://wiki.freecad.org/Sketcher_ConnectLines): Connect sketcher elements by applying coincident constraints to endpoints.
* ![](SketcherWorkbench_files/img_h16/Sketcher_SelectConstraints_h16.png) [Select Constraints](https://wiki.freecad.org/Sketcher_SelectConstraints): Selects the constraints of a sketcher element.
* ![](SketcherWorkbench_files/img_h16/Sketcher_SelectElementsAssociatedWithConstraints_h16.png) [Select Elements Associated with constraints](https://wiki.freecad.org/Sketcher_SelectElementsAssociatedWithConstraints): Select sketcher elements associated with constraints.
* ![](SketcherWorkbench_files/img_h16/Sketcher_SelectRedundantConstraints_h16.png) [Select Redundant Constraints](https://wiki.freecad.org/Sketcher_SelectRedundantConstraints): Selects redundant constraints of a sketch.
* ![](SketcherWorkbench_files/img_h16/Sketcher_SelectConflictingConstraints_h16.png) [Select Conflicting Constraints](https://wiki.freecad.org/Sketcher_SelectConflictingConstraints): Selects conflicting constraints of a sketch.
* ![](SketcherWorkbench_files/img_h16/Sketcher_RestoreInternalAlignmentGeometry_h16.png) [Show/Hide internal geometry](https://wiki.freecad.org/Sketcher_RestoreInternalAlignmentGeometry): Recreates missing/deletes unneeded internal geometry of a selected ellipse, arc of ellipse/hyperbola/parabola or B-spline.
* ![](SketcherWorkbench_files/img_h16/Sketcher_SelectOrigin_h16.png) [Select Origin](https://wiki.freecad.org/Sketcher_SelectOrigin): Selects the origin of a sketch.
* ![](SketcherWorkbench_files/img_h16/Sketcher_SelectVerticalAxis_h16.png) [Select Vertical Axis](https://wiki.freecad.org/Sketcher_SelectVerticalAxis): Selects the vertical axis of a sketch.
* ![](SketcherWorkbench_files/img_h16/Sketcher_SelectHorizontalAxis_h16.png) [Select Horizontal Axis](https://wiki.freecad.org/Sketcher_SelectHorizontalAxis): Selects the horizontal axis of a sketch.
* ![](SketcherWorkbench_files/img_h16/Sketcher_Symmetry_h16.png) [Symmetry](https://wiki.freecad.org/Sketcher_Symmetry): Copies a sketcher element symmetrical to a chosen line.
* ![][Sketcher_Clone_h16] [Clone](https://wiki.freecad.org/Sketcher_Clone): Clones a sketcher element by 1. makes a copy, 2. sets _existing contraints_ to be equal. Hint: Add needed common constrains on original before cloning.
* ![](SketcherWorkbench_files/img_h16/Sketcher_Copy_h16.png) [Copy](https://wiki.freecad.org/Sketcher_Copy): Copies a sketcher element.
* ![](SketcherWorkbench_files/img_h16/Sketcher_Move_h16.png) [Move](https://wiki.freecad.org/Sketcher_Move): Moves the selected geometry taking as reference the last selected point.
* ![](SketcherWorkbench_files/img_h16/Sketcher_RectangularArray_h16.png) [Rectangular Array](https://wiki.freecad.org/Sketcher_RectangularArray): Creates an array of selected sketcher elements.
* ![](SketcherWorkbench_files/img_h16/Sketcher_RemoveAxesAlignment_h16.png) [Remove Axes Alignment](https://wiki.freecad.org/Sketcher_RemoveAxesAlignment): Remove axes alignment while trying to preserve the constraint relationship of the selection. 
* ![](SketcherWorkbench_files/img_h16/Sketcher_DeleteAllGeometry_h16.png) [Delete All Geometry](https://wiki.freecad.org/Sketcher_DeleteAllGeometry): Deletes all geometry from the sketch.
* ![](SketcherWorkbench_files/img_h16/Sketcher_DeleteAllConstraints_h16.png) [Delete All Constraints](https://wiki.freecad.org/Sketcher_DeleteAllConstraints): Deletes all constraints from the sketch.

[Sketcher_Clone_h16]:SketcherWorkbench_files/img_h16/Sketcher_Clone_h16.png


### Sketcher B-spline tools

* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineDegree_h16.png) [Show/hide B-spline degree](https://wiki.freecad.org/Sketcher_BSplineDegree)
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplinePolygon_h16.png) [Show/hide B-spline control polygon](https://wiki.freecad.org/Sketcher_BSplinePolygon)
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineComb_h16.png) [Show/hide B-spline curvature comb](https://wiki.freecad.org/Sketcher_BSplineComb)
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineKnotMultiplicity_h16.png) [Show/hide B-spline knot multiplicity](https://wiki.freecad.org/Sketcher_BSplineKnotMultiplicity)
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplinePoleWeight_h16.png) [Show/hide B-spline control point weight](https://wiki.freecad.org/Sketcher_BSplinePoleWeight), 
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineApproximate_h16.png) [Convert geometry to B-spline](https://wiki.freecad.org/Sketcher_BSplineApproximate)
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineIncreaseDegree_h16.png) [Increase B-spline degree](https://wiki.freecad.org/Sketcher_BSplineIncreaseDegree)
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineDecreaseDegree_h16.png) [Decrease B-spline degree](https://wiki.freecad.org/Sketcher_BSplineDecreaseDegree), 
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineIncreaseKnotMultiplicity_h16.png) [Increase knot multiplicity](https://wiki.freecad.org/Sketcher_BSplineIncreaseKnotMultiplicity)
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineDecreaseKnotMultiplicity_h16.png) [Decrease knot multiplicity](https://wiki.freecad.org/Sketcher_BSplineDecreaseKnotMultiplicity)
* ![](SketcherWorkbench_files/img_h16/Sketcher_BSplineInsertKnot_h16.png) [Insert knot](https://wiki.freecad.org/Sketcher_BSplineInsertKnot), 

### Sketcher virtual space

* ![](SketcherWorkbench_files/img_h16/Sketcher_SwitchVirtualSpace_h16.png) [Switch Virtual Space](https://wiki.freecad.org/Sketcher_SwitchVirtualSpace): Allows you to hide all constraints of a sketch and make them visible again.

## Preferences

* ![](SketcherWorkbench_files/img_h16/Preferences-general_h16.png) [Preferences](https://wiki.freecad.org/Sketcher_Preferences): Preferences for the **Sketcher** workbench.

## Best Practices

* A series of simple sketches is easier to manage than a single complex one.
* Always create a closed profile, or the sketch won't produce a solid. 
    * An open profile will only create a set of open faces.
    * Create objects to be excluded from the solid creation as construction elements with the Construction Mode tool.
* Use the auto constraints feature to limit the number of manual constraints which must also be.
* As a general rule, 
    1. first, apply geometric constraints
    2. then add dimensional constraints
    3. lock the sketch last.
* If possible, center the sketch to the origin (0,0) with the lock constraint.
    * If the sketch is not symmetric, 
        * locate one of its points to the origin, 
        * or, choose nice round numbers for the lock distances.
* Prefer a `Horizontal Distance` or `Vertical Distance` constraint to a `Length` constraint. `Horizontal Distance` and `Vertical Distance` constraints are computationally cheaper.
* In general, the best constraints to use are: 
    * Horizontal and Vertical Constraints; 
    * Horizontal and Vertical Length Constraints; 
    * Point-to-Point Tangency.
* If possible, limit the use of these: 
    * the general Length Constraint; 
    * Edge-to-Edge Tangency; 
    * Fix Point Onto a Line Constraint; 
    * Symmetry Constraint.
* If in doubt about the validity of a sketch once it is complete (features turn green), 
    1. close the Sketcher dialog, 
    2. switch to the ![](SketcherWorkbench_files/img_h16/Workbench_Part_h16.png) [Part Workbench](https://wiki.freecad.org/Part_Workbench)
    3. run ![](SketcherWorkbench_files/img_h16/Part_CheckGeometry_h16.png) [Check geometry](https://wiki.freecad.org/Part_CheckGeometry).

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* <a href="https://forum.freecadweb.org/viewtopic.php?f=36&amp;t=30104" rel="nofollow">Sketcher tutorial</a> by chrisb. This is a 70-page long PDF document that serves as a detailed manual for the sketcher. It explains the basics of Sketcher usage, and goes into a lot of detail about the creation of geometrical shapes, and each of the constraints.
* [Basic Sketcher Tutorial](https://wiki.freecad.org/Basic_Sketcher_Tutorial) for beginners
* [Sketcher Micro Tutorial - Constraint Practices](https://wiki.freecad.org/Sketcher_Micro_Tutorial_-_Constraint_Practices)
* [Sketcher requirement for a sketch](https://wiki.freecad.org/Sketcher_requirement_for_a_sketch) Minimum requirement for a sketch and Complete determination of a sketch

