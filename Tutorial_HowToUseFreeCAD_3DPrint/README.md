# How To Use FreeCAD For 3D Printing<br>(For Beginners)

The markdown pages listed below provide notes based on the Maker Tales ["How To Use FreeCAD For Beginners … For 3D Printing" series ⇗](https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN) video tutorial series. Videos 1 - 5 provide a beginner level introduction to FreeCAD. How to export a model for 3D printing is covered in the video 4.

0. [FreeCAD's "Topological Naming Problem"](00_FreeCAD_Broken.md)
1. [Setup & Basics](01_FreeCAD_Setup_Basics.md)
2. [Sketcher Workbench: Lines, Tools & References](02_FreeCAD_Sketcher_Lines_Tools_Refs.md)
3. [Sketcher Workbench: Constraints](03_FreeCAD_Sketcher_Constraints.md)
4. [3D Printing](04_FreeCAD_3D_Print.md)
5. [How To Do Parametric Modeling](05_FreeCAD_Parametric.md)
