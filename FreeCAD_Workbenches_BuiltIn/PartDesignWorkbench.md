# FreeCAD: ![](PartDesignWorkbench_files/img_h16/Workbench_PartDesign_h16.png) [PartDesign Workbench][t]
[t]:https://wiki.freecad.org/PartDesign_Workbench

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

**![](PartDesignWorkbench_files/img_h16/Workbench_PartDesign_h16.png) [PartDesign Workbench](https://wiki.freecad.org/PartDesign_Workbench)** 

* provides advanced tools for modeling complex solid parts. 
* create mechanical parts to be manufactured and assembled into a finished product.
* create solids for [architectural design](https://wiki.freecad.org/Arch_Workbench), [finite element analysis](https://wiki.freecad.org/FEM_Workbench), or [machining and 3D printing](https://wiki.freecad.org/Path_Workbench).
* intrinsically related to the [Sketcher Workbench](https://wiki.freecad.org/Sketcher_Workbench). 
     * creates a Sketch, then uses the [PartDesign Pad](https://wiki.freecad.org/PartDesign_Pad) tool to extrude it and create a basic solid, and then this solid is further modified.
* uses a parametric, feature editing methodology. 
    * A basic solid is sequentially transformed by adding features on top until the final shape is obtained.
    * See also: 
        * [feature editing](https://wiki.freecad.org/Feature_editing) page for a process explanation 
        * [creating a simple part with PartDesign](https://wiki.freecad.org/Creating_a_simple_part_with_PartDesign) to get started.

> The bodies created with PartDesign are often subject to the [topological naming problem](https://wiki.freecad.org/Topological_naming_problem). Internal features to be renamed when the parametric operations are modified. This problem can be minimized by following the best practices described in the [feature editing](https://wiki.freecad.org/Feature_editing) page, and by taking advantage of datum objects as support for sketches and features.

![](PartDesignWorkbench_files/500px-PartDesign_Example.png)

## Tools

The Part Design tools are all located in the **Part Design** menu and the PartDesign toolbar that appear when you load the Part Design workbench.

### Structure tools

These tools are in fact not part of the PartDesign Workbench. They belong to the [Std Base](https://wiki.freecad.org/Std_Base) system. They were developed in v0.17 with the intention that they would be useful to organize a model, and create [assemblies](https://wiki.freecad.org/Assembly); as such, they are very useful when working with bodies created with this workbench.

* ![](PartDesignWorkbench_files/img_h16/Std_Part_h16.png) [Part](https://wiki.freecad.org/Std_Part): adds a new Part container in the active document and makes it active.
* ![](PartDesignWorkbench_files/img_h16/Std_Group_h16.png) [Group](https://wiki.freecad.org/Std_Group): adds a Group container in the active document, which allows organizing the objects in the [tree view](https://wiki.freecad.org/Tree_view).

### Part Design Helper tools

* ![](PartDesignWorkbench_files/img_h16/PartDesign_Body_h16.png) [Create body](https://wiki.freecad.org/PartDesign_Body): creates a [Body](https://wiki.freecad.org/Body) object in the active document and makes it active.
* ![](PartDesignWorkbench_files/img_h16/Sketcher_NewSketch_h16.png) [Create sketch](https://wiki.freecad.org/PartDesign_NewSketch): creates‎ a new sketch on a selected face or plane. If no face is selected while this tool is executed, the user is prompted to select a plane from the Tasks panel. The interface then switches to the [Sketcher Workbench](https://wiki.freecad.org/Sketcher_Workbench) in sketch editing mode.
    * > Beta Issue: _v20 made need to close and reopen to select sketch to see other sketches._
    * In PartDesign Workbench, prefer `PartDesign_NewSketch` instead of `Sketcher_NewSketch`
    1. Select face
    2. Click `PartDesign_NewSketch` button
* ![](PartDesignWorkbench_files/img_h16/Sketcher_EditSketch_h16.png) [Edit sketch](https://wiki.freecad.org/Sketcher_EditSketch): Edit the selected Sketch.
* ![](PartDesignWorkbench_files/img_h16/Sketcher_MapSketch_h16.png) [Map sketch to face](https://wiki.freecad.org/Sketcher_MapSketch): Maps a sketch to a previously selected plane or a face of the active body.

### Part Design Modeling tools

#### Datum tools

A `datum` object (yellow-gold) can be used as a `PartDesign` Body reference for sketches or other datum geometry. (_Construction reference geometries_ occur inside Sketcher workbench.) Datum objects can help avoid the [topological naming problem](https://wiki.freecadweb.org/Topological_naming_problem). 

> Note: A sketch on a data plane, instead of a sketch on a face, can avoid the internal face re-name issue.

* ![](PartDesignWorkbench_files/img_h16/PartDesign_Point_h16.png) [Create a datum point](https://wiki.freecad.org/PartDesign_Point): creates a datum point in the active body.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_Line_h16.png) [Create a datum line](https://wiki.freecad.org/PartDesign_Line): creates a datum line in the active body.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_Plane_h16.png) [Create a datum plane](https://wiki.freecad.org/PartDesign_Plane): creates a datum plane in the active body.
    * Hide any `Origin` (e.g. Part Origin) which will not be used. 
    * Reference 1: Select a Body Origin plane (should shows attachment mode as `Plane face`)
    * Attachment offset _(in local coordinate)_ … i.e. locally, datum plane lays flat in local-x-y coordinate perspective
    * Reference: make independent copy (recommended)
* ![](PartDesignWorkbench_files/img_h16/PartDesign_CoordinateSystem_h16.png) [Create a local coordinate system](https://wiki.freecad.org/PartDesign_CoordinateSystem): creates a local coordinate system attached to datum geometry in the active body.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_ShapeBinder_h16.png) [Create a shape binder](https://wiki.freecad.org/PartDesign_ShapeBinder): creates a shape binder in the active body.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_SubShapeBinder_h16.png) [Create a sub-object shape binder](https://wiki.freecad.org/PartDesign_SubShapeBinder): creates a shape binder to a subelement, like edge or face from another body, while retaining the relative position of that element. 
* ![](PartDesignWorkbench_files/img_h16/PartDesign_Clone_h16.png) [Create a clone](https://wiki.freecad.org/PartDesign_Clone): creates a clone of the selected body.

#### Additive tools

* **Create a sketch-based additive volume**
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_Pad_h16.png) [Pad](https://wiki.freecad.org/PartDesign_Pad): extrudes a solid from a selected sketch.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_Revolution_h16.png) [Revolution](https://wiki.freecad.org/PartDesign_Revolution): creates a solid by revolving a sketch around an axis. The sketch must form a closed profile.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveLoft_h16.png) [Additive loft](https://wiki.freecad.org/PartDesign_AdditiveLoft): creates a solid by making a transition between two or more sketches.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditivePipe_h16.png) [Additive pipe](https://wiki.freecad.org/PartDesign_AdditivePipe): creates a solid by sweeping one or more sketches along an open or closed path.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveHelix_h16.png) [Additive helix](https://wiki.freecad.org/PartDesign_AdditiveHelix): creates a solid by sweeping a sketch along a helix. 
* ![](PartDesignWorkbench_files/48px-PartDesign_CompPrimitiveAdditive.png) **[Create an additive primitive](https://wiki.freecad.org/PartDesign_CompPrimitiveAdditive)**
    1. parameters
    2. attachment: see [Part_EditAttachment](https://wiki.freecadweb.org/Part_EditAttachment) for attachment mode options which are available in `Combo View` > `Tasks` > `Attachments`.
    3. name/label
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveBox_h16.png) [Additive box](https://wiki.freecad.org/PartDesign_AdditiveBox): creates an additive box.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveCone_h16.png) [Additive cone](https://wiki.freecad.org/PartDesign_AdditiveCone): creates an additive cone.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveCylinder_h16.png) [Additive cylinder](https://wiki.freecad.org/PartDesign_AdditiveCylinder): creates an additive cylinder.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveEllipsoid_h16.png) [Additive ellipsoid](https://wiki.freecad.org/PartDesign_AdditiveEllipsoid): creates an additive ellipsoid.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditivePrism_h16.png) [Additive prism](https://wiki.freecad.org/PartDesign_AdditivePrism): creates an additive prism.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveSphere_h16.png) [Additive sphere](https://wiki.freecad.org/PartDesign_AdditiveSphere): creates an additive sphere.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveTorus_h16.png) [Additive torus](https://wiki.freecad.org/PartDesign_AdditiveTorus): creates an additive torus.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_AdditiveWedge_h16.png) [Additive wedge](https://wiki.freecad.org/PartDesign_AdditiveWedge): creates an additive wedge.

#### Subtractive tools

* **Create a sketch-based additive volume**
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_Pocket_h16.png) [Pocket](https://wiki.freecad.org/PartDesign_Pocket): cuts away along a straight path extrusion based on a sketch or face.
        1. Select one or more sketches|faces
        2. Press ![](PartDesignWorkbench_files/img_h16/PartDesign_Pocket_h16.png) pocket
        3. Set parameters
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_Hole_h16.png) [Hole](https://wiki.freecad.org/PartDesign_Hole): creates a hole feature from a selected sketch. The sketch must contain one or more selected circles|arcs.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_Groove_h16.png) [Groove](https://wiki.freecad.org/PartDesign_Groove): creates a groove by revolving a sketch around an axis.
        * TDB: how to attach a sketch for the groove rotation?
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveLoft_h16.png) [Subtractive loft](https://wiki.freecad.org/PartDesign_SubtractiveLoft): creates a solid shape by making a transition between two or more sketches and subtracts it from the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractivePipe_h16.png) [Subtractive pipe](https://wiki.freecad.org/PartDesign_SubtractivePipe): creates a solid shape by sweeping one or more sketches along an open or closed path and subtracts it from the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveHelix_h16.png) [Subtractive helix](https://wiki.freecad.org/PartDesign_SubtractiveHelix): creates a solid shape by sweeping a sketch along a helix and subtracts it from the active body. 
* ![](PartDesignWorkbench_files/48px-PartDesign_CompPrimitiveSubtractive.png) [Create a subtractive primitive](https://wiki.freecad.org/PartDesign_CompPrimitiveSubtractive)
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveBox_h16.png) [Subtractive box](https://wiki.freecad.org/PartDesign_SubtractiveBox): adds a subtractive box to the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveCone_h16.png) [Subtractive cone](https://wiki.freecad.org/PartDesign_SubtractiveCone): adds a subtractive cone to the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveCylinder_h16.png) [Subtractive cylinder](https://wiki.freecad.org/PartDesign_SubtractiveCylinder): adds a subtractive cylinder to the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveEllipsoid_h16.png) [Subtractive ellipsoid](https://wiki.freecad.org/PartDesign_SubtractiveEllipsoid): adds a subtractive ellipsoid to the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractivePrism_h16.png) [Subtractive prism](https://wiki.freecad.org/PartDesign_SubtractivePrism): adds a subtractive prism to the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveSphere_h16.png) [Subtractive sphere](https://wiki.freecad.org/PartDesign_SubtractiveSphere): adds a subtractive sphere to the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveTorus_h16.png) [Subtractive torus](https://wiki.freecad.org/PartDesign_SubtractiveTorus): adds a subtractive torus to the active body.
    * ![](PartDesignWorkbench_files/img_h16/PartDesign_SubtractiveWedge_h16.png) ‎[Subtractive wedge](https://wiki.freecad.org/PartDesign_SubtractiveWedge): adds a subtractive wedge to the active body.

#### Transformation tools

These are tools for transforming existing features. They will allow you to choose which features to transform.

* ![](PartDesignWorkbench_files/img_h16/PartDesign_Mirrored_h16.png) [Mirrored](https://wiki.freecad.org/PartDesign_Mirrored): mirrors one or more features on a plane or face.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_LinearPattern_h16.png) [Linear Pattern](https://wiki.freecad.org/PartDesign_LinearPattern): creates a linear pattern based on one or more features.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_PolarPattern_h16.png) [Polar Pattern](https://wiki.freecad.org/PartDesign_PolarPattern): creates a polar pattern of one or more features.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_MultiTransform_h16.png) [Create MultiTransform](https://wiki.freecad.org/PartDesign_MultiTransform): creates a pattern with any combination of the other transformations.

#### Dress-up tools

These tools apply a treatment to the selected edges or faces.

* ![](PartDesignWorkbench_files/img_h16/PartDesign_Fillet_h16.png) [Fillet](https://wiki.freecad.org/PartDesign_Fillet): fillets (rounds) edges of the active body.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_Chamfer_h16.png) [Chamfer](https://wiki.freecad.org/PartDesign_Chamfer): chamfers edges of the active body.
    * Equal distance: chamfer edges are equally distanced from the body edge
    * Two distances: distances of each chamfer edge to the body edge are specified
    * Distance and angle: One distances of the chamfer edge to the body edge is specified. The second chamfer edge is defined by the angle of the chamfer.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_Draft_h16.png) [Draft](https://wiki.freecad.org/PartDesign_Draft): applies an angular draft to selected faces of the active body.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_Thickness_h16.png) [Thickness](https://wiki.freecad.org/PartDesign_Thickness): creates a thick shell from the active body and opens selected face(s).

#### Boolean

* ![](PartDesignWorkbench_files/img_h16/PartDesign_Boolean_h16.png) [Boolean operation](https://wiki.freecad.org/PartDesign_Boolean): imports one or more Bodies or PartDesign Clones into the active body and applies a Boolean operation.

#### Extras

Some additional functionality found in the Part Design menu:

* ![](PartDesignWorkbench_files/img_h16/PartDesign_Migrate_h16.png) [Migrate](https://wiki.freecad.org/PartDesign_Migrate): migrates files created with older FreeCAD versions. If the file is pure PartDesign feature-based, migration should succeed. If the file contains mixed Part/Part Design/Draft objects, the conversion will most likely fail.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_Sprocket_h16.png) [Sprocket design wizard](https://wiki.freecad.org/PartDesign_Sprocket): creates a sprocket profile that can be padded. 
* ![](PartDesignWorkbench_files/img_h16/PartDesign_InternalExternalGear_h16.png) [Involute gear design wizard](https://wiki.freecad.org/PartDesign_InvoluteGear): creates an involute gear profile that can be padded.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_WizardShaft_h16.png) [Shaft design wizard](https://wiki.freecad.org/PartDesign_WizardShaft): Generates a shaft from a table of values and allows to analyze forces and moments. The shaft is made with a revolved sketch that can be edited.

### Measurements Tools

The `Parts` workbench measurement tools are also available in the `Part Design` workbench. See the `Parts` workbench for details: 
<code>[Part_Measure_Linear](https://wiki.freecad.org/Part_Measure_Linear)</code>, 
<code>[Part_Measure_Angular](https://wiki.freecad.org/Part_Measure_Angular)</code>, 
<code>[Part_Measure_Refresh](https://wiki.freecad.org/Part_Measure_Refresh)</code>, 
<code>[Part_Measure_Clear_All](https://wiki.freecad.org/Part_Measure_Clear_All)</code>, 
<code>[Part_Measure_Toggle_All](https://wiki.freecad.org/Part_Measure_Toggle_All)</code>, 
<code>[Part_Measure_Toggle_3D](https://wiki.freecad.org/Part_Measure_Toggle_3D)</code>, 
<code>[Part_Measure_Toggle_Delta](https://wiki.freecad.org/Part_Measure_Toggle_Delta)</code>

### Context Menu Items

* ![](PartDesignWorkbench_files/img_h16/PartDesign_MoveTip_h16.png) [Set tip](https://wiki.freecad.org/PartDesign_MoveTip): redefines the tip, which is the feature exposed outside of the Body.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_MoveFeature_h16.png) [Move object to other body](https://wiki.freecad.org/PartDesign_MoveFeature): moves the selected sketch, datum geometry or feature to another Body.
* ![](PartDesignWorkbench_files/img_h16/PartDesign_MoveFeatureInTree_h16.png) [Move object after other object](https://wiki.freecad.org/PartDesign_MoveFeatureInTree): allows reordering of the Body tree by moving the selected sketch, datum geometry or feature to another position in the list of features.

#### Items shared with the Part workbench

* ![](PartDesignWorkbench_files/img_h16/Std_SetAppearance_h16.png) [Appearance](https://wiki.freecad.org/Std_SetAppearance): determines appearance of the whole part (color transparency etc.).
* ![](PartDesignWorkbench_files/img_h16/Part_FaceColors_h16.png) [Set colors](https://wiki.freecad.org/Part_FaceColors): assigns colors to part faces.

## Preferences

* ![](PartDesignWorkbench_files/img_h16/Preferences-part_design_h16.png) [Preferences](https://wiki.freecad.org/PartDesign_Preferences): preferences available for PartDesign Tools.
* [Fine tuning](https://wiki.freecad.org/Fine-tuning): some extra parameters to fine-tune PartDesign behavior.

## Datum Reference  <a id="datum-reference-"></a><sup>[▴](#contents)</sup>

![](PartDesignWorkbench_files/DatumReference.png)

> You selected geometries which are not part of the active body. _For example, a Part origin plane, instead of an in Body origin._


* Make independent copy
    * copies stuff from the other body or part
    * "recommended", but not forum user recommended
* Make dependent copy
    * adds a ShapeBinder
* Create cross-reference
    * forum comment "create an illegal reference to another sketch" ?

_related_

* [Sketch Reference: Independent/Dependent/Cross Reference](https://forum.freecadweb.org/viewtopic.php?t=51742)

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [How to use FreeCAD](http://help-freecad-jpg87.fr/) a website describing the workflow for mechanical design.
* [Creating a simple part with PartDesign](https://wiki.freecad.org/Creating_a_simple_part_with_PartDesign)
* [Basic Part Design Tutorial](https://wiki.freecad.org/Basic_Part_Design_Tutorial)
* [PartDesign Bearingholder Tutorial I](https://wiki.freecad.org/PartDesign_Bearingholder_Tutorial_I) (needs updating)
* [PartDesign Bearingholder Tutorial II](https://wiki.freecad.org/PartDesign_Bearingholder_Tutorial_II) (needs updating)
