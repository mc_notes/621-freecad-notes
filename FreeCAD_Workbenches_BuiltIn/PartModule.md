# FreeCAD: ![](PartModule_files/img_h16/Workbench_Part_h16.png) [Part Module][t]
[t]:https://wiki.freecad.org/Part_Module

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Attachment Concepts](#attachment-concepts-) •
[Tools](#tools-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

**[OpenCASCADE Technology (OCC|OCCT)](https://wiki.freecad.org/OpenCASCADE)**

* FreeCAD solid modeling is based on OCCT 
* ![](PartModule_files/img_h16/Workbench_Part_h16.png) [Part Workbench](https://wiki.freecad.org/Part_Workbench) 
    * is the basic layer that exposes the OCCT functions to all workbenches.
    * gives the user access to OCCT geometric primitives and functions. 
* [Part workbench versus Part Design workbench](https://wiki.freecad.org/Part_and_PartDesign)
    * Part Workbench
        * relatively simple objects
        * objects are used with boolean operations (unions and cuts) to build more complex shapes
        * [constructive solid geometry](https://wiki.freecad.org/Constructive_solid_geometry) (CSG) workflow
        * each object created is separate unless expressly combined (e.g. Boolean add or substract) with another object
    * [PartDesign Workbench](https://wiki.freecad.org/PartDesign_Workbench) 
        * provides a more modern workflow to constructing shapes
            1. create an initial Body block of material from one of:
                * additive primitive
                * parametrically defined sketch extrusion
                * imported shape (aka Base Feature)
            2. sequentially modify with parametric transformations ([feature editing](https://wiki.freecad.org/Feature_editing))
        * each feature edit adds or subtracts material
        * `Body` object is constructed directly as a single solitary cumulative solid.
        * `Body` object does not have any disconnected features
* Part objects provide more advanced operations than [Mesh Workbench](https://wiki.freecad.org/Mesh_Workbench) objects:
    * coherent boolean operations, modifications history, and parametric behaviour.

![](PartModule_files/Part_Workbench_relationships.svg)

## Attachment Concepts <a id="attachment-concepts-"></a><sup>[▴](#contents)</sup>

See [Part_EditAttachment](https://wiki.freecadweb.org/Part_EditAttachment) for attachment mode options which are available in `Combo View` > `Tasks` > `Attachments`.

* `Inertia 2-3` Object will be attached to a plane passing through second and third principal axes of inertia (passes through center of mass). 


## Tools <a id="tools-"></a><sup>[▴](#contents)</sup>

The tools are located in the **Part** menu or the **Measure** menu.

### Primitives

These are tools for creating primitive objects.

* ![](PartModule_files/img_h16/Part_Box_h16.png) [Box](https://wiki.freecad.org/Part_Box): Creates a box.
* ![](PartModule_files/img_h16/Part_Cylinder_h16.png) [Cylinder](https://wiki.freecad.org/Part_Cylinder): Creates a cylinder.
* ![](PartModule_files/img_h16/Part_Sphere_h16.png) [Sphere](https://wiki.freecad.org/Part_Sphere): Creates a sphere.
* ![](PartModule_files/img_h16/Part_Cone_h16.png) [Cone](https://wiki.freecad.org/Part_Cone): Creates a cone.
* ![](PartModule_files/img_h16/Part_Torus_h16.png) [Torus](https://wiki.freecad.org/Part_Torus): Creates a torus (ring).
* ![](PartModule_files/img_h16/Part_Tube_h16.png) [Tube](https://wiki.freecad.org/Part_Tube): Creates a tube. 
* ![](PartModule_files/img_h16/Part_Primitives_h16.png) [Primitives](https://wiki.freecad.org/Part_Primitives): A tool to create one of the following primitives:
    * ![](PartModule_files/img_h16/Part_Plane_h16.png) [Plane](https://wiki.freecad.org/Part_Plane): Creates a plane.
    * ![](PartModule_files/img_h16/Tree_Part_Box_Parametric_h16.png) [Box](https://wiki.freecad.org/Part_Box): Creates a box. This object can also be created with the ![](PartModule_files/img_h16/Part_Box_h16.png) [Box](https://wiki.freecad.org/Part_Box) tool.
    * ![](PartModule_files/img_h16/Tree_Part_Cylinder_Parametric_h16.png) [Cylinder](https://wiki.freecad.org/Part_Cylinder): Creates a cylinder. This object can also be created with the ![](PartModule_files/img_h16/Part_Cylinder_h16.png) [Cylinder](https://wiki.freecad.org/Part_Cylinder) tool.
    * ![](PartModule_files/img_h16/Tree_Part_Cone_Parametric_h16.png) [Cone](https://wiki.freecad.org/Part_Cone): Creates a cone. This object can also be created with the ![](PartModule_files/img_h16/Part_Cone_h16.png) [Cone](https://wiki.freecad.org/Part_Cone) tool.
    * ![](PartModule_files/img_h16/Tree_Part_Sphere_Parametric_h16.png) [Sphere](https://wiki.freecad.org/Part_Sphere): Creates a sphere. This object can also be created with the ![](PartModule_files/img_h16/Part_Sphere_h16.png) [Sphere](https://wiki.freecad.org/Part_Sphere) tool.
    * ![](PartModule_files/img_h16/Part_Ellipsoid_h16.png) [Ellipsoid](https://wiki.freecad.org/Part_Ellipsoid): Creates a ellipsoid.
    * ![](PartModule_files/img_h16/Tree_Part_Torus_Parametric_h16.png) [Torus](https://wiki.freecad.org/Part_Torus): Creates a torus. This object can also be created with the ![](PartModule_files/img_h16/Part_Torus_h16.png) [Torus](https://wiki.freecad.org/Part_Torus) tool.
    * ![](PartModule_files/img_h16/Part_Prism_h16.png) [Prism](https://wiki.freecad.org/Part_Prism): Creates a prism.
    * ![](PartModule_files/img_h16/Part_Wedge_h16.png) [Wedge](https://wiki.freecad.org/Part_Wedge): Creates a wedge.
    * ![](PartModule_files/img_h16/Part_Helix_h16.png) [Helix](https://wiki.freecad.org/Part_Helix): Creates a helix.
    * ![](PartModule_files/img_h16/Part_Spiral_h16.png) [Spiral](https://wiki.freecad.org/Part_Spiral): Creates a spiral.
    * ![](PartModule_files/img_h16/Part_Circle_h16.png) [Circle](https://wiki.freecad.org/Part_Circle): Creates a circular edge.
    * ![](PartModule_files/img_h16/Part_Ellipse_h16.png) [Ellipse](https://wiki.freecad.org/Part_Ellipse): Creates an elliptical edge.
    * ![](PartModule_files/img_h16/Part_Point_h16.png) [Point](https://wiki.freecad.org/Part_Point): Creates a point (vertex).
    * ![](PartModule_files/img_h16/Part_Line_h16.png) [Line](https://wiki.freecad.org/Part_Line): Creates a line (edge).
    * ![](PartModule_files/img_h16/Part_RegularPolygon_h16.png) [Regular Polygon](https://wiki.freecad.org/Part_RegularPolygon): Creates a regular polygon.
* ![](PartModule_files/img_h16/Part_Builder_h16.png) [Builder](https://wiki.freecad.org/Part_Builder): Creates shapes from various primitives.

### Creation and modification

These are tools for creating new and modifying existing objects.

* ![](PartModule_files/img_h16/Part_Extrude_h16.png) [Extrude](https://wiki.freecad.org/Part_Extrude): Extrudes planar faces.
* ![](PartModule_files/img_h16/Part_Revolve_h16.png) [Revolve](https://wiki.freecad.org/Part_Revolve): Creates a solid by revolving an object (not a solid) around an axis.
* ![](PartModule_files/img_h16/Part_Mirror_h16.png) [Mirror](https://wiki.freecad.org/Part_Mirror): Mirrors the selected object across a mirror plane.
* ![](PartModule_files/img_h16/Part_Fillet_h16.png) [Fillet](https://wiki.freecad.org/Part_Fillet): Fillets (rounds) edges of an object.
* ![](PartModule_files/img_h16/Part_Chamfer_h16.png) [Chamfer](https://wiki.freecad.org/Part_Chamfer): Chamfers edges of an object.
* ![](PartModule_files/img_h16/Part_MakeFace_h16.png) [Make face from wires](https://wiki.freecad.org/Part_MakeFace): Makes a face from a set of wires (contours). 
* ![](PartModule_files/img_h16/Part_RuledSurface_h16.png) [Ruled Surface](https://wiki.freecad.org/Part_RuledSurface): Creates a ruled surface.
* ![](PartModule_files/img_h16/Part_Loft_h16.png) [Loft](https://wiki.freecad.org/Part_Loft): Lofts from one profile to another.
* ![](PartModule_files/img_h16/Part_Sweep_h16.png) [Sweep](https://wiki.freecad.org/Part_Sweep): Sweeps one or more profiles along a path.
* ![](PartModule_files/img_h16/Part_Section_h16.png) [Section](https://wiki.freecad.org/Part_Section): Creates a section by intersecting an object with a section plane.
* ![](PartModule_files/img_h16/Part_CrossSections_h16.png) [Cross sections...](https://wiki.freecad.org/Part_CrossSections): Creates one or more cross-sections through an object.
* [Offset tools](https://wiki.freecad.org/Part_CompOffsetTools):
    * ![](PartModule_files/img_h16/Part_Offset_h16.png) [3D Offset](https://wiki.freecad.org/Part_Offset): Constructs a parallel shape at a certain distance from an original.
    * ![](PartModule_files/img_h16/Part_Offset2D_h16.png) [2D Offset](https://wiki.freecad.org/Part_Offset2D): Constructs a parallel wire at certain distance from an original, or enlarges/shrinks a planar face.
* ![](PartModule_files/img_h16/Part_Thickness_h16.png) [Thickness](https://wiki.freecad.org/Part_Thickness): Hollows out a solid.
* ![](PartModule_files/img_h16/Part_ProjectionOnSurface_h16.png) [Projection on surface](https://wiki.freecad.org/Part_ProjectionOnSurface): Projects a logo, text or any face, wire or edge onto a surface. 
* ![](PartModule_files/img_h16/Part_EditAttachment_h16.png) [Attachment](https://wiki.freecad.org/Part_EditAttachment): Attaches an object to another object.

### Boolean

These tools perform boolean operations.

* [Compound Tools](https://wiki.freecad.org/Part_CompCompoundTools):
    * ![](PartModule_files/img_h16/Part_Compound_h16.png) [Make compound](https://wiki.freecad.org/Part_Compound): Creates a compound from the selected objects.
    * ![](PartModule_files/img_h16/Part_ExplodeCompound_h16.png) [Explode Compound](https://wiki.freecad.org/Part_ExplodeCompound): Splits up compounds.
    * ![](PartModule_files/img_h16/Part_CompoundFilter_h16.png) [Compound Filter](https://wiki.freecad.org/Part_CompoundFilter): Extracts the individual pieces from compounds.
* ![](PartModule_files/img_h16/Part_Boolean_h16.png) [Boolean](https://wiki.freecad.org/Part_Boolean): Performs boolean operations on objects.
* ![](PartModule_files/img_h16/Part_Cut_h16.png) [Cut](https://wiki.freecad.org/Part_Cut): Cuts (subtracts) one object from another.
* ![](PartModule_files/img_h16/Part_Fuse_h16.png) [Fuse](https://wiki.freecad.org/Part_Fuse): Fuses (unions) two objects.
* ![](PartModule_files/img_h16/Part_Common_h16.png) [Common](https://wiki.freecad.org/Part_Common): Extracts the common (intersection) part of two objects.
* [Join features](https://wiki.freecad.org/Part_CompJoinFeatures):
    * ![](PartModule_files/img_h16/Part_JoinConnect_h16.png) [Connect](https://wiki.freecad.org/Part_JoinConnect): Connects interiors of walled objects.
    * ![](PartModule_files/img_h16/Part_JoinEmbed_h16.png) [Embed](https://wiki.freecad.org/Part_JoinEmbed): Embeds a walled object into another walled object.
    * ![](PartModule_files/img_h16/Part_JoinCutout_h16.png) [Cutout](https://wiki.freecad.org/Part_JoinCutout): Creates a cutout in a wall of an object for another walled object.
* [Splitting tools](https://wiki.freecad.org/Part_CompSplittingTools):
    * ![](PartModule_files/img_h16/Part_BooleanFragments_h16.png) [Boolean fragments](https://wiki.freecad.org/Part_BooleanFragments): Creates all pieces obtained from Boolean operations.
    * ![](PartModule_files/img_h16/Part_SliceApart_h16.png) [Slice a part](https://wiki.freecad.org/Part_SliceApart): Slices and splits an object by intersecting it with other objects.
    * ![](PartModule_files/img_h16/Part_Slice_h16.png) [Slice](https://wiki.freecad.org/Part_Slice): Slices an object by intersecting it with other objects.
    * ![](PartModule_files/img_h16/Part_XOR_h16.png) [XOR](https://wiki.freecad.org/Part_XOR): Removes space shared by an even number of objects (symmetric version of [Cut](https://wiki.freecad.org/Part_Cut)).

### Measure

[Measure](https://wiki.freecad.org/Part_Measure_Menu): Tools for linear and angular measurements.

* ![](PartModule_files/img_h16/Part_Measure_Linear_h16.png) [Measure Linear](https://wiki.freecad.org/Part_Measure_Linear): `Part_Measure_Linear` Creates a linear measurement.
* ![](PartModule_files/img_h16/Part_Measure_Angular_h16.png) [Measure Angular](https://wiki.freecad.org/Part_Measure_Angular): `Part_Measure_Angular` Creates an angular measurement.
* ![](PartModule_files/img_h16/Part_Measure_Refresh_h16.png) [Measure Refresh](https://wiki.freecad.org/Part_Measure_Refresh): `Part_Measure_Refresh` Updates all measurements.
* ![](PartModule_files/img_h16/Part_Measure_Clear_All_h16.png) [Clear All](https://wiki.freecad.org/Part_Measure_Clear_All): `Part_Measure_Clear_All` Clears all measurements.
* ![](PartModule_files/img_h16/Part_Measure_Toggle_All_h16.png) [Toggle All](https://wiki.freecad.org/Part_Measure_Toggle_All): `Part_Measure_Toggle_All` Shows or hides all measurements.
* ![](PartModule_files/img_h16/Part_Measure_Toggle_3d_h16.png) [Toggle 3D](https://wiki.freecad.org/Part_Measure_Toggle_3d): `Part_Measure_Toggle_3D` Shows or hides 3D measurements.
* ![](PartModule_files/img_h16/Part_Measure_Toggle_Delta_h16.png) [Toggle Delta](https://wiki.freecad.org/Part_Measure_Toggle_Delta): `Part_Measure_Toggle_Delta` Shows or hides delta measurements.

### Other tools

* ![](PartModule_files/img_h16/Part_Import_h16.png) [Import](https://wiki.freecad.org/Part_Import): Imports from \*.IGES, \*.STEP, or \*.BREP files.
* ![](PartModule_files/img_h16/Part_Export_h16.png) [Export](https://wiki.freecad.org/Part_Export): Exports to \*.IGES, \*.STEP, or \*.BREP files.
* ![](PartModule_files/img_h16/Part_BoxSelection_h16.png) [BoxSelection](https://wiki.freecad.org/Part_BoxSelection): Selects faces from a rectangular area.
* ![](PartModule_files/img_h16/Part_ShapeFromMesh_h16.png) [Shape from Mesh](https://wiki.freecad.org/Part_ShapeFromMesh): Creates a shape object from a mesh object.
* ![](PartModule_files/img_h16/Part_PointsFromMesh_h16.png) [Points from mesh](https://wiki.freecad.org/Part_PointsFromMesh): Creates a shape object made of points from a mesh object. 
* ![](PartModule_files/img_h16/Part_MakeSolid_h16.png) [Convert to solid](https://wiki.freecad.org/Part_MakeSolid): Converts a shape object to a solid object.
* ![](PartModule_files/img_h16/Part_ReverseShapes_h16.png) [Reverse shapes](https://wiki.freecad.org/Part_ReverseShapes): Flips the normals of all faces of selected objects.
* Create a copy:
    * ![](PartModule_files/img_h16/Part_SimpleCopy_h16.png) [Create simple copy](https://wiki.freecad.org/Part_SimpleCopy) Creates a simple copy of a selected object.
    * ![](PartModule_files/img_h16/Part_TransformedCopy_h16.png) [`Part_TransformedCopy`](https://wiki.freecad.org/Part_TransformedCopy) Creates a transformed copy of a selected object.
    * ![](PartModule_files/img_h16/Part_ElementCopy_h16.png) [Create shape element copy](https://wiki.freecad.org/Part_ElementCopy) Creates a copy from an element (vertex, edge, face) of a selected object.
    * ![](PartModule_files/img_h16/Part_RefineShape_h16.png) [`Part_RefineShape`](https://wiki.freecad.org/Part_RefineShape) Cleans faces by removing unnecessary lines.
* ![](PartModule_files/img_h16/Part_CheckGeometry_h16.png) [Check geometry](https://wiki.freecad.org/Part_CheckGeometry): Checks the geometry of selected objects for errors.
* ![](PartModule_files/img_h16/Part_Defeaturing_h16.png) [Defeaturing](https://wiki.freecad.org/Part_Defeaturing): Removes features from an object.

### Context Menu

* ![](PartModule_files/img_h16/Std_SetAppearance_h16.png) [Appearance](https://wiki.freecad.org/Std_SetAppearance): Determines the appearance of a whole object (color, transparency etc.).
* ![](PartModule_files/img_h16/Part_FaceColors_h16.png) [Set colors](https://wiki.freecad.org/Part_FaceColors): Assigns colors to individual faces of objects.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [OpenCascade ⇗](https://www.opencascade.com/), [wikipedia](https://en.wikipedia.org/wiki/Open_Cascade_Technology)
* [Import from STL or OBJ ⇗](https://wiki.freecad.org/Import_from_STL_or_OBJ) : How to import STL/OBJ files in FreeCAD
* [Export to STL or OBJ ⇗](https://wiki.freecad.org/Export_to_STL_or_OBJ) : How to export STL/OBJ files from FreeCAD
* [Whiffle Ball tutorial ⇗](https://wiki.freecad.org/Whiffle_Ball_tutorial) : How to use the Part Module

