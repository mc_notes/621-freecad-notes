# FreeCAD: ![](SurfaceWorkbench_files/img_h16/Workbench_Surface_h16.png) [Surface Workbench][t]
[t]:https://wiki.freecad.org/Surface_Workbench

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

The ![](SurfaceWorkbench_files/img_h16/Workbench_Surface_h16.png) Surface Workbench provides tools to create and modify simple <a href="https://en.wikipedia.org/wiki/Non-uniform_rational_B-spline" rel="nofollow">NURBS surfaces</a>. These tools have a similar functionality to the ![](SurfaceWorkbench_files/img_h16/Part_Builder_h16.png) [Part Builder](https://wiki.freecad.org/Part_Builder) tool when the **Face from edges** option is used. However, unlike that tool, the tools of the Surface Workbench are parametric and provide additional options. In this respect, the tools in this workbench are similar to ![](SurfaceWorkbench_files/img_h16/PartDesign_AdditiveLoft_h16.png) [PartDesign AdditiveLoft](https://wiki.freecad.org/PartDesign_AdditiveLoft) and ![](SurfaceWorkbench_files/img_h16/PartDesign_AdditivePipe_h16.png) [PartDesign AdditivePipe](https://wiki.freecad.org/PartDesign_AdditivePipe).

Some of the features provided are:

* Creation of surfaces from boundary edges.
* Alignment of the curvature from neighboring faces.
* Constraining of surfaces to additional curves and vertices.
* Extension of faces.
* A mesh can be used as a template to create spline curves on its surface.

![](SurfaceWorkbench_files/350px-Surface_example.png)

## Usage

The Surface Workbench intends to create faces with shapes, which is not possible to do with the standard tools in other workbenches.

![](SurfaceWorkbench_files/350px-Toy_Duck.png)

Surface created with sketches placed in datum planes with the tools of the [PartDesign Workbench](https://wiki.freecad.org/PartDesign_Workbench)

The Surface Workbench integrates with other workbenches of FreeCAD. The above example was created from ![](SurfaceWorkbench_files/img_h16/Sketcher_NewSketch_h16.png) [Sketches](https://wiki.freecad.org/Sketch) placed on ![](SurfaceWorkbench_files/img_h16/PartDesign_Plane_h16.png) [PartDesign Datum planes](https://wiki.freecad.org/PartDesign_Plane) in the ![](SurfaceWorkbench_files/img_h16/Workbench_PartDesign_h16.png) [PartDesign Workbench](https://wiki.freecad.org/PartDesign_Workbench). The design can be fully parametric if all datum planes and sketches are defined accordingly. In most cases it is sufficient to draw a closed sketch to define the boundary of a face, and then use different options to further modify its shape.

The generated surface cannot be placed inside a ![](SurfaceWorkbench_files/img_h16/PartDesign_Body_h16.png) [PartDesign Body](https://wiki.freecad.org/PartDesign_Body). However, the generated surface can be contained inside a ![](SurfaceWorkbench_files/img_h16/Std_Part_h16.png) [Std Part](https://wiki.freecad.org/Std_Part) together with the associated ![](SurfaceWorkbench_files/img_h16/PartDesign_Body_h16.png) [PartDesign Body](https://wiki.freecad.org/PartDesign_Body) that holds the datum planes and sketches. The non-parametric ![](SurfaceWorkbench_files/img_h16/Part_Builder_h16.png) [Part Builder](https://wiki.freecad.org/Part_Builder) tool can then be used in order to create a [shell](https://wiki.freecad.org/Glossary#Shell) and finally a [solid](https://wiki.freecad.org/Glossary#Solid).

## Tools

* [![](SurfaceWorkbench_files/img_h16/Surface_Filling_h16.png)](https://wiki.freecad.org/Surface_Filling) [Filling](https://wiki.freecad.org/Surface_Filling): fills a series of boundary curves with a surface.
* [![](SurfaceWorkbench_files/img_h16/Surface_GeomFillSurface_h16.png)](https://wiki.freecad.org/Surface_GeomFillSurface) [Fill boundary curves](https://wiki.freecad.org/Surface_GeomFillSurface): creates a surface from two, three or four boundary edges.
* [![](SurfaceWorkbench_files/img_h16/Surface_Sections_h16.png)](https://wiki.freecad.org/Surface_Sections) [Sections](https://wiki.freecad.org/Surface_Sections): creates a surface from edges that represent transversal sections of surface. 
* [![](SurfaceWorkbench_files/img_h16/Surface_ExtendFace_h16.png)](https://wiki.freecad.org/Surface_ExtendFace) [Extend face](https://wiki.freecad.org/Surface_ExtendFace): extrapolates the surface at the boundaries with its local U parameter and V parameter.
* [![](SurfaceWorkbench_files/img_h16/Surface_CurveOnMesh_h16.png)](https://wiki.freecad.org/Surface_CurveOnMesh) [Curve on mesh](https://wiki.freecad.org/Surface_CurveOnMesh): create approximated spline segments on top of a selected [mesh](https://wiki.freecad.org/Mesh_Workbench).

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

