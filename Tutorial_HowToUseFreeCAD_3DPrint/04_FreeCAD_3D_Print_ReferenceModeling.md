## Direct Reference Modeling <a id="direct-reference-modeling-"></a><sup>[▴](#contents)</sup>

* [10m28s] New Document. rename e.g. `Direct Reference Modeling` or `BoxReferenceModeling`

**Box**

* Add ![Std_Part_h16](04_FreeCAD_3D_Print_files/tools/Std_Part_h16.png) Part "Box", Add ![PartDesign_Body_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Body_h16.png) Body "Box_Body"
* Create a new ![Sketcher_NewSketch_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_NewSketch_h16.png) Sketch in the "Box_Body" with XY Plane view.
* Add ![Sketcher_CreateRectangle_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_CreateRectangle_h16.png) Rectangle (R) and center with equal constrained sides.
    * click two opposing outer corners, click center 
    * add ![Sketcher_ConstrainSymmetric_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainSymmetric_h16.png) Sketcher_ConstrainSymmetric (S)
    * click two adjacent sides
    * add ![Sketcher_ConstrainEqual_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainEqual_h16.png) Equal constraint (E)
    * add 70 mm ![Sketcher_ConstrainDistance_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainDistance_h16.png) length (distance) constraint to a side
    * close Sketcher tasks and rename to "Box_Base"
* ![PartDesign_Pad_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Pad_h16.png) Pad the "Box_Base" to 70 mm. <kbd>OK</kbd> Rename to "Box_Base_Pad"
* Select "Box_Base_Pad" top face. 
* Create a new ![Sketcher_NewSketch_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_NewSketch_h16.png) Sketch on this selected face. (v20 may need close and re-open the sketch to refresh view for box_body top to show.)
    * Repeat the above steps for a (S) symmetrically centered rectangle with (E) equal side constraints.
    * Zoom into upper right corner
    * Click ![](04_FreeCAD_3D_Print_files/tools/Sketcher_ViewSketch_h16.png) to ensure the model view is perpendicular to the sketch plane.
    * Rename Sketch to "Box_Hole".
    * ![PartDesign_Pocket_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Pocket_h16.png) Pocket this Sketch to 70 - 3 = 67 mm. Result will be an internal base height: 3.0 mm.
    * Rename pocket to bos hole pocket.

**Lid**

* Hide Box: Select "Box". View > ![Std_ToggleVisibility_h16](04_FreeCAD_3D_Print_files/tools/Std_ToggleVisibility_h16.png) Toggle visibility <kbd>space</kbd> 
* Add ![Std_Part_h16](04_FreeCAD_3D_Print_files/tools/Std_Part_h16.png) Part "Lid", Add ![PartDesign_Body_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Body_h16.png) Body "Lid_Body"
* Create a new ![Sketcher_NewSketch_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_NewSketch_h16.png) Sketch in the "Lid_Body" with XY Plane view.
* Add ![Sketcher_CreateRectangle_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_CreateRectangle_h16.png) Rectangle (R) and center with equal constrained sides.
    * click two opposing outer corners, click center 
    * add ![Sketcher_ConstrainSymmetric_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainSymmetric_h16.png) Sketcher_ConstrainSymmetric (S)
    * click two adjacent sides
    * add ![Sketcher_ConstrainEqual_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainEqual_h16.png) Equal constraint (E)
    * add 70 mm ![Sketcher_ConstrainDistance_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainDistance_h16.png) length (distance) constraint to a side
    * close Sketcher tasks and rename to "Lid_Base"
* ![PartDesign_Pad_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Pad_h16.png) Pad the "Lid_Base" to 3 mm. <kbd>OK</kbd> Rename to "Lid_Base_Pad"
* Select "Lid_Base_Pad" top face in the 3D view. 
* Create a new ![Sketcher_NewSketch_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_NewSketch_h16.png) Sketch on this selected face.
    * NOTE: On version 0.20, may need to close and reopen Sketch to see the "Lid_Base" in Sketch. {Refresh issue?}
    * Repeat the above steps for a centered rectangle which is 70 -1.5 -1.5 -0.4 -0.4 = 66.2 mm.
        * 0.4 on each side is used add some 3D printing tolerance
        * Result will be wall thickness: 1.5.
    * Rename Sketch to "Lid_Lip"
    * ![PartDesign_Pad_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Pad_h16.png) Pad the "Lid_Lip" to 3 mm.
