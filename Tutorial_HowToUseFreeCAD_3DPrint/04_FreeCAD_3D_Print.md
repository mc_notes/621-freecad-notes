# Maker Tales: [How To Use FreeCAD][t]<br>3D Printing (part 4/5)
[t]:https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN

[<< Return to 621-freecad-notes/README.md](README.md)

_The ["FreeCAD Project For 3D Printing" video](https://www.youtube.com/watch?v=AntQAnDFKC8) shows how to create a 3D print design in FreeCAD using "pure direct" modeling and "direct reference" modeling._

> _NOTE: The ["How To Use FreeCAD For Beginners … For 3D Printing" series](https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN) is based on the _Stable_ version of the FreeCAD RealThunder branch. The notes below have been checked on macOS 11.6 (Big Sur) with the FreeCAD pre-release 0.20.26498 and FreeCAD LinkStage3 (RealThunder) branch version: 2021.1015.24301. See ["Resources" section](#resources-) below._

## Contents <a id="contents"></a>
[Pure Direct Modeling](#pure-direct-modeling-) •
[Direct Reference Modeling](#direct-reference-modeling-) •
[STL Export Workflow](#stl-export-workflow-)
[Time Stamps](#time-stamps-) •
[Resources](#resources-)

## Pure Direct Modeling <a id="pure-direct-modeling-"></a><sup>[▴](#contents)</sup>

**Box**

* New Document. rename e.g. `BoxDirectModeling`
* Add ![Std_Part_h16](04_FreeCAD_3D_Print_files/tools/Std_Part_h16.png) Part "Box", Add ![PartDesign_Body_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Body_h16.png) Body "Box_Body"
* Create a new ![Sketcher_NewSketch_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_NewSketch_h16.png) Sketch in the "Box_Body" with XY Plane view.
* Add ![Sketcher_CreateRectangle_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_CreateRectangle_h16.png) Rectangle (R) and center with equal constrained sides.
    * click two opposing outer corners, click center 
    * add ![Sketcher_ConstrainSymmetric_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainSymmetric_h16.png) Sketcher_ConstrainSymmetric (S)
    * click two adjacent sides
    * add ![Sketcher_ConstrainEqual_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainEqual_h16.png) Equal constraint (E)
    * add 70 mm ![Sketcher_ConstrainDistance_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainDistance_h16.png) length (distance) constraint to a side
    * close Sketcher tasks and rename to "Box_Base"
* ![PartDesign_Pad_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Pad_h16.png) Pad the "Box_Base" to 70 mm. <kbd>OK</kbd> Rename to "Box_Base_Pad"
* Select "Box_Base_Pad" top face. 
* Create a new ![Sketcher_NewSketch_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_NewSketch_h16.png) Sketch on this selected face.
    * Repeat the above steps for a centered rectangle which is 70 - 1.5 - 1.5 = 67 mm.
    * Result will be wall thickness: 1.5.
    * Rename Sketch to "Box_Hole"
    * ![PartDesign_Pocket_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Pocket_h16.png) Pocket this Sketch to 70 - 3 = 67 mm. Result will be an internal base height: 3.0 mm.
    * Rename pocket to bos hole pocket.

**Lid**

* Hide Box: Select "Box". View > ![Std_ToggleVisibility_h16](04_FreeCAD_3D_Print_files/tools/Std_ToggleVisibility_h16.png) Toggle visibility <kbd>space</kbd> 
* Add ![Std_Part_h16](04_FreeCAD_3D_Print_files/tools/Std_Part_h16.png) Part "Lid", Add ![PartDesign_Body_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Body_h16.png) Body "Lid_Body"
* Create a new ![Sketcher_NewSketch_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_NewSketch_h16.png) Sketch in the "Lid_Body" with XY Plane view.
* Add ![Sketcher_CreateRectangle_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_CreateRectangle_h16.png) Rectangle (R) and center with equal constrained sides.
    * click two opposing outer corners, click center 
    * add ![Sketcher_ConstrainSymmetric_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainSymmetric_h16.png) Sketcher_ConstrainSymmetric (S)
    * click two adjacent sides
    * add ![Sketcher_ConstrainEqual_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainEqual_h16.png) Equal constraint (E)
    * add 70 mm ![Sketcher_ConstrainDistance_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_ConstrainDistance_h16.png) length (distance) constraint to a side
    * close Sketcher tasks and rename to "Lid_Base"
* ![PartDesign_Pad_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Pad_h16.png) Pad the "Lid_Base" to 3 mm. <kbd>OK</kbd> Rename to "Lid_Base_Pad"
* Select "Lid_Base_Pad" top face in the 3D view. 
* Create a new ![Sketcher_NewSketch_h16](04_FreeCAD_3D_Print_files/tools/Sketcher_NewSketch_h16.png) Sketch on this selected face.
    * NOTE: On version 0.20, may need to close and reopen Sketch to see the "Lid_Base" in Sketch. {Refresh issue?}
    * Repeat the above steps for a centered rectangle which is 70 -1.5 -1.5 -0.4 -0.4 = 66.2 mm.
        * 0.4 on each side is used add some 3D printing tolerance
        * Result will be wall thickness: 1.5.
    * Rename Sketch to "Lid_Lip"
    * ![PartDesign_Pad_h16](04_FreeCAD_3D_Print_files/tools/PartDesign_Pad_h16.png) Pad the "Lid_Lip" to 3 mm.

8m16s  (-14.15)

## Direct Reference Modeling <a id="direct-reference-modeling-"></a><sup>[▴](#contents)</sup>

* [10m28s] New Document. rename e.g. `Direct Reference Modeling` or `BoxReferenceModeling`

## STL Export Workflow <a id="stl-export-workflow-"></a><sup>[▴](#contents)</sup>

* Go to the [Mesh Design Workbench](https://wiki.freecadweb.org/Mesh_Workbench)
* Select `Part`
* Click `Mesh_FromPartShape`
    * Surface deviation: 0.001 (smallest possible, 1 nanometer)
    * Angle deviation: 1.00°
* Select "Part_Name (Meshed)"
* Click `Mesh_Export` 
    * Binary STL (*.stl)

* 

*multi-part union*

* optionally, create a `Std_Group` folder for the Part Links
* `Std_LinkMake` for each part
    * right-click "Part Link" > Transform to move into position
* `Part` Workbench
    * cmd-click select multiple part links
    * `Part_Fuse` Union
* `Mesh` Workbench
    * select the "Fusion" of Part Links
    * create `Mesh_FromPartShape`
    * `Mesh_Export` to STL

## Time Stamps <a id="time-stamps-"></a><sup>[▴](#contents)</sup>

* 0:00 - Intro.
* 00:28 - Quick Recap &  What This Video Is All About.
* 02:09 - Pure Direct Modeling The Box.
* 06:09 - Pure Direct Modeling The Lid.
* 09:34 - Pure Direct Modeling Part Moving.
* 10:00 - Thangs Sponsorship Segment.
* 10:28 - Direct Reference Modelling The Box.
* 12:53 - Direct Reference Modelling The Lid.
* 17:09 - Direct Reference Modelling Part Moving.
* 18:47 - Preparing & Exporting A Mesh For 3D Printing.
* 21:19 - Slicer Review.
* 22:00 - Closing Notes.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [2021.08.27 Your First FreeCAD Project For 3D Printing | The Realthunder Branch | Direct Modeling | P. 4 ⇗](https://www.youtube.com/watch?v=AntQAnDFKC8)
* [FreeCAD: weekly builds ⇗](https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds)
* [GitHub/realthunder: FreeCAD releases ⇗](https://github.com/realthunder/FreeCAD_assembly3/releases)
* [GitLab: 621-FreeCAD-Notes ⇗](https://gitlab.com/mc_qref/621-freecad-notes) / [README.md ⇗](https://gitlab.com/mc_qref/621-freecad-notes/-/blob/main/README.md)
