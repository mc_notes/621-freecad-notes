# FreeCAD: ![](ImageWorkbench_files/img_h16/Workbench_Image_h16.png) [Image Workbench][t]
[t]:https://wiki.freecad.org/Image_Workbench

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Resources](#resources-)

* [1 Introduction](#Introduction)
* [2 Tools](#Tools)
* [3 Features](#Features)
* [4 Workflow](#Workflow)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

The ![](ImageWorkbench_files/img_h16/Workbench_Image_h16.png) Image Workbench manages different types of [bitmap](https://wiki.freecad.org/Bitmap) images, and allows you to open them in FreeCAD.

## Tools

* ![](ImageWorkbench_files/img_h16/Image_Open_h16.png) [Open...](https://wiki.freecad.org/Image_Open): open an image on a new viewport.
* ![](ImageWorkbench_files/img_h16/Image_CreateImagePlane_h16.png) [Create image plane...](https://wiki.freecad.org/Image_CreateImagePlane): import an image to a plane in the 3D view.
* ![](ImageWorkbench_files/img_h16/Image_Scaling_h16.png) [Scale image plane](https://wiki.freecad.org/Image_Scaling): scale an image imported to a plane.

## Features

* Like a [Sketch](https://wiki.freecad.org/Sketcher_Workbench), an imported image can be attached to one of the main planes XY, XZ, or YZ, and given a positive or negative offset.
* The image is imported with relation of 1 pixel to 1 millimeter.
* The recommendation is to have the imported image at a reasonable resolution.

## Workflow

A major use of this workbench is tracing over the image, with the [Draft](https://wiki.freecad.org/Draft_Workbench) or [Sketcher](https://wiki.freecad.org/Sketcher_Workbench) tools, in order to generate a solid body based on the contours of the image.

Tracing over an image works best if the image has a small negative offset, for example, -0.1 mm, from the working plane. This means that the image will be slightly behind the plane where you draw your 2D geometry, so you won't draw on the image itself.

The offset of the image can be set during import, or changed later through its properties.

  

![](ImageWorkbench_files/img_h16/Arrow-left_h16.png) Previous: ![](ImageWorkbench_files/img_h16/Workbench_FEM_h16.png) [FEM Workbench](https://wiki.freecad.org/FEM_Workbench)

Next: [Inspection Workbench](https://wiki.freecad.org/Inspection_Workbench) ![](ImageWorkbench_files/img_h16/Workbench_Inspection_h16.png) ![](ImageWorkbench_files/img_h16/Arrow-right_h16.png)

[Index](https://wiki.freecad.org/Online_Help_Toc) ![](ImageWorkbench_files/img_h16/Online_Help_Toc_h16.png)

Expand![](ImageWorkbench_files/img_h16/Workbench_Image_h16.png) Image

* **Tools:** [Open](https://wiki.freecad.org/Image_Open), [Image plane](https://wiki.freecad.org/Image_CreateImagePlane), [Scaling](https://wiki.freecad.org/Image_Scaling)

------------------------------------------------------------------------

* **Additional:** [Bitmap](https://wiki.freecad.org/Bitmap)

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

