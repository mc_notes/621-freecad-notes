# FreeCAD: ![](_StdBase_files/img_h16/Freecad_h16.png) [Std Base][t]
[t]:https://wiki.freecad.org/Std_Base

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Menus](#menus-) •
[Menus, Contextual](#menus-contextual-) •
[Toolbar](#toolbar-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

**Std Base** is a category of 'standard' commands and tools that can be used in all workbenches.

## Menus <a id="menus-"></a><sup>[▴](#contents)</sup>

**[File](https://wiki.freecad.org/Std_File_Menu)**

* [Import](https://wiki.freecad.org/Std_Import)
* [Export](https://wiki.freecad.org/Std_Export)
* [Merge project](https://wiki.freecad.org/Std_MergeProjects)
* [Project information](https://wiki.freecad.org/Std_ProjectInfo)
* [Print](https://wiki.freecad.org/Std_Print)
* [Print preview](https://wiki.freecad.org/Std_PrintPreview)

**[Edit](https://wiki.freecad.org/Std_Edit_Menu):**

_[Undo](https://wiki.freecad.org/Std_Undo), [Redo](https://wiki.freecad.org/Std_Redo)_


_[Cut](https://wiki.freecad.org/Std_Cut), [Copy](https://wiki.freecad.org/Std_Copy), [Paste](https://wiki.freecad.org/Std_Paste)_

_[Duplicate selection](https://wiki.freecad.org/Std_DuplicateSelection)_


* [Issue #6278 - [bug/crash] Duplicate object results in <Exception> Object can only be in a single GeoFeatureGroup](https://forum.freecad.org/viewtopic.php?t=44534)
* [GitHub: Duplicate object results in <Exception> Object can only be in a single GeoFeatureGroup #6278](https://github.com/FreeCAD/FreeCAD/issues/6278)

[Refresh](https://wiki.freecad.org/Std_Refresh), [Box selection](https://wiki.freecad.org/Std_BoxSelection), [Box element selection](https://wiki.freecad.org/Std_BoxElementSelection), [Select All](https://wiki.freecad.org/Std_SelectAll), [Delete](https://wiki.freecad.org/Std_Delete), [Send to Python Console](https://wiki.freecad.org/Std_SendToPythonConsole), [Placement](https://wiki.freecad.org/Std_Placement), [Transform](https://wiki.freecad.org/Std_TransformManip), [Alignment](https://wiki.freecad.org/Std_Alignment), [Toggle Edit mode](https://wiki.freecad.org/Std_Edit), [Edit mode](https://wiki.freecad.org/Std_UserEditMode), [Preferences](https://wiki.freecad.org/Std_DlgPreferences)

**[View](https://wiki.freecad.org/Std_View_Menu):**

* **Miscellaneous:** [Create new view](https://wiki.freecad.org/Std_ViewCreate), [Orthographic view](https://wiki.freecad.org/Std_OrthographicCamera), [Perspective view](https://wiki.freecad.org/Std_PerspectiveCamera), [Fullscreen](https://wiki.freecad.org/Std_MainFullscreen), [Bounding box](https://wiki.freecad.org/Std_SelBoundingBox), [Toggle axis cross](https://wiki.freecad.org/Std_AxisCross), [Clipping plane](https://wiki.freecad.org/Std_ToggleClipPlane), [Texture mapping](https://wiki.freecad.org/Std_TextureMapping), [Toggle navigation/Edit mode](https://wiki.freecad.org/Std_ToggleNavigation), [Appearance](https://wiki.freecad.org/Std_SetAppearance), [Random color](https://wiki.freecad.org/Std_RandomColor), [Workbench](https://wiki.freecad.org/Std_Workbench), [Status bar](https://wiki.freecad.org/Std_ViewStatusBar)
* **Standard views:** [Fit all](https://wiki.freecad.org/Std_ViewFitAll), [Fit selection](https://wiki.freecad.org/Std_ViewFitSelection), [Isometric](https://wiki.freecad.org/Std_ViewIsometric), [Dimetric](https://wiki.freecad.org/Std_ViewDimetric), [Trimetric](https://wiki.freecad.org/Std_ViewTrimetric), [Home](https://wiki.freecad.org/Std_ViewHome), [Front](https://wiki.freecad.org/Std_ViewFront), [Top](https://wiki.freecad.org/Std_ViewTop), [Right](https://wiki.freecad.org/Std_ViewRight), [Rear](https://wiki.freecad.org/Std_ViewRear), [Bottom](https://wiki.freecad.org/Std_ViewBottom), [Left](https://wiki.freecad.org/Std_ViewLeft), [Rotate Left](https://wiki.freecad.org/Std_ViewRotateLeft), [Rotate Right](https://wiki.freecad.org/Std_ViewRotateRight)
* **[Freeze display](https://wiki.freecad.org/Std_FreezeViews):** [Save views](https://wiki.freecad.org/Std_FreezeViews#Save_views), [Load views](https://wiki.freecad.org/Std_FreezeViews#Load_views), [Freeze view](https://wiki.freecad.org/Std_FreezeViews#Freeze_view), [Clear views](https://wiki.freecad.org/Std_FreezeViews#Clear_views)
* **[Draw style](https://wiki.freecad.org/Std_DrawStyle):** [As is](https://wiki.freecad.org/Std_DrawStyle#As_is), [Points](https://wiki.freecad.org/Std_DrawStyle#Points), [Wireframe](https://wiki.freecad.org/Std_DrawStyle#Wireframe), [Hidden line](https://wiki.freecad.org/Std_DrawStyle#Hidden_line), [No shading](https://wiki.freecad.org/Std_DrawStyle#No_shading), [Shaded](https://wiki.freecad.org/Std_DrawStyle#Shaded), [Flat lines](https://wiki.freecad.org/Std_DrawStyle#Flat_lines)
* **Stereo:** [Stereo red/cyan](https://wiki.freecad.org/Std_ViewIvStereoRedGreen), [Stereo quad buffer](https://wiki.freecad.org/Std_ViewIvStereoQuadBuff), [Stereo Interleaved Rows](https://wiki.freecad.org/Std_ViewIvStereoInterleavedRows), [Stereo Interleaved Columns](https://wiki.freecad.org/Std_ViewIvStereoInterleavedColumns), [Stereo Off](https://wiki.freecad.org/Std_ViewIvStereoOff), [Issue camera position](https://wiki.freecad.org/Std_ViewIvIssueCamPos)
* **Zoom:** [Zoom In](https://wiki.freecad.org/Std_ViewZoomIn), [Zoom Out](https://wiki.freecad.org/Std_ViewZoomOut), [Box zoom](https://wiki.freecad.org/Std_ViewBoxZoom)
* **Document window:** [Docked](https://wiki.freecad.org/Std_ViewDockUndockFullscreen#Docked), [Undocked](https://wiki.freecad.org/Std_ViewDockUndockFullscreen#Undocked), [Fullscreen](https://wiki.freecad.org/Std_ViewFullscreen)
* **Visibility:** [Toggle visibility](https://wiki.freecad.org/Std_ToggleVisibility), [Show selection](https://wiki.freecad.org/Std_ShowSelection), [Hide selection](https://wiki.freecad.org/Std_HideSelection), [Select visible objects](https://wiki.freecad.org/Std_SelectVisibleObjects), [Toggle all objects](https://wiki.freecad.org/Std_ToggleObjects), [Show all objects](https://wiki.freecad.org/Std_ShowObjects), [Hide all objects](https://wiki.freecad.org/Std_HideObjects), [Toggle selectability](https://wiki.freecad.org/Std_ToggleSelectability), [Toggle measurement](https://wiki.freecad.org/View_Measure_Toggle_All), [Clear measurement](https://wiki.freecad.org/View_Measure_Clear_All)
* **Toolbars:** File, Workbench, Macro, View, Structure
* **Panels:** [Report view](https://wiki.freecad.org/Report_view), [Tree view](https://wiki.freecad.org/Tree_view), [Property view](https://wiki.freecad.org/Property_editor), [Selection view](https://wiki.freecad.org/Selection_view), [Combo view](https://wiki.freecad.org/Combo_view), [Python console](https://wiki.freecad.org/Python_console), [DAG view](https://wiki.freecad.org/DAG_view)
* **Tree view actions:** [Sync view](https://wiki.freecad.org/Std_TreeSyncView), [Sync selection](https://wiki.freecad.org/Std_TreeSyncSelection), [Sync placement](https://wiki.freecad.org/Std_TreeSyncPlacement), [Pre-selection](https://wiki.freecad.org/Std_TreePreSelection), [Record selection](https://wiki.freecad.org/Std_TreeRecordSelection), [Single document](https://wiki.freecad.org/Std_TreeSingleDocument), [Multi document](https://wiki.freecad.org/Std_TreeMultiDocument), [Collapse/Expand](https://wiki.freecad.org/Std_TreeCollapseDocument), [Initiate dragging](https://wiki.freecad.org/Std_TreeDrag), [Go to selection](https://wiki.freecad.org/Std_TreeSelection)


**[Tools](https://wiki.freecad.org/Std_Tools_Menu):** 

* [Edit parameters](https://wiki.freecad.org/Std_DlgParameter)
* [Save picture](https://wiki.freecad.org/Std_ViewScreenShot)
* [Scene inspector](https://wiki.freecad.org/Std_SceneInspector)
* [Dependency graph](https://wiki.freecad.org/Std_DependencyGraph)
* [Project utility](https://wiki.freecad.org/Std_ProjectUtil)
* [Measure distance](https://wiki.freecad.org/Std_MeasureDistance) `Std_MeasureDistance`
    * creates linear measure & display distance object between two 3D-view points
    * see `Parts` (and `Part Design`) workbench for more measurement tools 
* [Add text document](https://wiki.freecad.org/Std_TextDocument)
* [View turntable](https://wiki.freecad.org/Std_DemoMode)
* [Units calculator](https://wiki.freecad.org/Std_UnitsCalculator)
* [Customize](https://wiki.freecad.org/Std_DlgCustomize)
* [Addon manager](https://wiki.freecad.org/Std_AddonMgr)

**[Macro](https://wiki.freecad.org/Std_Macro_Menu)**

**[Windows](https://wiki.freecad.org/Std_Windows_Menu)**

**[Help](https://wiki.freecad.org/Std_Help_Menu)** 

* ![](_StdBase_files/img_h16/Std_WhatsThis_h16.png) [`Std WhatsThis`](https://wiki.freecad.org/Std_WhatsThis) opens the help documentation for a specified command.
* Install "Help" addon system via the Addons Manager
    * work-in-progress module aimed at replacing the entire FreeCAD Help system

## Links <a id="links-"></a><sup>[▴](#contents)</sup>

**App Link**

* object that references/links another object in same document or another document.
    * `?` what breaks link? moving referenced file? renaming referenced file?
* designed to efficiently duplicate a single object multiple times
* use case: multiple reusable components like screws, nuts, and similar fasteners


## Menus, Contextual <a id="menus-contextual-"></a><sup>[▴](#contents)</sup>

:WIP:

## Toolbar <a id="tools-"></a><sup>[▴](#contents)</sup>

* ![](_StdBase_files/img_h16/Std_Part_h16.png) `Std_Part` [Create part](https://wiki.freecad.org/Std_Part): Creates a new part and makes it active.
    * container to group objects to be moved together
    * basic building block for mechanical assemblies
    * 
* ![](_StdBase_files/img_h16/Std_Group_h16.png) `Std_Group` [Create group](https://wiki.freecad.org/Std_Group): Creates a new group.
* ![](_StdBase_files/img_h16/Std_LinkMake_h16.png) `Std_LinkMake` [Make link](https://wiki.freecad.org/Std_LinkMake): Creates a link.
* Link Actions 
    * ![](_StdBase_files/img_h16/Std_LinkMakeRelative_h16.png) `Std_LinkMakeRelative` [Make sub-link](https://wiki.freecad.org/Std_LinkMakeRelative): Creates a sub-object or sub-element link. 
    * ![](_StdBase_files/img_h16/Std_LinkReplace_h16.png) `Std_LinkReplace` [Replace with link](https://wiki.freecad.org/Std_LinkReplace): Replaces object(s) with new link(s). 
    * ![](_StdBase_files/img_h16/Std_LinkUnlink_h16.png) `Std_LinkUnlink` [Unlink](https://wiki.freecad.org/Std_LinkUnlink): Replaces link(s) with their linked object(s). 
    * ![](_StdBase_files/img_h16/Std_LinkImport_h16.png) `Std_LinkImport` [Import link](https://wiki.freecad.org/Std_LinkImport): Imports selected external link(s). 
    * ![](_StdBase_files/img_h16/Std_LinkImportAll_h16.png) `Std_LinkImportAll` [Import all links](https://wiki.freecad.org/Std_LinkImportAll): Imports all external link(s). 
* ![](_StdBase_files/img_h16/Std_LinkMakeGroup_h16.png) `Std_LinkMakeGroup` [Make link group](https://wiki.freecadweb.org/Std_LinkMakeGroup): Creates a group of links.
* Link Object Actions
    * ![](_StdBase_files/img_h16/Std_LinkSelectLinked_h16.png) [Go to linked object](https://wiki.freecad.org/Std_LinkSelectLinked): Selects the linked object and switches to its document. 
    * ![](_StdBase_files/img_h16/Std_LinkSelectLinkedFinal_h16.png) [Go to deepest linked object](https://wiki.freecad.org/Std_LinkSelectLinkedFinal): Selects the deepest linked object and switches to its document. 
    * ![](_StdBase_files/img_h16/Std_LinkSelectAllLinks_h16.png) [Select all links](https://wiki.freecad.org/Std_LinkSelectAllLinks): Selects all links to an object. 
* ![](_StdBase_files/img_h16/Std_TreeSelectAllInstances_h16.png) `Std_TreeSelectAllInstances` [Select all instances](https://wiki.freecadweb.org/Std_TreeSelectAllInstances): Selects all instances of an object. 
* ![](_StdBase_files/img_h16/Std_SelBack_h16.png) `Std_SelBack` [Back](https://wiki.freecad.org/Std_SelBack): Restores the previous saved selection.
* ![](_StdBase_files/img_h16/Std_SelForward_h16.png) `Std_SelForward` [Forward](https://wiki.freecad.org/Std_SelForward): Restores the next saved selection. 

------------------------------------------------------------------------

* **Additional:** [Restore previous selection](https://wiki.freecad.org/Std_SelBack), [Restore next selection](https://wiki.freecad.org/Std_SelForward)

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* FreeCAD release notes: [version 0.19 ⇗](https://wiki.freecad.org/Release_notes_0.19), [version 0.20 ⇗](https://wiki.freecad.org/Release_notes_0.20)
* [FreeCAD: User documentation hub ⇗](https://wiki.freecad.org/User_hub)
    * [App Link ⇗](https://wiki.freecad.org/App_Link) `App::Link`
    * [Std Base ⇗](https://wiki.freecad.org/Std_Base)
    * [Std LinkMake ⇗](https://wiki.freecad.org/Std_LinkMake)

