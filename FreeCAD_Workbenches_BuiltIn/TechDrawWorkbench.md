# FreeCAD: ![](TechDrawWorkbench_files/img_h16/Workbench_TechDraw_h16.png) [TechDraw Workbench][t]
[t]:https://wiki.freecad.org/TechDraw_Workbench

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

The ![](TechDrawWorkbench_files/img_h16/Workbench_TechDraw_h16.png) TechDraw Workbench is used to produce basic technical drawings from 3D models created with another workbench such as [Part](https://wiki.freecad.org/Part_Workbench), [PartDesign](https://wiki.freecad.org/PartDesign_Workbench), or [Arch](https://wiki.freecad.org/Arch_Workbench), or imported from other applications. Each drawing is a Page, which can contain various Views of drawable objects such as Part::Features, PartDesign::Bodies, App::Part groups, and Document Object groups. The resulting drawings can be used for things like documentation, manufacturing instructions, contracts, permits, etc.

Dimensions, sections, hatched areas, annotations, and [SVG](https://wiki.freecad.org/SVG) symbols can be added to the page, which can be further exported to different formats like [DXF](https://wiki.freecad.org/DXF), [SVG](https://wiki.freecad.org/SVG), and [PDF](https://wiki.freecad.org/PDF).

TechDraw was officially included in FreeCAD starting with version 0.17; it is intended to replace the unsupported [Drawing Workbench](https://wiki.freecad.org/Drawing_Workbench). Both workbenches are still provided in v0.17, but the Drawing Workbench may be removed in future releases. To keep up with TechDraw plans and developments, visit the [TechDraw Roadmap](https://wiki.freecad.org/TechDraw_Roadmap).

If your primary goal is the production of complex 2D drawings and [DXF](https://wiki.freecad.org/DXF) files, and you don't need 3D modelling, FreeCAD may not be the right choice for you. You may wish to consider a dedicated software program for technical drafting instead, such as [LibreCAD](https://en.wikipedia.org/wiki/LibreCAD) or [QCad](https://en.wikipedia.org/wiki/QCad).

![](TechDrawWorkbench_files/600px-TechDraw_Workbench_Example.png)

## Pages

These are tools for creating Page objects.

* ![](TechDrawWorkbench_files/img_h16/TechDraw_PageDefault_h16.png) [Insert Default Page](https://wiki.freecad.org/TechDraw_PageDefault): adds a new page using the default [template](https://wiki.freecad.org/TechDraw_Templates).
* ![](TechDrawWorkbench_files/img_h16/TechDraw_PageTemplate_h16.png) [Insert Page using Template](https://wiki.freecad.org/TechDraw_PageTemplate): adds a new page using a selected [template](https://wiki.freecad.org/TechDraw_Templates).
* ![](TechDrawWorkbench_files/img_h16/TechDraw_RedrawPage_h16.png) [Redraw Page](https://wiki.freecad.org/TechDraw_RedrawPage): forces an update of the selected page. 

## Views

These are tools for creating View objects.

* ![](TechDrawWorkbench_files/img_h16/TechDraw_View_h16.png) [Insert View](https://wiki.freecad.org/TechDraw_View): adds a 2D projection view of an object.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ActiveView_h16.png) [Insert Active View](https://wiki.freecad.org/TechDraw_ActiveView): inserts a view of the active 3D view. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ProjectionGroup_h16.png) [Insert Projection Group](https://wiki.freecad.org/TechDraw_ProjectionGroup): invokes a dialog to create many views of an object from multiple directions.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_SectionView_h16.png) [Insert Section View](https://wiki.freecad.org/TechDraw_SectionView): inserts a cross-section view of an existing view.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_DetailView_h16.png) [Insert Detail View](https://wiki.freecad.org/TechDraw_DetailView): inserts a detail view of a portion of an existing view.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_DraftView_h16.png) [Insert Draft Workbench Object](https://wiki.freecad.org/TechDraw_DraftView): inserts a view of a [Draft Workbench](https://wiki.freecad.org/Draft_Workbench) object.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ArchView_h16.png) [Insert Arch Workbench Object](https://wiki.freecad.org/TechDraw_ArchView): inserts a view of an [Arch Workbench](https://wiki.freecad.org/Arch_Workbench) [Section Plane](https://wiki.freecad.org/Arch_SectionPlane) object.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_SpreadsheetView_h16.png) [Insert Spreadsheet View](https://wiki.freecad.org/TechDraw_SpreadsheetView): inserts a view of a [Spreadsheet Workbench](https://wiki.freecad.org/Spreadsheet_Workbench) sheet.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_MoveView_h16.png) [Move View](https://wiki.freecad.org/TechDraw_MoveView): Moves a view and its dependents to a different page. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ShareView_h16.png) [Share View](https://wiki.freecad.org/TechDraw_ShareView): Share a view between multiple pages. 

## Clips

These are tools to create and manage Clip objects (clipped views).

* ![](TechDrawWorkbench_files/img_h16/TechDraw_ClipGroup_h16.png) [Insert Clip Group](https://wiki.freecad.org/TechDraw_ClipGroup): inserts a clip group into a page.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ClipGroupAdd_h16.png) [Add View to Clip Group](https://wiki.freecad.org/TechDraw_ClipGroupAdd): adds an existing view to a clip group.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ClipGroupRemove_h16.png) [Remove View from Clip Group](https://wiki.freecad.org/TechDraw_ClipGroupRemove): removes a view from a clip group.

## Decorations

These are tools to decorate pages or views:

* ![](TechDrawWorkbench_files/img_h16/TechDraw_Hatch_h16.png) [Hatch Face using Image File](https://wiki.freecad.org/TechDraw_Hatch): applies a hatch pattern from a file to a face.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_GeometricHatch_h16.png) [Apply Geometric Hatch to Face](https://wiki.freecad.org/TechDraw_GeometricHatch): applies a hatch pattern to a face using an Autodesk PAT specification.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_Symbol_h16.png) [Insert SVG Symbol](https://wiki.freecad.org/TechDraw_Symbol): inserts a symbol from a [SVG](https://wiki.freecad.org/SVG) file into a page.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_Image_h16.png) [Insert Bitmap Image](https://wiki.freecad.org/TechDraw_Image): inserts a PNG or JPG [bitmap](https://wiki.freecad.org/Bitmap) image into a page.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ToggleFrame_h16.png) [Turn View Frames On/Off](https://wiki.freecad.org/TechDraw_ToggleFrame): turns on/off frames and labels surrounding a view.

## Dimensions

These are tools for creating and working with Dimension objects.

Linear dimensions can be based on two points, on one line, or on two lines.

* ![](TechDrawWorkbench_files/img_h16/TechDraw_LengthDimension_h16.png) [Insert Length Dimension](https://wiki.freecad.org/TechDraw_LengthDimension): adds a length dimension.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_HorizontalDimension_h16.png) [Insert Horizontal Dimension](https://wiki.freecad.org/TechDraw_HorizontalDimension): adds a horizontal length dimension.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_VerticalDimension_h16.png) [Insert Vertical Dimension](https://wiki.freecad.org/TechDraw_VerticalDimension): adds a vertical length dimension.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_RadiusDimension_h16.png) [Insert Radius Dimension](https://wiki.freecad.org/TechDraw_RadiusDimension): adds a radius dimension to a circle or circular arc.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_DiameterDimension_h16.png) [Insert Diameter Dimension](https://wiki.freecad.org/TechDraw_DiameterDimension): adds a diameter dimension to a circle or a circular arc.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_AngleDimension_h16.png) [Insert Angle Dimension](https://wiki.freecad.org/TechDraw_AngleDimension): adds an angle dimension between two straight edges.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_3PtAngleDimension_h16.png) [Insert 3-Point Angle Dimension](https://wiki.freecad.org/TechDraw_3PtAngleDimension): adds an angle dimension using three vertices.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_HorizontalExtentDimension_h16.png) [Insert Horizontal Extent Dimension](https://wiki.freecad.org/TechDraw_HorizontalExtentDimension): adds a horizontal extent dimension. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_VerticalExtentDimension_h16.png) [Insert Vertical Extent Dimension](https://wiki.freecad.org/TechDraw_VerticalExtentDimension): adds a vertical extent dimension. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_LinkDimension_h16.png) [Link Dimension to 3D Geometry](https://wiki.freecad.org/TechDraw_LinkDimension): links an existing dimension to the 3D geometry.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_Balloon_h16.png) [Insert Balloon Annotation](https://wiki.freecad.org/TechDraw_Balloon): adds a "balloon" annotation to a page. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_LandmarkDimension_h16.png) [Insert Landmark Dimension](https://wiki.freecad.org/TechDraw_LandmarkDimension): adds a landmark distance dimension. 

## Annotations

The annotation tools are for "marking up" a drawing with additional information.

* ![](TechDrawWorkbench_files/img_h16/TechDraw_Annotation_h16.png) [Insert Annotation](https://wiki.freecad.org/TechDraw_Annotation): adds a plain text block as annotation.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_LeaderLine_h16.png) [Add Leaderline to View](https://wiki.freecad.org/TechDraw_LeaderLine): adds a leaderline to a view. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_RichTextAnnotation_h16.png) [Insert Rich Text Annotation](https://wiki.freecad.org/TechDraw_RichTextAnnotation): adds an rich text block as annotation to a leaderline or a view. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_CosmeticVertex_h16.png) [Add Cosmetic Vertex](https://wiki.freecad.org/TechDraw_CosmeticVertex): adds a Vertex which is not part of the source geometry. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_Midpoints_h16.png) [Add Midpoint Vertices](https://wiki.freecad.org/TechDraw_Midpoints): adds Cosmetic Vertices at midpoints of selected edges. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_Quadrants_h16.png) [Add Quadrant Vertices](https://wiki.freecad.org/TechDraw_Quadrants): adds Cosmetic Vertices at quarter points of selected (circular) edges. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_FaceCenterLine_h16.png) [Add Centerline to Faces](https://wiki.freecad.org/TechDraw_FaceCenterLine): adds a centerline to selected face(s). 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_2LineCenterLine_h16.png) [Add Centerline between 2 Lines](https://wiki.freecad.org/TechDraw_2LineCenterLine): adds a centerline between 2 lines. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_2PointCenterLine_h16.png) [Add Centerline between 2 Points](https://wiki.freecad.org/TechDraw_2PointCenterLine): adds a centerline between 2 points. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_2PointCosmeticLine_h16.png) [Add Cosmetic Line Through 2 points](https://wiki.freecad.org/TechDraw_2PointCosmeticLine): adds a cosmetic line connecting 2 vertices. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_CosmeticEraser_h16.png) [Remove Cosmetic Object](https://wiki.freecad.org/TechDraw_CosmeticEraser): removes cosmetic objects from a page. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_DecorateLine_h16.png) [Change Appearance of Lines](https://wiki.freecad.org/TechDraw_DecorateLine): changes the appearance of selected line(s). 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ShowAll_h16.png) [Show/Hide Invisible Edges](https://wiki.freecad.org/TechDraw_ShowAll): shows/hides invisible lines/edges in a view. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_WeldSymbol_h16.png) [Add Welding Information to Leader](https://wiki.freecad.org/TechDraw_WeldSymbol): adds welding specifications to an existing leaderline. 

## Extensions

These are tools to improve your TechDraw drawings.

### Attributes and modifications

* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionSelectLineAttributes_h16.png) [Select Line Attributes, Cascade Spacing and Delta Distance](https://wiki.freecad.org/TechDraw_ExtensionSelectLineAttributes): selects the attributes (style, width and color) for new cosmetic lines and centerlines, and specifies the cascade spacing and delta distance. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionChangeLineAttributes_h16.png) [Change Line Attributes](https://wiki.freecad.org/TechDraw_ExtensionChangeLineAttributes): changes the attributes (style, width and color) of cosmetic lines and centerlines. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionExtendLine_h16.png) [Extend Line](https://wiki.freecad.org/TechDraw_ExtensionExtendLine): extends a cosmetic line or centerline at both ends. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionShortenLine_h16.png) [Shorten Line](https://wiki.freecad.org/TechDraw_ExtensionShortenLine): shortens a cosmetic line or centerline at both ends. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionLockUnlockView_h16.png) [Lock/Unlock View](https://wiki.freecad.org/TechDraw_ExtensionLockUnlockView): locks or unlocks the position of a view. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionPositionSectionView_h16.png) [Position Section View](https://wiki.freecad.org/TechDraw_ExtensionPositionSectionView): orthogonally aligns a section view with its source view. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionPosHorizChainDimension_h16.png) [Position Horizontal Chain Dimensions](https://wiki.freecad.org/TechDraw_ExtensionPosHorizChainDimension): aligns horizontal dimensions to create a chain dimension. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionPosVertChainDimension_h16.png) [Position Vertical Chain Dimensions](https://wiki.freecad.org/TechDraw_ExtensionPosVertChainDimension): aligns vertical dimensions to create a chain dimension. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionPosObliqueChainDimension_h16.png) [Position Oblique Chain Dimensions](https://wiki.freecad.org/TechDraw_ExtensionPosObliqueChainDimension): aligns oblique dimensions to create a chain dimension. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCascadeHorizDimension_h16.png) [Cascade Horizontal Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCascadeHorizDimension): evenly spaces horizontal dimensions. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCascadeVertDimension_h16.png) [Cascade Vertical Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCascadeVertDimension): evenly spaces vertical dimensions. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCascadeObliqueDimension_h16.png) [Cascade Oblique Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCascadeObliqueDimension): evenly spaces oblique dimensions. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionAreaAnnotation_h16.png) [AreaAnnotation](https://wiki.freecad.org/TechDraw_ExtensionAreaAnnotation): insert an area annotation. 

### Centerlines and threading

* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCircleCenterLines_h16.png) [Add Circle Centerlines](https://wiki.freecad.org/TechDraw_ExtensionCircleCenterLines): adds centerlines to circles and arcs. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionHoleCircle_h16.png) [Add Bolt Circle Centerlines](https://wiki.freecad.org/TechDraw_ExtensionHoleCircle): adds centerlines to a circular pattern of circles. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionThreadHoleSide_h16.png) [Add Cosmetic Thread Hole Side View](https://wiki.freecad.org/TechDraw_ExtensionThreadHoleSide): adds a cosmetic thread to the side view of a hole. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionThreadHoleBottom_h16.png) [Add Cosmetic Thread Hole Bottom View](https://wiki.freecad.org/TechDraw_ExtensionThreadHoleBottom): adds a cosmetic thread to the top or bottom view of holes. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionThreadBoltSide_h16.png) [Add Cosmetic Thread Bolt Side View](https://wiki.freecad.org/TechDraw_ExtensionThreadBoltSide): adds a cosmetic thread to the side view of a bolt/screw/rod. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionThreadBoltBottom_h16.png) [Add Cosmetic Thread Bolt Bottom View](https://wiki.freecad.org/TechDraw_ExtensionThreadBoltBottom): adds a cosmetic thread to the top or bottom view of bolts/screws/rods. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionVertexAtIntersection_h16.png) [Add Cosmetic Intersection Vertex(es)](https://wiki.freecad.org/TechDraw_ExtensionVertexAtIntersection): adds cosmetic vertex(es) at the intersection(s) of selected edges. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionDrawCosmCircle_h16.png) [Add Cosmetic Circle](https://wiki.freecad.org/TechDraw_ExtensionDrawCosmCircle): adds a cosmetic circle based on two vertexes. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionDrawCosmArc_h16.png) [Add Cosmetic Arc](https://wiki.freecad.org/TechDraw_ExtensionDrawCosmArc): adds a cosmetic counter clockwise arc based on three vertexes. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionDrawCosmCircle3Points_h16.png) [Add Cosmetic Circle 3 Points](https://wiki.freecad.org/TechDraw_ExtensionDrawCosmCircle3Points): adds a cosmetic circle based on three vertexes. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionLineParallel_h16.png) [Add Cosmetic Parallel Line](https://wiki.freecad.org/TechDraw_ExtensionLineParallel): adds a cosmetic line parallel to another line through a vertex. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionLinePerpendicular_h16.png) [Add Cosmetic Perpendicular Line](https://wiki.freecad.org/TechDraw_ExtensionLinePerpendicular): adds a cosmetic line perpendicular to another line through a vertex. 

### Dimensions

* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateHorizChainDimension_h16.png) [Create Horizontal Chain Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCreateHorizChainDimension): creates a sequence of aligned horizontal dimensions. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateVertChainDimension_h16.png) [Create Vertical Chain Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCreateVertChainDimension): creates a sequence of aligned vertical dimensions. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateObliqueChainDimension_h16.png) [Create Oblique Chain Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCreateObliqueChainDimension): creates a sequence of aligned oblique dimensions. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateHorizCoordDimension_h16.png) [Create Horizontal Coordinate Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCreateHorizCoordDimension): creates multiple evenly spaced horizontal dimensions starting from the same baseline. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateVertCoordDimension_h16.png) [Create Vertical Coordinate Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCreateVertCoordDimension): creates multiple evenly spaced vertical dimensions starting from the same baseline. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateObliqueCoordDimension_h16.png) [Create Oblique Coordinate Dimensions](https://wiki.freecad.org/TechDraw_ExtensionCreateObliqueCoordDimension): creates multiple evenly spaced oblique dimensions starting from the same baseline. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateHorizChamferDimension_h16.png) [Create Horizontal Chamfer Dimension](https://wiki.freecad.org/TechDraw_ExtensionCreateHorizChamferDimension): creates a horizontal size and angle dimension for a chamfer. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateVertChamferDimension_h16.png) [Create Vertical Chamfer Dimension](https://wiki.freecad.org/TechDraw_ExtensionCreateVertChamferDimension): creates a vertical size and angle dimension for a chamfer. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionCreateLengthArc_h16.png) [Create Arc Length Dimension](https://wiki.freecad.org/TechDraw_ExtensionCreateLengthArc): creates an arc length dimension. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionInsertDiameter_h16.png) [Insert '⌀' Prefix](https://wiki.freecad.org/TechDraw_ExtensionInsertDiameter): inserts a '⌀' symbol at the beginning of the dimension text. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionInsertSquare_h16.png) [Insert '〼' Prefix](https://wiki.freecad.org/TechDraw_ExtensionInsertSquare): inserts a '〼' symbol at the beginning of the dimension text. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionRemovePrefixChar_h16.png) [Remove Prefix](https://wiki.freecad.org/TechDraw_ExtensionRemovePrefixChar): removes all symbols at the beginning of the dimension text. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionIncreaseDecimal_h16.png) [Increase Decimal Places](https://wiki.freecad.org/TechDraw_ExtensionIncreaseDecimal): increases the number of decimal places of the dimension text. 
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExtensionDecreaseDecimal_h16.png) [Decrease Decimal Places](https://wiki.freecad.org/TechDraw_ExtensionDecreaseDecimal): decreases the number of decimal places of the dimension text. 

## Export

These are tools for exporting pages to other applications.

* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExportPageSVG_h16.png) [Export Page as SVG](https://wiki.freecad.org/TechDraw_ExportPageSVG): saves the current page as [SVG](https://wiki.freecad.org/SVG) file.
* ![](TechDrawWorkbench_files/img_h16/TechDraw_ExportPageDXF_h16.png) [Export Page as DXF](https://wiki.freecad.org/TechDraw_ExportPageDXF): saves the current page as [DXF](https://wiki.freecad.org/DXF) file.

## Additional features

* [Line Groups](https://wiki.freecad.org/TechDraw_LineGroup): to control the appearance of various types of lines.
* [Templates](https://wiki.freecad.org/TechDraw_Templates): the default templates defined for the drawing pages.
* [Hatching](https://wiki.freecad.org/TechDraw_Hatching): explanation of the different hatching techniques.
* [Geometric dimensioning and tolerancing](https://wiki.freecad.org/TechDraw_Geometric_dimensioning_and_tolerancing): explanation on how to achieve geometric dimensioning and tolerancing.

## Preferences

* ![](TechDrawWorkbench_files/img_h16/Preferences-techdraw_h16.png) [Preferences](https://wiki.freecad.org/TechDraw_Preferences): preferences for the default values of the drawing page such as projection angle, colors, text sizes, and line styles.

## Scripting

The TechDraw tools can be used in [macros](https://wiki.freecad.org/Macros) and from the [Python](https://wiki.freecad.org/Python) console by using two APIs.

* [TechDraw API](https://wiki.freecad.org/TechDraw_API)
* [TechDrawGui API](https://wiki.freecad.org/TechDrawGui_API)

## Limitations

* TechDraw drawings and its API are not interchangeable with the [Drawing Workbench](https://wiki.freecad.org/Drawing_Workbench) and its API. It is possible to convert Drawing Pages to TechDraw Pages using a Python script (`moveViews.py`).
* It is possible to have both TechDraw and Drawing Pages in the same FreeCAD document, as each page is completely independent from each other.
* There are minor differences in specifying editable texts in [SVG](https://wiki.freecad.org/SVG) templates compared to the Drawing module. In TechDraw the scaling of the SVG document affects the position of the editable text fields. See the forum discussion [TechDraw templates scale](https://forum.freecadweb.org/viewtopic.php?f=3&t=24981&p=196271#p196271) for more details.
* Do not cut, copy and paste TechDraw objects in the [Tree view](https://wiki.freecad.org/Tree_view) as this generally does not work out well.
* Do not drag TechDraw objects in the [Tree view](https://wiki.freecad.org/Tree_view) with the mouse.

## Tutorials

* [Basic TechDraw Tutorial](https://wiki.freecad.org/Basic_TechDraw_Tutorial): introduction to creating drawings with the TechDraw Workbench.
* [Creating a new template](https://wiki.freecad.org/TechDraw_TemplateHowTo): instructions to create a new page template in Inkscape for using with the TechDraw Workbench.
* [Measurement Of Angles On Holes](https://wiki.freecad.org/Measurement_Of_Angles_On_Holes): instructions for adding centerlines and subsequent angle representations on holes.
* [Miscellaneous](https://wiki.freecad.org/TechDraw_HowTo_Page): instructions for different settings like center marks, etc.
* [TechDraw Pitch Circle Tutorial](https://wiki.freecad.org/TechDraw_Pitch_Circle_Tutorial): instructions for adding a pitch circle.

Video tutorials by sliptonic

* TechDraw Workbench [Part 1 (Basics)](https://www.youtube.com/watch?v=7LbOmSGW9F0), [Part 2 (Dimensions)](https://www.youtube.com/watch?v=z3w84RfvqaE), [Part 3 (Multiview)](https://www.youtube.com/watch?v=uNjXg-m38aI)
* TechDraw Workbench [Part 4 (Section and Detail)](https://www.youtube.com/watch?v=3zSdeFV6I5o), [Part 5 (Customizing Templates)](https://www.youtube.com/watch?v=kcmdJ7xa7gg)