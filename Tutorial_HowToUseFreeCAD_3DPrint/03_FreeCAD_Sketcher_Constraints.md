# Maker Tales: [How To Use FreeCAD][t]<br>_Sketcher Workbench_<br>Constraints (part 3/5)
[t]:https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN

[<< Return to 621-freecad-notes/README.md](README.md)

_The ["How To Use FreeCAD - Constraints" video](https://www.youtube.com/watch?v=hqPA_AfVzmo) explains how to use constraints in the FreeCAD [Sketcher Workbench](https://wiki.freecadweb.org/Sketcher_Workbench)._


> _NOTE: The ["How To Use FreeCAD For Beginners … For 3D Printing" series](https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN) is based on the _Stable_ version of the FreeCAD RealThunder branch. The notes below have been checked on macOS 11.6 (Big Sur) with the FreeCAD pre-release 0.20.26498 and FreeCAD LinkStage3 (RealThunder) branch version: 2021.1015.24301. See ["Resources" section](#resources-) below._

## Contents <a id="contents"></a>
[Sketcher Geometric Constraints](#sketcher-geometric-constraints-) •
[Sketcher Dimensional Constraints](#sketcher-dimensional-constraints-) •
[Time Stamps](#time-stamps-) •
[Resources](#resources-)

## Sketcher Geometric Constraints <a id="sketcher-geometric-constraints-"></a><sup>[▴](#contents)</sup>

![](03_FreeCAD_Sketcher_Constraints_files/Sketcher_Geometric_Constraints.png)

* **Coincident** (<kbd>C</kbd>)
    * Select parent, child, child… . Apply tool causes children to coincide with parent.
    * Activate tool. Click parent point, then child point.
    * Origin can be one of the points
* **Point Onto Object** (v20: <kbd>O</kbd> | RealThunder: <kbd>shift</kbd><kbd>O</kbd>)
    * Select one point (marker) and and several lines (curves). Apply tool.
    * Select one line (curve) and several points. Apply tool.
    * Constraint includes projections.
    * Selected object can be an axis.
* **Horizontal** (<kbd>H</kbd>) points or lines
* **Vertical** (<kbd>V</kbd>) points or lines
* **Parallel** (v20: <kbd>P</kbd> | RealThunder: <kbd>shift</kbd><kbd>P</kbd>)
* **Perpendicular (aka Normal)** (<kbd>V</kbd>)
    * points can be coincident or not
* **Tangent** (<kbd>T</kbd>)
    * line to curve
    * point to curve
    * point to point becomes coincident tangential
    * line to line becomes collinear
    * point tangency with marker: maker + entity + entity
* **Equal** (<kbd>E</kbd>) primary dimensions
* **Symmetry** (<kbd>S</kbd>)
    * Click one or more member entities. Click symmetry reference entity. Click tool.
    * Click point. Click line|curve. Point becomes symmetrically center constrained.
    * Entity point. Entity point. Marker point.
* **Block** block selected edge from moving (like a lock)
    * For _temporary use_ while performing some other operations
    * Will leave line under constrained if not removed

## Sketcher Dimensional Constraints <a id="sketcher-dimensional-constraints-"></a><sup>[▴](#contents)</sup>

![](03_FreeCAD_Sketcher_Constraints_files/Sketcher_Dimensional_Constraints.png)

* **Padlock** 
    * Sets vertical and horizontal distances relative to the origin
    * Use sparingly to avoid over constraining
* **Horizontal Distance** (v20: <kbd>L</kbd> | RealThunder: <kbd>shift</kbd><kbd>H</kbd>)
* **Vertical Distance** (v20: <kbd>I</kbd> | RealThunder: <kbd>shift</kbd><kbd>V</kbd>)
* **Diagonal Distance** (v20:  <kbd>K</kbd><kbd>D</kbd> | RealThunder: <kbd>shift</kbd><kbd>D</kbd>)
* **Radial | Diameter** (RealThunder: <kbd>shift</kbd><kbd>R</kbd>)
* **Angle** (v20: <kbd>K</kbd><kbd>A</kbd> | RealThunder: <kbd>A</kbd>) Select two lines.
* **Snell's Refraction Law** (v20: <kbd>K</kbd><kbd>W</kbd>)

## Sketcher Constraint Tools <a id="sketcher-constraint-tools-"></a><sup>[▴](#contents)</sup>

![](03_FreeCAD_Sketcher_Constraints_files/Sketcher_Constraint_Tools.png)

* **Toggle Driving/Reference Constraint** Toggles the toolbar or the selected constraints to/from reference mode.
    * Reference distances can provide non-constraining distance information
* **Activate/Deactivate Constraint**

## Time Stamps <a id="time-stamps-"></a><sup>[▴](#contents)</sup>

* 0:00 - Intro.
* 00:18 - Quick Recap &  What This Video Is All About.
* 01:01 - What Are Constraints.
* 02:09 - Coincident Constraint ( C ).
* 03:21 - Fix A Point Onto An Object Constraint ( Shift + O ).
* 05:11 - Vertical & Horizontal Constraint ( V / H ).
* 07:18 - Parallel Constraint ( Shift + P ).
* 08:05 - Perpendicular Constraint ( N ).
* 09:47 - Tangent Constraint ( T ).
* 14:13 - Equal Constraint ( E ).
* 14:56 - Symmetry Constraint ( S ).
* 16:48 - Block Constraint / Tool.
* 17:51 - Thangs Sponsorship Segment.
* 18:33 - Lock Constraint.
* 19: 32 - Linear Dimensional Constraints ( Shift + H/V/D ).
* 21:22 - Radius & Diameter Constraints ( Shift + R ).
* 22:38 - Angle Constraint ( A ).
* 23:03 - Refraction Law / Snell's Law.
* 23:34 - Reference Dimensions.
* 24:30 - Deactivate & Activate Constraints.
* 25:08 - Closing Notes.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [2021.08.20 How To Use FreeCAD - Constraints | The Realthunder Branch | Sketcher Workbench | P. 3 ⇗](https://www.youtube.com/watch?v=hqPA_AfVzmo)
* [FreeCAD: weekly builds ⇗](https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds)
* [GitHub/realthunder: FreeCAD releases ⇗](https://github.com/realthunder/FreeCAD_assembly3/releases)
* [GitLab: 621-FreeCAD-Notes ⇗](https://gitlab.com/mc_qref/621-freecad-notes) / [README.md ⇗](https://gitlab.com/mc_qref/621-freecad-notes/-/blob/main/README.md)
