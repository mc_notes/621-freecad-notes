# [621 FreeCAD Notes](https://gitlab.com/mc_qref/621-freecad-notes/-/blob/main/README.md)

_The [621-FreeCAD-Notes repository](https://gitlab.com/mc_qref/621-freecad-notes) provides a collection of [Markdown](https://en.wikipedia.org/wiki/Markdown) notes on how to use the [FreeCAD design software](https://www.freecadweb.org/features.php)._

## Contents <a id="contents"></a>
[3D Navigation](#3d-navigation-) •
[Tutorials](#tutorials-) •
[Workbenches](#tutorials-) •
[Resources](#resources-)

## 3D Navigation <a id="3d-navigation-"></a><sup>[▴](#contents)</sup>

Several navigation approaches are detailed on the [FreeCAD 3D View Navigation](FreeCAD_3D_Navigation) page. In general, the `Gesture` mode was found to be applicable for the standard 3-button mouse, the Apple Magic Mouse, and the Apple (no-button) Trackpad.

* [FreeCAD 3D View Navigation](FreeCAD_3D_Navigation/README.md)

## Tutorials <a id="tutorials-"></a><sup>[▴](#contents)</sup>

[**How To Use FreeCAD For 3D Printing (For Beginners)**](Tutorial_HowToUseFreeCAD_3DPrint/README.md)

The pages listed below provide notes based on the Maker Tales ["How To Use FreeCAD For Beginners … For 3D Printing" series ⇗](https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN) video tutorial series. Videos 1 - 5 provide a beginner level introduction to FreeCAD. How to export a model for 3D printing is covered in the video 4.

0. [FreeCAD's "Topological Naming Problem"](Tutorial_HowToUseFreeCAD_3DPrint/00_FreeCAD_Broken.md)
1. [Setup & Basics](Tutorial_HowToUseFreeCAD_3DPrint/01_FreeCAD_Setup_Basics.md)
2. [Sketcher Workbench: Lines, Tools & References](Tutorial_HowToUseFreeCAD_3DPrint/02_FreeCAD_Sketcher_Lines_Tools_Refs.md)
3. [Sketcher Workbench: Constraints](Tutorial_HowToUseFreeCAD_3DPrint/03_FreeCAD_Sketcher_Constraints.md)
4. [3D Printing](Tutorial_HowToUseFreeCAD_3DPrint/04_FreeCAD_3D_Print.md)
5. [How To Do Parametric Modeling](Tutorial_HowToUseFreeCAD_3DPrint/05_FreeCAD_Parametric.md)

## Workbenches <a id="workbenches-"></a><sup>[▴](#contents)</sup>

**[Built-in Workbenches](FreeCAD_Workbenches_BuiltIn/README.md)**, ([wiki ⇗](https://wiki.freecad.org/Workbenches))

* [ArchWorkbench](FreeCAD_Workbenches_BuiltIn/ArchWorkbench.md)
* [DraftWorkbench](FreeCAD_Workbenches_BuiltIn/DraftWorkbench.md)
* [ImageWorkbench](FreeCAD_Workbenches_BuiltIn/ImageWorkbench.md)
* [MeshWorkbench](FreeCAD_Workbenches_BuiltIn/MeshWorkbench.md)
* [OpenSCADWorkbench](FreeCAD_Workbenches_BuiltIn/OpenSCADWorkbench.md)
* [PartDesignWorkbench](FreeCAD_Workbenches_BuiltIn/PartDesignWorkbench.md)
* [PartModule](FreeCAD_Workbenches_BuiltIn/PartModule.md)
* [SketcherWorkbench](FreeCAD_Workbenches_BuiltIn/SketcherWorkbench.md)
* [SpreadsheetWorkbench](FreeCAD_Workbenches_BuiltIn/SpreadsheetWorkbench.md)
* [SurfaceWorkbench](FreeCAD_Workbenches_BuiltIn/SurfaceWorkbench.md)
* [TechDrawWorkbench](FreeCAD_Workbenches_BuiltIn/TechDrawWorkbench.md)

**External Workbenches**, ([wiki ⇗](https://wiki.freecad.org/External_workbenches))

* [Assembly3]()

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [FreeCAD Website ⇗](https://www.freecadweb.org/), [weekly builds ⇗](https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds)
* [FreeCAD RealThunder branch: releases ⇗](https://github.com/realthunder/FreeCAD_assembly3/releases)
* [GitLab: 621-FreeCAD-Notes ⇗](https://gitlab.com/mc_qref/621-freecad-notes) / [README.md ⇗](https://gitlab.com/mc_qref/621-freecad-notes/-/blob/main/README.md)
