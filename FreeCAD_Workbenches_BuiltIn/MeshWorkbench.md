# FreeCAD: ![](MeshWorkbench_files/img_h16/Workbench_Mesh_h16.png) [Mesh Workbench][t]
[t]:https://wiki.freecad.org/Mesh_Workbench

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

The ![](MeshWorkbench_files/img_h16/Workbench_Mesh_h16.png) Mesh Workbench handles [triangle meshes](http://en.wikipedia.org/wiki/Triangle_mesh).

* triangle meshes cannot accurately define curved surfaces
* uses [boundary representation (BREP)](http://en.wikipedia.org/wiki/Boundary_representation) instead of triangle meshes. 
* provides some commands to directly manipulate meshes
* imports 3D mesh data and convert it to a solid for use with the ![](MeshWorkbench_files/img_h16/Workbench_Part_h16.png) [Part Workbench](https://wiki.freecad.org/Part_Workbench) or ![](MeshWorkbench_files/img_h16/Workbench_PartDesign_h16.png) [PartDesign Workbench](https://wiki.freecad.org/PartDesign_Workbench).
* creates a `Mesh_FromPartShape` static snapshot which does not dynamically update if the part is later changed 

![](MeshWorkbench_files/500px-Mesh_example.jpg)

## Tools

All Mesh Workbench tools can be accessed from the **Meshes** menu. Almost all are also available in one of the Mesh toolbars.

* ![](MeshWorkbench_files/img_h16/Mesh_Import_h16.png) [Import mesh...](https://wiki.freecad.org/Mesh_Import): Imports a mesh object from a file.
* ![](MeshWorkbench_files/img_h16/Mesh_Export_h16.png) [Export mesh...](https://wiki.freecad.org/Mesh_Export): Exports a mesh object to a file.
    * > Note: ASCII STL extension `.atl` is not supported by PrusaSlicer 2.3
* ![](MeshWorkbench_files/img_h16/Mesh_FromPartShape_h16.png) [Create mesh from shape...](https://wiki.freecad.org/Mesh_FromPartShape): Creates mesh objects from shape objects.
    * select part first, then click `Mesh_FromPartShape`
    * > Note: creates a static snapshot of the selected part
* ![](MeshWorkbench_files/img_h16/Mesh_RemeshGmsh_h16.png) [Refinement...](https://wiki.freecad.org/Mesh_RemeshGmsh): Remeshes a mesh object. 
* Analyze
    * ![](MeshWorkbench_files/img_h16/Mesh_Evaluation_h16.png) [Evaluate and repair mesh...](https://wiki.freecad.org/Mesh_Evaluation): Evaluates and repairs a mesh object.
    * ![](MeshWorkbench_files/img_h16/Mesh_EvaluateFacet_h16.png) [Face info](https://wiki.freecad.org/Mesh_EvaluateFacet): Shows information about faces of mesh objects.
    * ![](MeshWorkbench_files/img_h16/Mesh_CurvatureInfo_h16.png) [Curvature info](https://wiki.freecad.org/Mesh_CurvatureInfo): Shows the absolute curvature of [curvature objects](https://wiki.freecad.org/Mesh_VertexCurvature) at selected points.
    * ![](MeshWorkbench_files/img_h16/Mesh_EvaluateSolid_h16.png) [Check solid mesh](https://wiki.freecad.org/Mesh_EvaluateSolid): Checks if a mesh object is solid.
    * ![](MeshWorkbench_files/img_h16/Mesh_BoundingBox_h16.png) [Boundings info...](https://wiki.freecad.org/Mesh_BoundingBox): Shows the bounding box coordinates of a mesh object.
* ![](MeshWorkbench_files/img_h16/Mesh_VertexCurvature_h16.png) [Curvature plot](https://wiki.freecad.org/Mesh_VertexCurvature): Creates Mesh Curvature objects for mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_HarmonizeNormals_h16.png) [Harmonize normals](https://wiki.freecad.org/Mesh_HarmonizeNormals): Harmonizes the normals of mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_FlipNormals_h16.png) [Flip normals](https://wiki.freecad.org/Mesh_FlipNormals): Flips the normals of mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_FillupHoles_h16.png) [Fill holes...](https://wiki.freecad.org/Mesh_FillupHoles): Fills holes in mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_FillInteractiveHole_h16.png) [Close hole](https://wiki.freecad.org/Mesh_FillInteractiveHole): Fills selected holes in mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_AddFacet_h16.png) [Add triangle](https://wiki.freecad.org/Mesh_AddFacet): Adds faces along a boundary of an open mesh object.
* ![](MeshWorkbench_files/img_h16/Mesh_RemoveComponents_h16.png) [Remove components...](https://wiki.freecad.org/Mesh_RemoveComponents): Removes faces from mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_RemoveCompByHand_h16.png) [Remove components by hand...](https://wiki.freecad.org/Mesh_RemoveCompByHand): Removes components from mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_Segmentation_h16.png) [Create mesh segments...](https://wiki.freecad.org/Mesh_Segmentation): Creates separate mesh segments for specified surface types of a mesh object.
* ![](MeshWorkbench_files/img_h16/Mesh_SegmentationBestFit_h16.png) [Create mesh segments from best-fit surfaces...](https://wiki.freecad.org/Mesh_SegmentationBestFit): Creates separate mesh segments for specified surface types of a mesh object, and can identify their parameters.
* ![](MeshWorkbench_files/img_h16/Mesh_Smoothing_h16.png) [Smooth...](https://wiki.freecad.org/Mesh_Smoothing): Smooths mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_Decimating_h16.png) [Decimation...](https://wiki.freecad.org/Mesh_Decimating): Reduces the number of faces in mesh objects. 
* ![](MeshWorkbench_files/img_h16/Mesh_Scale_h16.png) [Scale...](https://wiki.freecad.org/Mesh_Scale): Scales mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_BuildRegularSolid_h16.png) [Regular solid...](https://wiki.freecad.org/Mesh_BuildRegularSolid): Creates a regular parametric solid mesh object.
* Boolean
    * ![](MeshWorkbench_files/img_h16/Mesh_Union_h16.png) [Union](https://wiki.freecad.org/Mesh_Union): Creates a mesh object that is the union of two mesh objects.
    * ![](MeshWorkbench_files/img_h16/Mesh_Intersection_h16.png) [Intersection](https://wiki.freecad.org/Mesh_Intersection): Creates a mesh object that is the intersection of two mesh objects.
    * ![](MeshWorkbench_files/img_h16/Mesh_Difference_h16.png) [Difference](https://wiki.freecad.org/Mesh_Difference): Creates a mesh object that is the difference of two mesh objects.
* Cutting
    * ![](MeshWorkbench_files/img_h16/Mesh_PolyCut_h16.png) [Cut mesh](https://wiki.freecad.org/Mesh_PolyCut): Cuts whole faces from mesh objects.
    * ![](MeshWorkbench_files/img_h16/Mesh_PolyTrim_h16.png) [Trim mesh](https://wiki.freecad.org/Mesh_PolyTrim): Trims faces and parts of faces from mesh objects.
    * ![](MeshWorkbench_files/img_h16/Mesh_TrimByPlane_h16.png) [Trim mesh with a plane](https://wiki.freecad.org/Mesh_TrimByPlane): Trims faces and parts of faces on one side of a plane from a mesh object.
    * ![](MeshWorkbench_files/img_h16/Mesh_SectionByPlane_h16.png) [Create section from mesh and plane](https://wiki.freecad.org/Mesh_SectionByPlane): Creates a cross section across a mesh object.
    * ![](MeshWorkbench_files/img_h16/Mesh_CrossSections_h16.png) [Cross-sections...](https://wiki.freecad.org/Mesh_CrossSections): Creates multiple cross sections across mesh objects. <span style="font-size:x-small;">[introduced in version 0.19](https://wiki.freecad.org/Release_notes_0.19)</span>
* ![](MeshWorkbench_files/img_h16/Mesh_Merge_h16.png) [Merge](https://wiki.freecad.org/Mesh_Merge): Creates a mesh object by combining the meshes of two or more mesh objects.
* ![](MeshWorkbench_files/img_h16/Mesh_SplitComponents_h16.png) [Split by components](https://wiki.freecad.org/Mesh_SplitComponents): Splits a mesh object into its components. 
* ![](MeshWorkbench_files/img_h16/MeshPart_CreateFlatMesh_h16.png) [Unwrap Mesh](https://wiki.freecad.org/MeshPart_CreateFlatMesh): Creates a flat representation of a mesh object. 
* ![](MeshWorkbench_files/img_h16/MeshPart_CreateFlatFace_h16.png) [Unwrap Face](https://wiki.freecad.org/MeshPart_CreateFlatFace): Creates a flat representation of a face of a shape object. 

## Preferences

* Display > Mesh View
* Import-Export > Mesh Formats (use by `Std_Export`)
* OpenSCAD

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* More mesh tools are available in the ![](MeshWorkbench_files/img_h16/Workbench_OpenSCAD_h16.png) [OpenSCAD Workbench](https://wiki.freecad.org/OpenSCAD_Workbench).
* See also [FreeCAD and Mesh Import](https://wiki.freecad.org/FreeCAD_and_Mesh_Import)
* See [Asymptote](https://wiki.freecad.org/Asymptote) to export meshes to the Asymptote format.

