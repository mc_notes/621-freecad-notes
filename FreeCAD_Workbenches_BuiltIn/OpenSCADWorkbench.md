# FreCAD: ![](OpenSCADWorkbench_files/img_h16/Workbench_OpenSCAD_h16.png) [OpenSCAD Workbench][t]
[t]:https://wiki.freecad.org/OpenSCAD_Workbench

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Tools](#tools-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

The ![](OpenSCADWorkbench_files/img_h16/Workbench_OpenSCAD_h16.png) OpenSCAD Workbench is intended to offer interoperability with the open source software [OpenSCAD](http://www.openscad.org/). This program is not distributed as part of FreeCAD, but should be installed to make full use of this workbench. OpenSCAD should not be confused with [OpenCASCADE](https://wiki.freecad.org/OpenCASCADE), which is the geometrical kernel that FreeCAD uses to build geometry on screen. The OpenCASCADE libraries are always needed to use FreeCAD, while the OpenSCAD executable is entirely optional.

It contains a [CSG](https://wiki.freecad.org/OpenSCAD_CSG) importer to open the CSG files from OpenSCAD, and an exporter to output a CSG based tree. Geometry which is not based on CSG operations will be exported as a mesh.

This workbench contains functions to modify the CSG feature tree and repair models. It also contains general purpose tools that do not require installation of OpenSCAD; they can be used in conjunction with other workbenches. For example, the [Mesh Workbench](https://wiki.freecad.org/Mesh_Workbench) internally uses the OpenSCAD functions to perform operations with [meshes](https://wiki.freecad.org/Mesh), as they are quite robust.

[![](OpenSCADWorkbench_files/OpenSCADexamaple1.png)](https://wiki.freecad.org/File:OpenSCADexamaple1.png)

## Dependencies

In FreeCAD 0.19, the Ply (Python-Lex-Yacc) module, which is used to import CSG files, was removed from the FreeCAD source code, as it is a third party library not developed by FreeCAD. As a result, you now need to install Ply before using the OpenSCAD Workbench. When using a pre-packaged, stable version of FreeCAD this dependency should be installed automatically in all platforms; in other cases, for example, when [compiling](https://wiki.freecad.org/Compiling) from source, you may have to install it from an online repository.

In openSUSE this is done by:

    sudo zypper install python3-ply

In Debian/Ubuntu based systems this is done like the following:

    sudo apt install python3-ply

The general installation in all platforms can be done from the Python package index.

## OpenSCAD language and file format

The OpenSCAD language allows the use of variables and loops. It allows you to specify sub-modules to reuse geometry and code. This high degree of flexibility makes parsing very complex. Currently the OpenSCAD Workbench cannot handle the OpenSCAD language natively. Instead, if OpenSCAD is installed, it can be used to convert the input to the CSG format, which is a subset of the OpenSCAD language, and can be used as the input to OpenSCAD for further processing. During conversion all parametric behavior is lost, meaning that all variable names are discarded, loops expanded, and mathematical expressions evaluated.

## Tools <a id="tools-"></a><sup>[▴](#contents)</sup>

* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_ColorCodeShape_h16.png) [Color Code Shape](https://wiki.freecad.org/OpenSCAD_ColorCodeShape): Change the color of selected or all shapes based on their validity.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_ReplaceObject_h16.png) [Replace Object](https://wiki.freecad.org/OpenSCAD_ReplaceObject): Replace an object in the feature tree.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_RemoveSubtree_h16.png) [Remove Subtree](https://wiki.freecad.org/OpenSCAD_RemoveSubtree): Removes the selected objects and all children that are not referenced from other objects.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_RefineShapeFeature_h16.png) [Refine Shape Feature](https://wiki.freecad.org/OpenSCAD_RefineShapeFeature): Create Refine Shape Feature.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_MirrorMeshFeature_h16.png) [Mirror Mesh Feature](https://wiki.freecad.org/OpenSCAD_MirrorMeshFeature): Create Mirror Mesh Feature.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_ScaleMeshFeature_h16.png) [Scale Mesh Feature](https://wiki.freecad.org/OpenSCAD_ScaleMeshFeature): Scale a Mesh Feature.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_ResizeMeshFeature_h16.png) [Resize Mesh Feature](https://wiki.freecad.org/OpenSCAD_ResizeMeshFeature): Resize a Mesh Feature.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_IncreaseToleranceFeature_h16.png) [Increase Tolerance Feature](https://wiki.freecad.org/OpenSCAD_IncreaseToleranceFeature): Increases tolerance of edges/faces/vertex of selected object(s).
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_Edgestofaces_h16.png) [Convert Edges To Faces](https://wiki.freecad.org/OpenSCAD_Edgestofaces): Convert edges to faces. Useful to prepare imported DXF geometry for extrusion.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_ExpandPlacements_h16.png) [Expand Placements](https://wiki.freecad.org/OpenSCAD_ExpandPlacements): Expand all placements downwards the FeatureTree.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_ExplodeGroup_h16.png) [Explode Group](https://wiki.freecad.org/OpenSCAD_ExplodeGroup): Explodes fused part primitives.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_AddOpenSCADElement_h16.png) [Add OpenSCAD Element](https://wiki.freecad.org/OpenSCAD_AddOpenSCADElement): Add an OpenSCAD element by entering OpenSCAD code into the task panel.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_MeshBoolean_h16.png) [Mesh Boolean](https://wiki.freecad.org/OpenSCAD_MeshBoolean): Creates new mesh object by boolean operation from shapes.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_Hull_h16.png) [Hull](https://wiki.freecad.org/OpenSCAD_Hull): Applies a hull to selected shapes.
* ![](OpenSCADWorkbench_files/img_h16/OpenSCAD_Minkowski_h16.png) [Minkowski](https://wiki.freecad.org/OpenSCAD_Minkowski): Applies a minkowski sum to selected shapes.

## Preferences

* ![](OpenSCADWorkbench_files/img_h16/Std_DlgPreferences_h16.png) [Preferences](https://wiki.freecad.org/OpenSCAD_Preferences): preferences available for the OpenSCAD tools.

_OpenSCAD import_

* Maximum number of faces for polygons (fn): 16
* Send to OpenSCAD via: Standard temp directory | User-specified directory | stdout pipe

_OpenSCAD export_

* maximum fragment size: angular (fa) 12.00°, size (fs) 2.00 mm
* convexity: 10
* Mesh fallback: deflection
* Triangulation settings: 0.00

## Limitations

OpenSCAD 

* creates Constructive Solid Geometry
* imports mesh files 
* extrudes 2D geometry from [DXF](https://wiki.freecad.org/DXF) files. 

FreeCAD 

* OCCT geometry kernel uses a BREP boundary representation
* conversion from CSG to BREP should, in theory, be possible
* conversion from BREP to CSG is, in general, not possible.

OpenSCAD works internally on meshes. Some operations which are useful on meshes are not meaningful on a BREP model and can not be fully supported. Among these are convex hull, minkowski sum, glide and subdiv. Currently we run the OpenSCAD binary in order to perform hull and minkwoski operations and import the result. This means that the involved geometry will be triangulated. 

In OpenSCAD non-uniform scaling is often used, which does not impose any problems when using meshes.

In FreeCAD geometry kernel, geometric primitives (lines, circular sections, etc) are converted to BSpline prior to performing such deformations. Those BSplines are known to cause trouble in later boolean operations.

An automatic solution is not available at the moment.
 
Often such problems can be solved be remodeling small parts. 

A deformation of a cylinder can substituted by an extrusion of an ellipses.

## Importing text

Importing OpenSCAD code with texts requires that the fonts that are used are properly installed on your system. You can verify this by opening OpenSCAD as a standalone tool and checking the list in **Help → Font List**. The list will also give you the correct font names. If a font does not appear in the list after installing, you may have to manually copy the font file to the appropriate system directory.

Importing texts is relatively slow. Behind the scenes FreeCAD uses a DXF file created by OpenSCAD. The more contours there are the slower the import.

It can be a good idea to first import a simple test case (replace `NameOfFont` with the correct font name):

``` c
TESTFONT="NameOfFont";
linear_extrude(0.001) {
  text("A", size=5, font=TESTFONT, script="Latn");
};
```

The `script="Latn"` parameter can be left out here, but is required if the text string does not contain any letters, but only punctuation and/or numbers.

Please note that `use <FONT>;` statements in your source files are ignored when importing in FreeCAD. Under OpenSCAD the effect of a `use` statement is that the provided font file is temporarily added to the list of known fonts (although even there the statement does not work when a script is modified interactively).

## Hints

When importing [DXF](https://wiki.freecad.org/DXF) set the Draft precision to a sensible amount as this will affect the detection of connected edges.

If FreeCAD crashes when importing CSG, it is strongly recommended that you enable "automatically check model after boolean operation" in **`Menu` > `Edit` > `Preferences` > `Part Design` > `Model setting`**.

## Tutorials

* [Import OpenSCAD code](https://wiki.freecad.org/Import_OpenSCAD_code)

## Links

* OpenSCAD source code repository on <a href="https://github.com/openscad/openscad" rel="nofollow">GitHub</a>
* <a href="https://freecadweb.org/tracker/search.php?tag_string=OpenSCAD" rel="nofollow">Open tickets tagged "Openscad" on the FreeCAD bugtracker</a>
* <a href="http://www.thingiverse.com/tag:openscad" rel="nofollow">Things tagged with "OpenSCAD" on Thingiverse</a>

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>


