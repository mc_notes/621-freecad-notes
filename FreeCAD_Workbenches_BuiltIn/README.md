# FreeCAD: Built-in Workbenches

* [ArchWorkbench](ArchWorkbench.md)
* [DraftWorkbench](DraftWorkbench.md)
* [ImageWorkbench](ImageWorkbench.md)
* [MeshWorkbench](MeshWorkbench.md)
* [OpenSCADWorkbench](OpenSCADWorkbench.md)
* [PartDesignWorkbench](PartDesignWorkbench.md)
* [PartModule](PartModule.md)
* [SketcherWorkbench](SketcherWorkbench.md)
* [SpreadsheetWorkbench](SpreadsheetWorkbench.md)
* [SurfaceWorkbench](SurfaceWorkbench.md)
* [TechDrawWorkbench](TechDrawWorkbench.md)
