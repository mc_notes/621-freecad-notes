# FreeCAD: ![](DraftWorkbench_files/img_h16/Workbench_Draft_h16.png) [Draft Workbench][t]
[t]:https://wiki.freecad.org/Draft_Workbench

## Contents <a id="contents"></a>
[Workflow](#workflow-) •
[Highlights](#highlights-) •
[Drafting](#drafting-) •
[Annotation](#annotation-) •
[Modification](#modification-) •
[Draft Tray](#draft-tray-) •
[Draft Annotation Scale Widget](#draft-annotation-scale-widget-) •
[Draft Snap Widget](#draft-snap-widget-) •
[Draft Snap Toolbar](#draft-snap-toolbar-) •
[Draft Utility Tools Toolbar](#draft-utility-tools-toolbar-) •
[Additional Tools](#additional-tools-) •
[Additional Features](#additional-features-) •
[Tree View Context Menu](#tree-view-context-menu-) •
[3D View Context Menu](#3d-view-context-menu-) •
[Obsolete Tools](#obsolete-tools-) •
[Resources](#resources-)

## Workflow <a id="workflow-"></a><sup>[▴](#contents)</sup>

**Draft Preferences**

_May need to quit and restart after changing preferences._

* General
* User interface settings (shortcuts, statusbar)
* Grid and snapping
    * Grid: main lines every …, grid spacing `mm`, grid size `lines`
* Visual settings
* Texts and dimensions

**Document Setup**

* Style
    * `Std_DrawStyle` "as-is" | wireframe | points | …
    * `Draft_AnnotationStyleEditor` can export/import
    * `Draft_SetStyle` Default line, face and annotation presets persist document to document. Did not find export/import.
* Grid
* Snap
* Scale

**Document Structure**

* Layer
* Named `Std Group` new, move, select
* Construction Group

**Document View**

* Display Mode: `Flat Lines` | `Wireframe`
* Plane: working plane | working plane proxy

**Start Document**

* `Std_New` New Document. Add name. Save.

1. Select Working Plane
    * Orientation
        * `XY (top)`, `XZ (front)`, `YZ (side)` 
        * `View` current view
        * `Auto` align to current view each time a command is started. (May prefer to expressly select the plane)
        * Selection based: three vertices | line plus vertex | plane
    * Options
        * Offset
        * Grid spacing: 1.00"
        * Main line every: 5
        * Center plane on view
2. Snap to Working Plane: off | on

Downgrade / Upgrade

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

The ![](DraftWorkbench_files/img_h16/Workbench_Draft_h16.png) **Draft Workbench** highlights:

* Create and modify 2D objects
    * any orientation and position in 3D space
    * some can also be non-planar
    * can be used for general drafting
    * can form a basis for 3D objects in other workbenches
        * [Draft Wire](https://wiki.freecad.org/Draft_Wire) may define the path of an [Arch Wall](https://wiki.freecad.org/Arch_Wall)
        * [Draft Polygon](https://wiki.freecad.org/Draft_Polygon) can be extruded with [Part Extrude](https://wiki.freecad.org/Part_Extrude)
* Many [Draft modifier tools](#Modification) can be applied to 2D and 3D objects created with other workbenches
    * [move](https://wiki.freecad.org/Draft_Move) a [Sketch](https://wiki.freecad.org/Sketcher_Workbench) 
    * create a [Draft OrthoArray](https://wiki.freecad.org/Draft_OrthoArray) from a [Part](https://wiki.freecad.org/Part_Workbench) object.
* Additional Draft Workbench tools define:
    * [working plane](https://wiki.freecad.org/Draft_SelectPlane)
    * [grid](https://wiki.freecad.org/Draft_Snap_Grid)
    * [snapping system](https://wiki.freecad.org/Draft_Snap) to precisely control the position of geometry.

![](DraftWorkbench_files/Draft_Workbench_Example.png)

* The image shows the [grid](https://wiki.freecad.org/Draft_Snap_Grid) aligned with the XY plane.  
* On the left, in white, several planar objects.  
* On the right a non-planar [Draft Wire](https://wiki.freecad.org/Draft_Wire) used as the Path Object of a [Draft PathArray](https://wiki.freecad.org/Draft_PathArray).

## Drafting <a id="drafting-"></a><sup>[▴](#contents)</sup>

* ![](DraftWorkbench_files/img_h16/Draft_Line_h16.png) [Line](https://wiki.freecad.org/Draft_Line): creates a straight line.
* ![](DraftWorkbench_files/img_h16/Draft_Wire_h16.png) [Polyline](https://wiki.freecad.org/Draft_Wire): creates a polyline, a sequence of several connected line segments.
* ![](DraftWorkbench_files/img_h16/Draft_Fillet_h16.png) [Fillet](https://wiki.freecad.org/Draft_Fillet): creates a fillet, a rounded corner, or a chamfer, a straight edge, between two [Draft Lines](https://wiki.freecad.org/Draft_Line). 
* ![](DraftWorkbench_files/img_h16/Draft_Arc_h16.png) Arc tools
    * ![](DraftWorkbench_files/img_h16/Draft_Arc_h16.png) [Arc](https://wiki.freecad.org/Draft_Arc): creates a circular arc from a center, a radius, a start angle and an aperture angle.
    * ![](DraftWorkbench_files/img_h16/Draft_Arc_3Points_h16.png) [Arc by 3 points](https://wiki.freecad.org/Draft_Arc_3Points): creates a circular arc from three points that define its circumference. 
* ![](DraftWorkbench_files/img_h16/Draft_Circle_h16.png) [Circle](https://wiki.freecad.org/Draft_Circle): creates a circle from a center and a radius.
* ![](DraftWorkbench_files/img_h16/Draft_Ellipse_h16.png) [Ellipse](https://wiki.freecad.org/Draft_Ellipse): creates an ellipse from two points defining a rectangle in which the ellipse will fit.
* ![](DraftWorkbench_files/img_h16/Draft_Rectangle_h16.png) [Rectangle](https://wiki.freecad.org/Draft_Rectangle): creates a rectangle from two points.
* ![](DraftWorkbench_files/img_h16/Draft_Polygon_h16.png) [Polygon](https://wiki.freecad.org/Draft_Polygon): creates a regular polygon from a center and a radius.
* ![](DraftWorkbench_files/img_h16/Draft_BSpline_h16.png) [B-spline](https://wiki.freecad.org/Draft_BSpline): creates a B-spline curve from several points.
* ![](DraftWorkbench_files/img_h16/Draft_CubicBezCurve_h16.png) Bézier tools
    * ![](DraftWorkbench_files/img_h16/Draft_CubicBezCurve_h16.png) [Cubic Bézier curve](https://wiki.freecad.org/Draft_CubicBezCurve): creates a Bézier curve of the third degree. 
    * ![](DraftWorkbench_files/img_h16/Draft_BezCurve_h16.png) [Bézier curve](https://wiki.freecad.org/Draft_BezCurve): creates a Bézier curve from several points.
* ![](DraftWorkbench_files/img_h16/Draft_Point_h16.png) [Point](https://wiki.freecad.org/Draft_Point): creates a simple point.
* ![](DraftWorkbench_files/img_h16/Draft_Facebinder_h16.png) [Facebinder](https://wiki.freecad.org/Draft_Facebinder): creates a surface object from selected faces.
* ![](DraftWorkbench_files/img_h16/Draft_ShapeString_h16.png) [ShapeString](https://wiki.freecad.org/Draft_ShapeString): creates a compound shape that represents a text string.
* ![](DraftWorkbench_files/img_h16/Draft_Hatch_h16.png) [Hatch](https://wiki.freecad.org/Draft_Hatch): creates hatches on the planar faces of a selected object. 

## Annotation <a id="annotation-"></a><sup>[▴](#contents)</sup>

* ![](DraftWorkbench_files/img_h16/Draft_Text_h16.png) [Text](https://wiki.freecad.org/Draft_Text): creates a multi-line text at a given point.
* ![](DraftWorkbench_files/img_h16/Draft_Dimension_h16.png) [Dimension](https://wiki.freecad.org/Draft_Dimension): creates a linear dimension, a radial dimension or an angular dimension.
* ![](DraftWorkbench_files/img_h16/Draft_Label_h16.png) [Label](https://wiki.freecad.org/Draft_Label): creates a multi-line text with a 2-segment leader line and an arrow.
* ![](DraftWorkbench_files/img_h16/Draft_AnnotationStyleEditor_h16.png) [Annotation styles...](https://wiki.freecad.org/Draft_AnnotationStyleEditor): allows you to define styles that affect the visual properties of annotation-like objects. 

## Modification <a id="modification-"></a><sup>[▴](#contents)</sup>

* ![](DraftWorkbench_files/img_h16/Draft_Move_h16.png) [Move](https://wiki.freecad.org/Draft_Move): moves or copies selected objects from one point to another.
* ![](DraftWorkbench_files/img_h16/Draft_Rotate_h16.png) [Rotate](https://wiki.freecad.org/Draft_Rotate): rotates or copies selected objects around a center point by a given angle.
* ![](DraftWorkbench_files/img_h16/Draft_Scale_h16.png) [Scale](https://wiki.freecad.org/Draft_Scale): scales or copies selected objects around a base point.
* ![](DraftWorkbench_files/img_h16/Draft_Mirror_h16.png) [Mirror](https://wiki.freecad.org/Draft_Mirror): creates mirrored copies from selected objects.
* ![](DraftWorkbench_files/img_h16/Draft_Offset_h16.png) [Offset](https://wiki.freecad.org/Draft_Offset): offsets each segment of a selected object over a given distance, or creates an offset copy of the selected object.
* ![](DraftWorkbench_files/img_h16/Draft_Trimex_h16.png) [Trimex](https://wiki.freecad.org/Draft_Trimex): trims or extends a selected object.
* ![](DraftWorkbench_files/img_h16/Draft_Stretch_h16.png) [Stretch](https://wiki.freecad.org/Draft_Stretch): stretches objects by moving selected points.
* ![](DraftWorkbench_files/img_h16/Draft_Clone_h16.png) [Clone](https://wiki.freecad.org/Draft_Clone): creates linked copies, clones, of selected objects.
* ![](DraftWorkbench_files/img_h16/Draft_OrthoArray_h16.png) Array tools
    * ![](DraftWorkbench_files/img_h16/Draft_OrthoArray_h16.png) [Array](https://wiki.freecad.org/Draft_OrthoArray): creates an orthogonal array from a selected object. It can optionally create a [Link](https://wiki.freecad.org/App_Link) array.
    * ![](DraftWorkbench_files/img_h16/Draft_PolarArray_h16.png) [Polar array](https://wiki.freecad.org/Draft_PolarArray): creates an array from a selected object by placing copies along a circumference. It can optionally create a [Link](https://wiki.freecad.org/App_Link) array.
    * ![](DraftWorkbench_files/img_h16/Draft_CircularArray_h16.png) [Circular array](https://wiki.freecad.org/Draft_CircularArray): creates an array from a selected object by placing copies along concentric circumferences. It can optionally create a [Link](https://wiki.freecad.org/App_Link) array.
    * ![](DraftWorkbench_files/img_h16/Draft_PathArray_h16.png) [Path array](https://wiki.freecad.org/Draft_PathArray): creates an array from a selected object by placing copies along a path.
    * ![](DraftWorkbench_files/img_h16/Draft_PathLinkArray_h16.png) [Path Link array](https://wiki.freecad.org/Draft_PathLinkArray): idem, but create a [Link](https://wiki.freecad.org/App_Link) array instead of a regular array.
    * ![](DraftWorkbench_files/img_h16/Draft_PointArray_h16.png) [Point Array](https://wiki.freecad.org/Draft_PointArray): creates an array from a selected object by placing copies at the points from a point compound.
    * ![](DraftWorkbench_files/img_h16/Draft_PointLinkArray_h16.png) [Point Link array](https://wiki.freecad.org/Draft_PointLinkArray): idem, but create a [Link](https://wiki.freecad.org/App_Link) array instead of a regular array.
* ![](DraftWorkbench_files/img_h16/Draft_Edit_h16.png) [`Draft_Edit`](https://wiki.freecad.org/Draft_Edit): puts selected objects in Draft Edit mode. In this mode the properties of objects can be edited graphically.
    * Supported Draft objects can also be put in Draft Edit mode with the Std Edit ![Std_Edit_h16](DraftWorkbench_files/img_h16/Std_Edit_h16.png) command.
* ![](DraftWorkbench_files/img_h16/Draft_SubelementHighlight_h16.png) [Subelement highlight](https://wiki.freecad.org/Draft_SubelementHighlight): temporarily highlights selected objects, or the base objects of selected objects.
* ![](DraftWorkbench_files/img_h16/Draft_Join_h16.png) [Join](https://wiki.freecad.org/Draft_Join): joins [Draft Lines](https://wiki.freecad.org/Draft_Line) and [Draft Wires](https://wiki.freecad.org/Draft_Wire) into a single wire.
* ![](DraftWorkbench_files/img_h16/Draft_Split_h16.png) [Split](https://wiki.freecad.org/Draft_Split): splits a [Draft Line](https://wiki.freecad.org/Draft_Line) or [Draft Wire](https://wiki.freecad.org/Draft_Wire) at a specified point or edge.
* ![](DraftWorkbench_files/img_h16/Draft_Upgrade_h16.png) [Upgrade](https://wiki.freecad.org/Draft_Upgrade): upgrades selected objects.
* ![](DraftWorkbench_files/img_h16/Draft_Downgrade_h16.png) [Downgrade](https://wiki.freecad.org/Draft_Downgrade): downgrades selected objects.
* ![](DraftWorkbench_files/img_h16/Draft_WireToBSpline_h16.png) [Wire to B-spline](https://wiki.freecad.org/Draft_WireToBSpline): converts [Draft Wires](https://wiki.freecad.org/Draft_Wire) to [Draft BSplines](https://wiki.freecad.org/Draft_BSpline) and vice versa.
* ![](DraftWorkbench_files/img_h16/Draft_Draft2Sketch_h16.png) [Draft to Sketch](https://wiki.freecad.org/Draft_Draft2Sketch): converts 
Draft
 objects to [Sketcher Sketches](https://wiki.freecad.org/Sketcher_NewSketch) and vice versa.
* ![](DraftWorkbench_files/img_h16/Draft_Slope_h16.png) [Set slope](https://wiki.freecad.org/Draft_Slope): slopes selected [Draft Lines](https://wiki.freecad.org/Draft_Line) or [Draft Wires](https://wiki.freecad.org/Draft_Wire) by increasing, or decreasing, the Z coordinate of all points after the first one.
* ![](DraftWorkbench_files/img_h16/Draft_FlipDimension_h16.png) [Flip dimension](https://wiki.freecad.org/Draft_FlipDimension): rotates the dimension text of selected [Draft Dimensions](https://wiki.freecad.org/Draft_Dimension) 180° around the dimension line.
* ![](DraftWorkbench_files/img_h16/Draft_Shape2DView_h16.png) [Shape 2D view](https://wiki.freecad.org/Draft_Shape2DView): creates 2D projections from selected objects.

## Draft Tray <a id="draft-tray-"></a><sup>[▴](#contents)</sup>

The [Draft Tray](https://wiki.freecad.org/Draft_Tray) allows selecting the working plane, defining style settings, toggling construction mode, and specifying the active layer or group.

![](DraftWorkbench_files/Draft_tray_default.png)

* ![](DraftWorkbench_files/Draft_tray_button_plane.png) [Select Plane](https://wiki.freecad.org/Draft_SelectPlane): current Draft working plane. **Draft WB Menu: `Utilities` > ![](DraftWorkbench_files/img_h16/Draft_SelectPlane_h16.png) `Select Plane`**.
* ![](DraftWorkbench_files/Draft_tray_button_style.png) [Set style `Draft_SetStyle`](https://wiki.freecad.org/Draft_SetStyle): default new object style. **Draft WB Menu: `Utilities` > ![](DraftWorkbench_files/img_h16/Draft_SetStyle_h16.png) `Set style`**.
* ![](DraftWorkbench_files/Draft_tray_button_construction.png) [Toggle construction mode](https://wiki.freecad.org/Draft_ToggleConstructionMode): switches Draft construction mode on or off. **Draft WB Menu: `Utilities` > ![](DraftWorkbench_files/img_h16/Draft_ToggleConstructionMode_h16.png) `Toggle construction mode`**.
* ![](DraftWorkbench_files/Draft_tray_button_layer.png) [AutoGroup](https://wiki.freecad.org/Draft_AutoGroup): changes the active [Draft Layer](https://wiki.freecad.org/Draft_Layer) or, optionally, the active [Std Group](https://wiki.freecad.org/Std_Group) or group-like [Arch](https://wiki.freecad.org/Arch_Workbench) object.

## Draft Annotation Scale Widget <a id="draft-annotation-scale-widget-"></a><sup>[▴](#contents)</sup>

With the [Draft annotation scale widget](https://wiki.freecad.org/Draft_annotation_scale_widget) the Draft annotation scale can be specified. 

![](DraftWorkbench_files/Draft_annotation_scale_widget_button.png)

## Draft Snap Widget <a id="draft-snap-widget-"></a><sup>[▴](#contents)</sup>

The [Draft snap widget](https://wiki.freecad.org/Draft_snap_widget) can be used as an alternative for the [Draft Snap toolbar](#Draft_Snap_toolbar). 

![](DraftWorkbench_files/Draft_snap_widget_button.png)

## Draft Snap Toolbar <a id="draft-snap-toolbar-"></a><sup>[▴](#contents)</sup>

The Draft Snap toolbar allows selecting the active snap options. The buttons belonging to active options stay depressed. For general information about snapping see: [Draft Snap](https://wiki.freecad.org/Draft_Snap).

* ![](DraftWorkbench_files/img_h16/Draft_Snap_Lock_h16.png) [Snap Lock](https://wiki.freecad.org/Draft_Snap_Lock): enables or disables snapping globally.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Endpoint_h16.png) [Snap Endpoint](https://wiki.freecad.org/Draft_Snap_Endpoint): snaps to the endpoints of edges.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Midpoint_h16.png) [Snap Midpoint](https://wiki.freecad.org/Draft_Snap_Midpoint): snaps to the midpoint of edges.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Center_h16.png) [Snap Center](https://wiki.freecad.org/Draft_Snap_Center): snaps to the center point of faces and circular edges, and to the 
Data

**Placement** point of [Draft WorkingPlaneProxies](https://wiki.freecad.org/Draft_WorkingPlaneProxy) and [Arch BuildingParts](https://wiki.freecad.org/Arch_BuildingPart).

* ![](DraftWorkbench_files/img_h16/Draft_Snap_Angle_h16.png) [Snap Angle](https://wiki.freecad.org/Draft_Snap_Angle): snaps to the special cardinal points on circular edges, at multiples of 30° and 45°.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Intersection_h16.png) [Snap Intersection](https://wiki.freecad.org/Draft_Snap_Intersection): snaps to the intersection of two edges.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Perpendicular_h16.png) [Snap Perpendicular](https://wiki.freecad.org/Draft_Snap_Perpendicular): snaps to the perpendicular point on edges.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Extension_h16.png) [Snap Extension](https://wiki.freecad.org/Draft_Snap_Extension): snaps to an imaginary line that extends beyond the endpoints of straight edges.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Parallel_h16.png) [Snap Parallel](https://wiki.freecad.org/Draft_Snap_Parallel): snaps to an imaginary line parallel to straight edges.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Special_h16.png) [Snap Special](https://wiki.freecad.org/Draft_Snap_Special): snaps to special points defined by the object.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Near_h16.png) [Snap Near](https://wiki.freecad.org/Draft_Snap_Near): snaps to the nearest point on faces or edges.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Ortho_h16.png) [Snap Ortho](https://wiki.freecad.org/Draft_Snap_Ortho): snaps to imaginary lines that cross the previous point at 0°, 45°, 90° and 135°.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Grid_h16.png) [Snap Grid](https://wiki.freecad.org/Draft_Snap_Grid): snaps to the intersections of grid lines.
* ![](DraftWorkbench_files/img_h16/Draft_Snap_WorkingPlane_h16.png) [Snap WorkingPlane](https://wiki.freecad.org/Draft_Snap_WorkingPlane): projects the snap point onto the current [working plane](https://wiki.freecad.org/Draft_SelectPlane).
* ![](DraftWorkbench_files/img_h16/Draft_Snap_Dimensions_h16.png) [Snap Dimensions](https://wiki.freecad.org/Draft_Snap_Dimensions): shows temporary X and Y dimensions.
* ![](DraftWorkbench_files/img_h16/Draft_ToggleGrid_h16.png) [Toggle Grid](https://wiki.freecad.org/Draft_ToggleGrid): switches the grid on or off.

## Draft Utility Tools Toolbar <a id="draft-utility-tools-toolbar-"></a><sup>[▴](#contents)</sup>

* ![](DraftWorkbench_files/img_h16/Draft_Layer_h16.png) [Layer](https://wiki.freecad.org/Draft_Layer): creates a [Draft Layer](https://wiki.freecad.org/Draft_Layer). 
* ![](DraftWorkbench_files/img_h16/Draft_AddNamedGroup_h16.png) [Add a new named group](https://wiki.freecad.org/Draft_AddNamedGroup): creates a named [Std Group](https://wiki.freecad.org/Std_Group) and moves selected objects to that group. 
* ![](DraftWorkbench_files/img_h16/Draft_AddToGroup_h16.png) [Move to group...](https://wiki.freecad.org/Draft_AddToGroup): moves objects to a [Std Group](https://wiki.freecad.org/Std_Group). It can also ungroup objects.
* ![](DraftWorkbench_files/img_h16/Draft_SelectGroup_h16.png) [Select group](https://wiki.freecad.org/Draft_SelectGroup): selects the contents of [Std Groups](https://wiki.freecad.org/Std_Group) or group-like [Arch](https://wiki.freecad.org/Arch_Workbench) objects.
* ![](DraftWorkbench_files/img_h16/Draft_AddConstruction_h16.png) [Add to Construction group](https://wiki.freecad.org/Draft_AddConstruction): moves objects to the [Draft construction group](https://wiki.freecad.org/Draft_ToggleConstructionMode).
* ![](DraftWorkbench_files/img_h16/Draft_ToggleDisplayMode_h16.png) [Toggle normal/wireframe display](https://wiki.freecad.org/Draft_ToggleDisplayMode): switches the 
View

**Display Mode** property of selected objects between `Flat Lines` and `Wireframe`.

* ![](DraftWorkbench_files/img_h16/Draft_WorkingPlaneProxy_h16.png) [Create working plane proxy](https://wiki.freecad.org/Draft_WorkingPlaneProxy): creates a working plane proxy to save the current [Draft working plane](https://wiki.freecad.org/Draft_SelectPlane).

## Additional Tools <a id="additional-tools-"></a><sup>[▴](#contents)</sup>

The **Draft > Utilities** menu contains several tools. Most of them can also be accessed from toolbars or the [Draft Tray](https://wiki.freecad.org/Draft_Tray) and have already been mentioned above. For the following tools this is not the case:

* ![](DraftWorkbench_files/img_h16/Draft_ApplyStyle_h16.png) [Apply current style](https://wiki.freecad.org/Draft_ApplyStyle): applies the current style settings to selected objects.
* ![](DraftWorkbench_files/img_h16/Draft_Heal_h16.png) [Heal](https://wiki.freecad.org/Draft_Heal): heals problematic Draft objects found in very old files.
* ![](DraftWorkbench_files/img_h16/Draft_ToggleContinueMode_h16.png) [Toggle continue mode](https://wiki.freecad.org/Draft_ToggleContinueMode): switches continue mode on or off.
* ![](DraftWorkbench_files/img_h16/Draft_ShowSnapBar_h16.png) [Show snap toolbar](https://wiki.freecad.org/Draft_ShowSnapBar): shows the [Draft Snap toolbar](#Draft_Snap_toolbar).

## Additional Features <a id="additional-features-"></a><sup>[▴](#contents)</sup>

* [Working plane](https://wiki.freecad.org/Draft_SelectPlane): the plane in the [3D view](https://wiki.freecad.org/3D_view) where new Draft objects are created.
* [Snapping](https://wiki.freecad.org/Draft_Snap): select exact geometric points on, or defined by, existing objects or the grid.
* [Constraining](https://wiki.freecad.org/Draft_Constrain): for each subsequent point you can constrain the movement of the cursor to the X, Y, or Z direction.
* [Construction mode](https://wiki.freecad.org/Draft_ToggleConstructionMode): places new Draft objects in a dedicated group making it easier to hide or delete them.
* [Pattern](https://wiki.freecad.org/Draft_Pattern): Draft objects with a 
Data
**Make Face** property can display an SVG pattern instead of a solid face color.

## Tree View Context Menu <a id="tree-view-context-menu-"></a><sup>[▴](#contents)</sup>

The following additional options are available in the [Tree view](https://wiki.freecad.org/Tree_view) context menu:

**Default options**

If there is an active document the context menu contains one additional sub-menu:
* **Utilities**: a subset of the tools available in the main Draft Utilities menu.

**Wire options**

For a [Draft Wire](https://wiki.freecad.org/Draft_Wire), [Draft BSpline](https://wiki.freecad.org/Draft_BSpline), [Draft CubicBezCurve](https://wiki.freecad.org/Draft_CubicBezCurve) and [Draft BezCurve](https://wiki.freecad.org/Draft_BezCurve) this additional option is available:

* ![](DraftWorkbench_files/img_h16/Draft_Edit_h16.png) `Draft_Edit` Flatten this wire: flattens the wire based on its internal geometry.
    * This option currently does not work properly. (which version?)

**Layer container options**

For a [Draft LayerContainer](https://wiki.freecad.org/Draft_Layer) these additional options are available:

* ![](DraftWorkbench_files/img_h16/Draft_Layer_h16.png) [Merge layer duplicates](https://wiki.freecad.org/Draft_Layer#Layer_container_options): merges all layers with the same base label. This does not work in FreeCAD version 0.19.
* ![](DraftWorkbench_files/img_h16/Draft_NewLayer_h16.png) [Add new layer](https://wiki.freecad.org/Draft_Layer#Layer_container_options): adds a new layer to the current document.

**Layer options**

For a [Draft Layer](https://wiki.freecad.org/Draft_Layer) these additional options are available:

* ![](DraftWorkbench_files/img_h16/Button_right_h16.png) [Activate this layer](https://wiki.freecad.org/Draft_AutoGroup): activates the selected layer.
* ![](DraftWorkbench_files/img_h16/Draft_SelectGroup_h16.png) [Select layer contents](https://wiki.freecad.org/Draft_SelectGroup): selects the objects inside the selected layer.

**Working plane proxy options**

For a [Draft WorkingPlaneProxy](https://wiki.freecad.org/Draft_WorkingPlaneProxy) these additional options are available:

* ![](DraftWorkbench_files/img_h16/Draft_SelectPlane_h16.png) [Write camera position](https://wiki.freecad.org/Draft_WorkingPlaneProxy#Context_menu): updates the 
View

**View Data** property of the working plane proxy with the current [3D view](https://wiki.freecad.org/3D_view) camera settings.

* ![](DraftWorkbench_files/img_h16/Draft_SelectPlane_h16.png) [Write objects state](https://wiki.freecad.org/Draft_WorkingPlaneProxy#Context_menu): updates the 
View

**Visibility Map** property of the working plane proxy with the current visibility state of objects in the document.

## 3D View Context Menu <a id="3d-view-context-menu-"></a><sup>[▴](#contents)</sup>

The following additional options are available in the [3D view](https://wiki.freecad.org/3D_view) context menu:

**Default options**

If there is an active document the context menu contains one additional sub-menu:

* **Utilities**: a subset of the tools available in the main Draft Utilities menu.

## Obsolete Tools <a id="obsolete-tools-"></a><sup>[▴](#contents)</sup>

These commands are obsolete but still available:

* ![](DraftWorkbench_files/img_h16/Draft_Array_h16.png) [Array](https://wiki.freecad.org/Draft_Array): creates an orthogonal array from a selected object. The created array can be turned into a [polar array](https://wiki.freecad.org/Draft_PolarArray) or a [circular array](https://wiki.freecad.org/Draft_CircularArray) by changing its 
Data

**Array Type** property. 
[obsolete in version 0.19](https://wiki.freecad.org/Release_notes_0.19)

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

**FreeCAD Documentation**

* [Draft Workbench](https://wiki.freecad.org/Draft_Workbench)