# Maker Tales: FreeCAD is Broken! [⇗](https://gitlab.com/mc_qref/621-freecad-notes)

[<< Return to 621-freecad-notes/README.md](README.md)

_The current FreeCAD has a "Topological Naming Problem"._

_The [Maker Tales video "FreeCAD Is Fundamentally Broken! - Now what…"](https://www.youtube.com/watch?v=QSsVFu929jo) explains the "Topological Naming Problem". Summarizes popular workarounds. Asks for input on what approach the Maker Tales FreeCAD tutorials should take._

## Contents <a id="contents"></a>
[Problem Summary](#problem-summary-) •
[Candidate Workarounds](#candidate-workarounds-) •
[More In-Depth Information](more-in-depth-information-) •
[Time Stamps](#time-stamps-) •
[Resources](#resources-)

## Problem Summary <a id="problem-summary-"></a><sup>[▴](#contents)</sup>

"Sketches" are 2D drawings (surfaces) which can be controlled with numbers to create accurate 3D solid models.  Each 3D solid body has a list of faces. (`Body_B`: `FaceName_9`, `FaceName_10`, …)

Bug: A body `FaceName` list may change topologically when a body is edited. Thus, if `Body_C` is linked to a `Body_B` `FaceName_N` which has topologically changed, then the subsequent `Body_B`/`Body_C` connection will be incorrect.

![Fig 1. Starting Point](00_FreeCAD_Broken_files/BodyBefore.png)

![Fig 2. After Correct Edit](00_FreeCAD_Broken_files/BodyEditCorrect.png)

![Fig 3. After Edit Problem](00_FreeCAD_Broken_files/BodyEditProblem.png)

![Fig 4. What Happened](00_FreeCAD_Broken_files/BodyEditWhatHappened.png)

## Candidate Workarounds <a id="candidate-workarounds-"></a><sup>[▴](#contents)</sup>

**Manually Reassign References** 

* Manually reassign and recompute the face reference for the sketch every single time there is a naming list edit.
* Workflow: Tedious and prone to errors

**Use Origin Reference Datum Planes**

* Only use origin reference datum planes with equations. 
* Don't use any face references at all. Link up everything with origin reference datum planes along with mostly simple equations to link them up to the bodies.
* Workflow: Provides robust designs for serious engineering projects. This approach and associated concepts may be too much for beginners and simple hobby projects.

**Use Unofficial (RealThunder) FreeCAD**

* Use the RealThunder branch that has fixed the "Topological Naming Problem" issue.
* Workflow: Project files may have some incompatibility issues between the official and unofficial branch. So, the project may become somewhat locked to the branch.

**Use (Prerelease) FreeCAD v0.20**

* The official FreeCAD v0.20 developer is working to fix the "Topological Naming Problem".
* Workflow: Might run into "beta software" issues.

## More In-Depth Information <a id="more-in-depth-information-"></a><sup>[▴](#contents)</sup>

**FreeCAD Wiki**

* [Topological naming problem ⇗](https://wiki.freecadweb.org/Topological_naming_problem)
* [Advice for creating stable models ⇗](https://wiki.freecadweb.org/Feature_editing#Advice_for_creating_stable_models)

## Time Stamps <a id="time-stamps-"></a><sup>[▴](#contents)</sup>

* 00:00 - Intro.
* 00:15 - What IS Broken.
* 00:38 - Understanding CAD Design & The Topological Naming Problem.
* 01:40 - The Point Of A CAD program * The "Problem".
* 02:10 - FreeCAD's "Problem" Broken Down.
* 03:38 - My Thoughts On The "Problem" & Where I'm Coming From.
* 04:20 - Top Solutions That I Know Of.
* 04:30 - Manually Reassign Sketches To The Right Face.
* 04:41 - No Face References & Use A Datum Plain Workflow.
* 04:51 - Don't Use FreeCAD Release. Instead, Use RealThunder Branch.
* 05:06 - The Point Of All Of This...
* 05:24 - Reason Against Manually Reassign Sketches.
* 05:32 - Reason Against A Datum Plain Workflow.
* 06:55 - Reason Against RealThunder's Branch.
* 08:27 - I Care Deeply & We As A FreeCAD Community Are So Close.
* 09:15 - So I Need Your Help...

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [2021.07.24 FreeCAD Is Fundamentally Broken! - Now what... Help Me Decide... ⇗](https://www.youtube.com/watch?v=QSsVFu929jo)
* [FreeCAD Website ⇗](https://www.freecadweb.org/), [weekly builds ⇗](https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds)
* [FreeCAD RealThunder branch: releases ⇗](https://github.com/realthunder/FreeCAD_assembly3/releases)
* [GitLab: 621-FreeCAD-Notes ⇗](https://gitlab.com/mc_qref/621-freecad-notes) / [README.md ⇗](https://gitlab.com/mc_qref/621-freecad-notes/-/blob/main/README.md)
