# FreeCAD: ![](SpreadsheetWorkbench_files/img_h16/Workbench_Spreadsheet_h16.png) [Spreadsheet Workbench][sw]

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Questions](#questions-)
[Resources](#resources-)

* Create label-value alias relationship
    * Context Menu
        1. Select name label cell & `cmd-C` copy. 
        2. Right-click value cell, select `Properties…`, select `Alias` tab & `cmd-V` paste 
    * Workbench Toolbar
        1. Select name label cell & `cmd-C` copy.
        2. Select value cell & apply `Speadsheet_SetAlias` `cmd-shift-A` 

**Short Answer**

> Where does the spreadsheet reside? 

A FreeCAD "spreadsheet" exists as an `XML` object inside the `Document.xml​` file within the `.FCStd`​ zipped container.

> Can one put the spreadsheet in its own file and then reference it from individual part files?

Yes, an `.FCStd`​ file which contains only a Spreadsheet can be used in other `.FCStd`​ documents. There are some caveats. See the "Cross-document linking" section of the Expressions wiki page.

* <https://wiki.freecad.org/Expressions#Cross-document_linking>

**More Details…**

Spreadsheet data is stored inside the `Document.xml` file contained within the FreeCAD `.FCStd` zipped container. Unzip `.FCStd` to see the contents which will include the `Document.xml`.

Below is a "Spreadsheet" snippet from `Document.xml`.

``` xml
<Object name="Spreadsheet">
    <Properties Count="7" TransientCount="0">
        <Property name="ExpressionEngine" type="App::PropertyExpressionEngine" status="67108864">
            <ExpressionEngine count="0">
            </ExpressionEngine>
        </Property>
        <Property name="Label" type="App::PropertyString" status="134217728">
            <String value="MySpreadsheetName"/>
        </Property>
        <Property name="cells" type="Spreadsheet::PropertySheet" status="67108864">
            <Cells Count="95" xlink="1">
                <Cell address="A2" content="SD_Micro_X" />
                <Cell address="B2" content="=15mm" displayUnit="mm" alias="SD_Micro_X" />
                <Cell address="D2" content="Gap_Micro_X" />
                <Cell address="E2" content="=0.1mm" displayUnit="mm" alias="Gap_Micro_X" />
```

The "Spreadsheet" cells are later referenced throughout the FreeCAD models. Below is a snippet of Expression Engine contraints which use the named spreadsheet:

``` sh
<Property name="ExpressionEngine" type="App::PropertyExpressionEngine" status="67108864">
    <ExpressionEngine count="11">
        <Expression path="Constraints[26]" expression="&lt;&lt;MySpreadsheetName&gt;&gt;.Tab_X"/>
        <Expression path="Constraints[31]" expression="&lt;&lt;MySpreadsheetName&gt;&gt;.Wall_Thickness + &lt;&lt;MySpreadsheetName&gt;&gt;.Clip_Width"/>
        <Expression path="Constraints[33]" expression="&lt;&lt;MySpreadsheetName&gt;&gt;.Case_Micro_X - &lt;&lt;MySpreadsheetName&gt;&gt;.Clip_Width"/>
        <Expression path="Constraints[34]" expression="&lt;&lt;MySpreadsheetName&gt;&gt;.Case_Micro_Yn - 2 * &lt;&lt;MySpreadsheetName&gt;&gt;.Clip_Width"/>
    </ExpressionEngine>
</Property>
```

FreeCAD "spreadsheet" characteristics:

* exists as an XML object inside the .FCStd zip container.
* can import/export "CSV" formatted files. Default preference: tab separator (aka .TSV format)
    * built-in export saves resulting values and not the original expressions

Python can be used to extend the FreeCAD Spreadsheet functionality.

``` py
import Spreadsheet
sheet = App.ActiveDocument.addObject("Spreadsheet::Sheet","MySpreadsheet")
sheet.Label = "Dimensions"

sheet.set('A1','10mm')
sheet.recompute()
sheet.get('A1')

sheet.setAlias('B1','Diameter')
sheet.set('Diameter','20mm')
sheet.recompute()
sheet.get('Diameter')
```

* <https://wiki.freecad.org/Spreadsheet_Workbench#Scripting_basics>

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

The ![](SpreadsheetWorkbench_files/img_h16/Workbench_Spreadsheet_h16.png) Spreadsheet Workbench can 

* create and edit spreadsheet
* provide parametric data to a model
* fill cells with data retrieved from a model
* perform calculations (expressions)
* import and export CSV data
* create at the document level or a lower (e.g. part) level
    * if a part which uses a document level spreadsheet is duplicated, then the document level spreadsheet is also duplicated.
    * helpful to choose spreadsheet names which reflect the level where created (e.g. Params_Part_A)

![](SpreadsheetWorkbench_files/600px-Spreadsheet_screenshot.jpg)

## Tools

* ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_CreateSheet_h16.png) [Create sheet](https://wiki.freecad.org/Spreadsheet_CreateSheet): create a new spreadsheet.
* ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_Import_h16.png) [Import](https://wiki.freecad.org/Spreadsheet_Import): import a CSV file into a spreadsheet.
* ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_Export_h16.png) [Export](https://wiki.freecad.org/Spreadsheet_Export): export a CSV file from a spreadsheet.
* Cells
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_MergeCells_h16.png) [Merge cells](https://wiki.freecad.org/Spreadsheet_MergeCells): merge selected cells.
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_SplitCell_h16.png) [Split cell](https://wiki.freecad.org/Spreadsheet_SplitCell): split previously merged cells.
* Alignment
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_AlignLeft_h16.png) [Align left](https://wiki.freecad.org/Spreadsheet_AlignLeft): align the contents of selected cells to the left.
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_AlignCenter_h16.png) [Align center](https://wiki.freecad.org/Spreadsheet_AlignCenter): align the contents of selected cells to the center horizontally.
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_AlignRight_h16.png) [Align right](https://wiki.freecad.org/Spreadsheet_AlignRight): align the contents of selected cells to the right.
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_AlignTop_h16.png) [Align top](https://wiki.freecad.org/Spreadsheet_AlignTop): align the contents of selected cells to the top.
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_AlignVCenter_h16.png) [Align vertical center](https://wiki.freecad.org/Spreadsheet_AlignVCenter): align the contents of selected cells to the center vertically.
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_AlignBottom_h16.png) [Align bottom](https://wiki.freecad.org/Spreadsheet_AlignBottom): top align the contents of selected cells to the bottom.
* Styles
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_StyleBold_h16.png) [Style bold](https://wiki.freecad.org/Spreadsheet_StyleBold): set the contents of selected cells to bold.
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_StyleItalic_h16.png) [Style italic](https://wiki.freecad.org/Spreadsheet_StyleItalic): set the contents of selected cells to italic.
    * ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_StyleUnderline_h16.png) [Style underline](https://wiki.freecad.org/Spreadsheet_StyleUnderline): set the contents of selected cells to underlined.
    * set foreground|background colors of selected cells
* ![](SpreadsheetWorkbench_files/img_h16/Spreadsheet_SetAlias_h16.png) [Set alias](https://wiki.freecad.org/Spreadsheet_SetAlias): set the alias for a selected cell.

## Insert/Remove/Copy/Paste

Either hold down the Cmd (macOS) | Ctrl (Linux/Windows) key while selecting, or hold down the left mouse button and dragging for a multi-selection.

> Note: _no prewarnings are proviced_ on whether removing cells with data will break the spreadsheet or the model
>
> Note: _good practice to save before pasting._

## Cell properties

The properties of a spreadsheet cell can be edited with a right-click on a cell. The following dialog pops up:

![](SpreadsheetWorkbench_files/SpreadsheetCellPropDialog.png)

As indicated by the tabs, the following properties can be changed:

* Color: Text color and background color
* Alignment: Text horizontal and vertical alignment
* Style: Text style: bold, italic, underline
* Units: Display units for this cell. Please read the [Units](#Units) section below.
* Alias: Define an [alias](https://wiki.freecad.org/Spreadsheet_SetAlias) for this cell. This alias can be used in cell formulas and also in general [expressions](https://wiki.freecad.org/Expressions); see section [Spreadsheet data in expressions](#Spreadsheet_data_in_expressions) for more information.

## Cell expressions

* starts with an equals `=` sign. 
* numbers: `.` decimal, `,` thousands
* constants: `e`, `pi`
* operators: `+`, `-`, `*`, `/`, `%`, `^` 
* [functions](https://wiki.freecad.org/Expressions#Supported_functions)
* cell coordinate references are column (CAPITAL letter) and row (number) `B4 + A6`
* cell [alias-name](#alias_name) references (`A-Z`, `a-z`, `0-9` and `_`) 
* non-cyclic property model references

> **Note:** Cell expressions are treated by FreeCAD as programming code.
> 
> * the decimal separator is always a dot
> * the number of displayed decimals can differ from the [preferences settings](https://wiki.freecad.org/Preferences_Editor#Units)

References to objects in the model are explained under [References to CAD-data](#References_to_CAD-data) below. Using spreadsheet cell values to define model properties are explained under [Spreadsheet data in expressions](#Spreadsheet_data_in_expressions) below. For more information on expressions and the available functions, see [Expressions](https://wiki.freecad.org/Expressions).

## Interaction between spreadsheets and the CAD model

_Data in the cells of a spreadsheet may be used in CAD model parameter expressions._

When values are changed in the spreadsheet, they are propagated throughout the model. If the name of an object in the CAD model is changed, the change will automatically be propagated to any references in spreadsheet expressions using the name which was changed.

* More than one spreadsheet may be used in a document.

The unchangeable unique spreadsheet name can be used to refer to the spreadsheet in an [Expression](https://wiki.freecad.org/Expressions) (see [Spreadsheet data in expressions](#Spreadsheet_data_in_expressions) below.)

The label of a spreadsheet is automatically set to the name of the spreadsheet upon creation. The label can be renamed.

### References to CAD-data

As indicated above, one can reference data from the CAD model in spreadsheet expressions.

Computed expressions in spreadsheet cells start with an equals (`=`) sign. However, the spreadsheet entry mechanism attempts to be smart. An expression may be entered without the leading `=`; if the string entered is a valid expression, an `=` is automatically added when the final Enter is typed. If the string entered is not a valid expression (often the result of entering something with the wrong case, e.g. "MyCube.length" instead of), no leading `=` is added and it is treated as simply a text string.

**Note:** The above behavior (auto insert of `=`) has some unpleasant ramifications:

* If you want to keep a column of names corresponding to the [alias-names](#alias_name) in an adjacent column of values, you must enter the name in the label column *before* giving the cell in the value column its alias-name. Otherwise, when you enter the alias-name in the label column the spreadsheet will assume it is an expression and change it to "=&lt;alias-name&gt;"; and the displayed text will be the value from the &lt;alias-name&gt; cell.
* If you make an error when entering the name in the label column and wish to correct it, you cannot simply change it to the alias-name. Instead, you must first change the alias-name to something else, then fix the text name in the label column, then change the alias-name in the value column back to its original.

One way to side-step these issues is to prefix text labels corresponding to alias-names with a fixed string, thereby making them different. Note that "\_" will not work, as it is converted to "=". However, a blank, while invisible, will work.

The following table shows some examples assuming the model has a feature named "MyCube":

| CAD-Data                                   | Cell in Spreadsheet          | Result                           |
|--------------------------------------------|------------------------------|----------------------------------|
| Parametric Length of a Part-Workbench Cube | =MyCube.Length               | Length with units mm             |
| Volume of the Cube                         | =MyCube.Shape.Volume         | Volume in mm³ without units      |
| Type of the Cube-shape                     | =MyCube.Shape.ShapeType      | String: Solid                    |
| Label of the Cube                          | =MyCube.Label                | String: MyCube                   |
| x-coordinate of center of mass of the Cube | =MyCube.Shape.CenterOfMass.x | x-coordinate in mm without units |

### Spreadsheet data in expressions

In order to use spreadsheet data in other parts of FreeCAD, you will usually create an [Expression](https://wiki.freecad.org/Expressions) that refers to the spreadsheet and the cell that contains the data you want to use. You can identify spreadsheets by name or by label, and you can identify the cells by position or by alias. Autocompletion is available for all forms of referencing.

|                  | Spreadsheet by Name       | Spreadsheet by Label         |
|------------------|---------------------------|------------------------------|
| Cell by Position | `=Spreadsheet042.B5`      | `=<<MySpreadsheet>>.B5`      |
| Cell by Alias    | `=Spreadsheet042.MyAlias` | **`=<<MySpreadsheet>>.MyAlias`** |

> Recommendation: prefer spreadsheet label and cell alias name.

### Complex models and recomputes

Editing a spreadsheet will trigger a recompute of the 3D model, even if the changes do not affect the model. For a complex model a recompute can take a long time, and having to wait after every single edit is of course quite annoying.

There are three solutions to deal with this:

1.  Temporarily skip recomputes:
    * In the [Tree view](https://wiki.freecad.org/Tree_view) right-click the ![](SpreadsheetWorkbench_files/img_h16/Document_h16.png) document that contains the spreadsheet.
    * Select the **Skip recomputes** option from the context menu.
    * There is a big disadvantage to this solution. New values entered in the spreadsheet will not be displayed until the document is recomputed. Instead `#PENDING` is shown.
    * You can either recompute manually, using the [Std Refresh](https://wiki.freecad.org/Std_Refresh) command, or disable **Skip recomputes** when you are done editing.
2.  Use a macro to automatically skip recomputes while editing a spreadsheet:
    * Download and run <a href="https://forum.freecadweb.org/viewtopic.php?f=8&amp;t=48600#p419301" rel="nofollow">skipSheet.FCMacro</a>.
    * This solution saves a few steps compared to the first solution, but also has the mentioned disadvantage.
3.  Put the spreadsheet in a separate [FreeCAD file](https://wiki.freecad.org/File_Format_FCStd):
    * You can reference spreadsheet data from an external .FCStd file with this syntax: `=NameOfFile#<<MySpreadsheet>>.MyAlias`.
    * The advantage of having the spreadsheet in another file over switching off recomputes is that the spreadsheet itself does get recomputed.
    * The disadvantage is that the model won't automatically recompute after changes to the spreadsheet.
    * In the scenario where you first open the 'spreadsheet' file, change one or more values and then open the 'model' file, there won't be any indication that the model needs to be recomputed. But if both files are open the [Std Refresh](https://wiki.freecad.org/Std_Refresh) icon will update correctly for the 'model' file after changes to the 'spreadsheet' file.

## Units

The Spreadsheet has a notion of dimension (units) associated with cell values.

* A number entered without an associated unit has no dimension.
* Enter unit immediately following the number value, with no intervening space. `17mm`
* An associated unit will be used in calculation
    * `a mm` * `b mm` becomes `c mm²`

If a cell contains a value which represents a dimension, it should be entered with its associated unit. While in many simple cases one can get by with a dimensionless value, it is unwise to not enter the unit. If a value representing a dimension is entered without its associated unit, there are some sequences of operations which cause FreeCAD to complain of incompatible units in an expression when it appears the expression should be valid. (This may be better understood by viewing <a href="https://forum.freecadweb.org/viewtopic.php?f=3&amp;t=34713&amp;p=292455#p292438" rel="nofollow">this thread</a> in the FreeCAD forums.)

_Associated Units vs Display Units_

* `Cell properties` `Display units` (if present) are for display purposes only and do not affect the value contained in the cell.
* The `Cell properties` `Display units` can differ from the associated content value units.
    * If different, `Display units` will perform a conversion.
        * `Diplay units` of `in` would cause a `5.08 mm` value to display as `2 in`
* A dimensionless number cannot be changed to a number with a unit by the cell properties dialog. 
* In order to change a dimensionless value to a value with a dimension, the value itself must be re-entered with its associated unit.

An associated dimension can be removed by multiplying by 1 with a reciprocal unit. `1/in`

## Importing and exporting

### CSV format

FreeCAD spreadsheets can be imported and exported to the CSV format. See [Spreadsheet Import](https://wiki.freecad.org/Spreadsheet_Import) and [Spreadsheet Export](https://wiki.freecad.org/Spreadsheet_Export) for more information.

### XLSX format

Excel-format XLSX can be imported with the [Std Import](https://wiki.freecad.org/Std_Import) command or the [Std Open](https://wiki.freecad.org/Std_Open) command. Supported features:

* All functions that are also available in the FreeCAD spreadsheet.
* Alias names for cells.
* Creates one FreeCAD spreadsheet per each Excel sheet.

## Printing

Insert FreeCAD spreadsheets into a [TechDraw Spreadsheet View](https://wiki.freecad.org/TechDraw_SpreadsheetView) for printing.

## Current limitations <a id="current-limitations-"></a><sup>[▴](#contents)</sup>

FreeCAD checks for cyclic dependencies. By design, that check stops at the level of the spreadsheet object. As a consequence, you should not have a spreadsheet which contains both cells whose values are used to specify parameters to the model, and cells whose values use output from the model. For example, you cannot have cells specifying the length, width, and height of an object, and another cell which references the total volume of the resulting shape. This restriction can be surmounted by having two spreadsheets: one used as a data-source for input parameters to the model and the other used for calculations based on resultant geometry-data.

## Questions <a id="questions-"></a><sup>[▴](#contents)</sup>

**Find/Report/Select Spreadsheet Alias Use Instances**

Q: Is there a way to find (or report or select) all the elements which use a given spreadsheet alias?

* [Forum: Is it possible to find out where my spreadsheet aliases are being used via a search?](https://forum.freecadweb.org/viewtopic.php?f=3&t=59380)

> Perhaps you could modify Macro TreeToAscii to show this information?
(or petition the macro author to add it)

Function to list all the objects of the active document that use the given string in expressions:

``` python
def search_alias_connection(name=""):
    """Search all the objects of current document that contain
    the given string in an expression"""
    doc = App.ActiveDocument
    for o in doc.Objects:
        if hasattr(o, "ExpressionEngine"):
            for exp in o.ExpressionEngine:
                if name in exp[1] or name is None:
                    print("{} used in property {} of object {}".format(exp[1], exp[0], o.Label))

search_alias_connection("Alias_1")
```

you can then perform new searches by simply repeating the last command with other string : 

```
search_alias_connection("Alias_2")
```

Can generate a function for the above Python. [Should this be part of FC - Spreadsheet -> Select cell -> Alias .. Where used ...?]

Q: Is there a way to find unused spreadsheet aliased?

_Unused aliases won't show up in the `Tools` > [`Dependency graph…`](https://wiki.freecadweb.org/Std_DependencyGraph)._

* GraphViz installs via homebrew as: /usr/local/bin/dot
* Save a dependency graph:
    * Make sure the Dependency graph tab is in the foreground.
    * Select the File > Save or File → Save As option from the menu.
    * Enter a filename and select the file type (*.png, *.gif, *.jpg, *.svg or *.pdf)
    
## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [FreeCAD: Spreadsheet Workbench ⇗][sw]
    * [Manual:Using spreadsheets ⇗](https://wiki.freecadweb.org/Manual:Using_spreadsheets)

[sw]:https://wiki.freecad.org/Spreadsheet_Workbench
