# FreeCAD: ![](ArchWorkbench_files/img_h16/Workbench_Arch_h16.png) [Arch Workbench][t]
[t]:https://wiki.freecad.org/Arch_Workbench

## Contents <a id="contents"></a>
[Highlights](#highlights-) •
[Resources](#resources-)

## Highlights <a id="highlights-"></a><sup>[▴](#contents)</sup>

The ![](ArchWorkbench_files/img_h16/Workbench_Arch_h16.png) Arch Workbench provides a modern [building information modelling](http://en.wikipedia.org/wiki/Building_Information_Modeling) (BIM) workflow to FreeCAD, with support for features like fully parametric architectural entities such as walls, beams, roofs, windows, stairs, pipes, and furniture. It supports industry foundation classes ([IFC](https://wiki.freecad.org/Arch_IFC)) files, and production of 2D floor plans in combination with the ![](ArchWorkbench_files/img_h16/Workbench_TechDraw_h16.png) [TechDraw Workbench](https://wiki.freecad.org/TechDraw_Workbench).

The Arch Workbench imports all tools from the ![](ArchWorkbench_files/img_h16/Workbench_Draft_h16.png) [Draft Workbench](https://wiki.freecad.org/Draft_Workbench), as it uses its 2D objects to build 3D parametric architectural objects. Nevertheless, Arch can also use solid shapes created with other workbenches like ![](ArchWorkbench_files/img_h16/Workbench_Part_h16.png) [Part](https://wiki.freecad.org/Part_Workbench) and ![](ArchWorkbench_files/img_h16/Workbench_PartDesign_h16.png) [PartDesign](https://wiki.freecad.org/PartDesign_Workbench).

The BIM functionality of FreeCAD is now progressively split into this Arch Workbench, which holds basic architectural tools, and the ![](ArchWorkbench_files/img_h16/Workbench_BIM_h16.png) [BIM Workbench](https://wiki.freecad.org/BIM_Workbench), which is available from the ![](ArchWorkbench_files/img_h16/Std_AddonMgr_h16.png) [Addon Manager](https://wiki.freecad.org/Std_AddonMgr). This BIM Workbench adds a new interface layer on top of the Arch tools, with the aim of making the BIM workflow more intuitive and user-friendly. See [FreeCAD BIM migration guide](https://yorik.uncreated.net/blog/2020-010-freecad-bim-guide).

The developers of Draft, Arch, and BIM also collaborate with the greater [OSArch community](https://osarch.org/), with the ultimate goal of improving building design by using entirely free software.

![](ArchWorkbench_files/600px-Screenshot_arch_window.jpg)

## Tools

These are tools for creating architectural objects.

* ![](ArchWorkbench_files/img_h16/Arch_Wall_h16.png) [Wall](https://wiki.freecad.org/Arch_Wall): creates a wall from scratch or using a selected object as a base.
* ![](ArchWorkbench_files/img_h16/Arch_CurtainWall_h16.png) [Curtain Wall](https://wiki.freecad.org/Arch_CurtainWall): creates a curtain wall from scratch or using a selected object as a base.
* ![](ArchWorkbench_files/img_h16/Arch_Structure_h16.png) [Structural element](https://wiki.freecad.org/Arch_Structure): creates a structural element from scratch or using a selected object as a base.
* [Rebar tools](https://wiki.freecad.org/Arch_CompRebarStraight): the Reinforcement Addon augments the Arch Workbench Structures.
    * (various rebar tools not listed)
    * ![](ArchWorkbench_files/img_h16/Arch_Rebar_h16.png) [Rebar](https://wiki.freecad.org/Arch_Rebar): creates a custom reinforcement bar in a selected structural element using a sketch.
* ![](ArchWorkbench_files/img_h16/Arch_Floor_h16.png) [Floor](https://wiki.freecad.org/Arch_Floor): Creates a floor including selected objects
* [Building Part](https://wiki.freecad.org/Arch_BuildingPart): Creates a building part including selected objects
* ![](ArchWorkbench_files/img_h16/Arch_Building_h16.png) [Building](https://wiki.freecad.org/Arch_Building): Creates a building including selected objects
* ![](ArchWorkbench_files/img_h16/Arch_Site_h16.png) [Site](https://wiki.freecad.org/Arch_Site): Creates a site including selected objects
* ![](ArchWorkbench_files/img_h16/Arch_Project_h16.png) [Project](https://wiki.freecad.org/Arch_Project): Creates a project including selected objects
* ![](ArchWorkbench_files/img_h16/Arch_Reference_h16.png) [Reference](https://wiki.freecad.org/Arch_Reference): Links objects from another FreeCAD file into this document
* ![](ArchWorkbench_files/img_h16/Arch_Window_h16.png) [Window](https://wiki.freecad.org/Arch_Window): Creates a window using a selected object as a base
* ![](ArchWorkbench_files/img_h16/Arch_SectionPlane_h16.png) [Section Plane](https://wiki.freecad.org/Arch_SectionPlane): Adds a section plane object to the document
* [Axis tools](https://wiki.freecad.org/Arch_CompAxis): The Axis tool allows you to places a series of axes in the current document.
    * ![](ArchWorkbench_files/img_h16/Arch_Axis_h16.png) [Axis](https://wiki.freecad.org/Arch_Axis): Adds a 1-direction array of axes to the document
    * ![](ArchWorkbench_files/img_h16/Arch_AxisSystem_h16.png) [Axis System](https://wiki.freecad.org/Arch_AxisSystem): Adds an axis system composed of several axes to the document
    * ![](ArchWorkbench_files/img_h16/Arch_Grid_h16.png) [Grid](https://wiki.freecad.org/Arch_Grid): Adds a grid-like object to the document
* ![](ArchWorkbench_files/img_h16/Arch_Roof_h16.png) [Roof](https://wiki.freecad.org/Arch_Roof): Creates a sloped roof from a selected face
* ![](ArchWorkbench_files/img_h16/Arch_Space_h16.png) [Space](https://wiki.freecad.org/Arch_Space): Creates a space object in the document
* ![](ArchWorkbench_files/img_h16/Arch_Stairs_h16.png) [Stairs](https://wiki.freecad.org/Arch_Stairs): Creates a stairs object in the document
* [Panel tools](https://wiki.freecad.org/Arch_CompPanel): Allows you to build all kinds of panel-like elements.
    * ![](ArchWorkbench_files/img_h16/Arch_Panel_h16.png) [Panel](https://wiki.freecad.org/Arch_Panel): Creates a panel object from a selected 2D object
    * ![](ArchWorkbench_files/img_h16/Arch_Panel_Cut_h16.png) [Panel Cut](https://wiki.freecad.org/Arch_Panel_Cut): Creates a 2D cut view from a panel
    * ![](ArchWorkbench_files/img_h16/Arch_Panel_Sheet_h16.png) [Panel Sheet](https://wiki.freecad.org/Arch_Panel_Sheet): Creates a 2D cut sheet including panel cuts or other 2D objects
    * ![](ArchWorkbench_files/img_h16/Arch_Nest_h16.png) [Nest](https://wiki.freecad.org/Arch_Nest): Allow to nest several flat objects inside a container shape
* ![](ArchWorkbench_files/img_h16/Arch_Frame_h16.png) [Frame](https://wiki.freecad.org/Arch_Frame): Creates a frame object from a selected layout
* ![](ArchWorkbench_files/img_h16/Arch_Fence_h16.png) [Fence](https://wiki.freecad.org/Arch_Fence): Creates a fence object from a selected post and path.
* ![](ArchWorkbench_files/img_h16/Arch_Truss_h16.png) [Truss](https://wiki.freecad.org/Arch_Truss): Creates a truss from a selected line of from scratch.
* ![](ArchWorkbench_files/img_h16/Arch_Equipment_h16.png) [Equipment](https://wiki.freecad.org/Arch_Equipment): Creates an equipment or furniture object
* ![](ArchWorkbench_files/img_h16/Arch_Profile_h16.png) [Profile](https://wiki.freecad.org/Arch_Profile): Creates a parametric 2D profile.
* [Pipe tools](https://wiki.freecad.org/Arch_CompPipe)
    * ![](ArchWorkbench_files/img_h16/Arch_Pipe_h16.png) [Pipe](https://wiki.freecad.org/Arch_Pipe): Creates a pipe
    * ![](ArchWorkbench_files/img_h16/Arch_PipeConnector_h16.png) [Pipe Connector](https://wiki.freecad.org/Arch_PipeConnector): Creates a corner or tee connection between 2 or 3 selected pipes
* [Material tools](https://wiki.freecad.org/Arch_CompSetMaterial): The Material tools allows to add materials to the active document.
    * ![](ArchWorkbench_files/img_h16/Arch_SetMaterial_h16.png) [Material](https://wiki.freecad.org/Arch_SetMaterial): Creates a material and attributes it to selected objects, if any
    * ![](ArchWorkbench_files/img_h16/Arch_MultiMaterial_h16.png) [Multi-Material](https://wiki.freecad.org/Arch_MultiMaterial): Creates a multi-material and attributes it to selected objects, if any
* ![](ArchWorkbench_files/img_h16/Arch_Schedule_h16.png) [Schedule](https://wiki.freecad.org/Arch_Schedule): Creates different types of schedules

### Modification tools

These are tools for modifying architectural objects.

* ![](ArchWorkbench_files/img_h16/Arch_CutLine_h16.png) [Cut with line](https://wiki.freecad.org/Arch_CutLine): Cut an object according to a line.
* ![](ArchWorkbench_files/img_h16/Arch_CutPlane_h16.png) [Cut with plane](https://wiki.freecad.org/Arch_CutPlane): Cut an object according to a plane.
* ![](ArchWorkbench_files/img_h16/Arch_Add_h16.png) [Add component](https://wiki.freecad.org/Arch_Add): Adds objects to a component
* ![](ArchWorkbench_files/img_h16/Arch_Remove_h16.png) [Remove component](https://wiki.freecad.org/Arch_Remove): Subtracts or removes objects from a component
* ![](ArchWorkbench_files/img_h16/Arch_Survey_h16.png) [Survey](https://wiki.freecad.org/Arch_Survey): Enters or leaves surveying mode

### Utilities

These are additional tools to help you in specific tasks.

* ![](ArchWorkbench_files/img_h16/Arch_Component_h16.png) [Component](https://wiki.freecad.org/Arch_Component): Creates a non-parametric Arch component
* ![](ArchWorkbench_files/img_h16/Arch_CloneComponent_h16.png) [Clone component](https://wiki.freecad.org/Arch_CloneComponent): Produces Arch Components that are clones of selected Arch objects (not to be confused with [Draft Clone](https://wiki.freecad.org/Draft_Clone))
* ![](ArchWorkbench_files/img_h16/Arch_SplitMesh_h16.png) [Split Mesh](https://wiki.freecad.org/Arch_SplitMesh): Splits a selected mesh into separate components
* ![](ArchWorkbench_files/img_h16/Arch_MeshToShape_h16.png) [Mesh To Shape](https://wiki.freecad.org/Arch_MeshToShape): Converts a mesh into a shape, unifying coplanar faces
* ![](ArchWorkbench_files/img_h16/Arch_SelectNonManifold_h16.png) [Select non-solid meshes](https://wiki.freecad.org/Arch_SelectNonSolidMeshes): Selects all non-solid meshes from the current selection or from the document
* ![](ArchWorkbench_files/img_h16/Arch_RemoveShape_h16.png) [Remove Shape](https://wiki.freecad.org/Arch_RemoveShape): Turns cubic shape-based arch object fully parametric
* ![](ArchWorkbench_files/img_h16/Arch_CloseHoles_h16.png) [Close Holes](https://wiki.freecad.org/Arch_CloseHoles): Closes holes in a selected shape-based object
* ![](ArchWorkbench_files/img_h16/Arch_MergeWalls_h16.png) [Merge Walls](https://wiki.freecad.org/Arch_MergeWalls): Merge two or more walls
* ![](ArchWorkbench_files/img_h16/Arch_Check_h16.png) [Check](https://wiki.freecad.org/Arch_Check): Check if the selected objects are solids and don't contain defects
* ![](ArchWorkbench_files/img_h16/IFC_h16.png) [Ifc Explorer](https://wiki.freecad.org/Arch_IfcExplorer): Browse the contents of an [IFC](https://wiki.freecad.org/Arch_IFC) file
* ![](ArchWorkbench_files/img_h16/Arch_ToggleIfcBrepFlag_h16.png) [Toggle IFC Brep flag](https://wiki.freecad.org/Arch_ToggleIfcBrepFlag): Forces a selected object to be exported as an [IfcFacetedBrep](http://www.buildingsmart-tech.org/ifc/IFC4/final/html/schema/ifcgeometricmodelresource/lexical/ifcfacetedbrep.htm).
* ![](ArchWorkbench_files/img_h16/Arch_3Views_h16.png) [3 Views from mesh](https://wiki.freecad.org/Arch_3Views): Creates top, frontal and side views from a [mesh](https://wiki.freecad.org/Mesh_Workbench).
* ![](ArchWorkbench_files/img_h16/Arch_IfcSpreadsheet_h16.png) [Create IFC spreadsheet...](https://wiki.freecad.org/Arch_IfcSpreadsheet): Creates a spreadsheet to store [IFC](https://wiki.freecad.org/Arch_IFC) properties of an object
* ![](ArchWorkbench_files/img_h16/Arch_ToggleSubs_h16.png) [Toggle Subcomponents](https://wiki.freecad.org/Arch_ToggleSubs): Shows or hides the subcomponents of an Arch object.

### Preferences

* ![](ArchWorkbench_files/img_h16/Preferences-arch_h16.png) [Preferences](https://wiki.freecad.org/Arch_Preferences): preferences for the default appearance of walls, structures, rebars, windows, stairs, panels, pipes, grids and axes.

### File formats

* [IFC](https://wiki.freecad.org/Arch_IFC): industry foundation classes
* [DAE](https://wiki.freecad.org/Arch_DAE): Collada mesh format
* [OBJ](https://wiki.freecad.org/Arch_OBJ): Obj mesh format (export only)
* [JSON](https://wiki.freecad.org/Arch_JSON): JavaScript Object Notation format (export only)
* [3DS](https://wiki.freecad.org/Arch_3DS): 3DS format (import only)
* [SHP](https://wiki.freecad.org/Arch_SHP): GIS Shapefiles (import only)

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [Architecture workflow](http://yorik.uncreated.net/guestblog.php?tag=freecad): An example of how FreeCAD can begin to have its preliminary place in an architecture workflow.
* [Arch tutorial](https://wiki.freecad.org/Arch_tutorial) (v0.14)
* [Quick arch overview on Yorik's blog](http://yorik.uncreated.net/guestblog.php?2012=180) (v0.13)
* [Video presentation of the Arch workbench](https://www.youtube.com/watch?v=lTDOeHapv_E) (2016)
* [Arch panel tutorial](https://wiki.freecad.org/Arch_panel_tutorial) (v0.15)
* [BIM modeling chapter from the FreeCAD manual](https://wiki.freecad.org/Manual:BIM_modeling)
* [Import from STL or OBJ](https://wiki.freecad.org/Import_from_STL_or_OBJ)
* [Export to STL or OBJ](https://wiki.freecad.org/Export_to_STL_or_OBJ)

