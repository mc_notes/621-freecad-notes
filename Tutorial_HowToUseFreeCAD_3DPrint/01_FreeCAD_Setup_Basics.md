# Maker Tales: [How To Use FreeCAD][t]<br>Setup & Basics (Part 1/5)
[t]:https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN

[<< Return to 621-freecad-notes/README.md](README.md)

_How to learn FreeCAD with the RealThunder branch for 3D printing. The video series is intended both for a complete beginner or for someone switching from another application._

The [Maker Tales "FreeCAD Setup & Basics" video covers](https://www.youtube.com/watch?v=p_ZEry2wTfg) quality-of-life pre-settings and the basics (user interface, sketching, constraints, extruding and holes).

> _NOTE: The ["How To Use FreeCAD For Beginners … For 3D Printing" series](https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN) is based on the _Stable_ version of the FreeCAD RealThunder branch. The notes below have been checked on macOS 11.6 (Big Sur) with the FreeCAD pre-release 0.20.26498 and FreeCAD LinkStage3 (RealThunder) branch version: 2021.1015.24301. See ["Resources" section](#resources-) below._

## Contents <a id="contents"></a>
[Setup Preferences](#setup-preferences-) •
[Workbenches](#workbenches-) •
[First Document](#first-document-) •
[First Sketch](#first-sketch-) •
[Time Stamps](#time-stamps-) •
[Resources](#resources-)

## Setup Preferences <a id="setup-preferences-"></a><sup>[▴](#contents)</sup>

Many of the preferences changes were done for the 4K monitor and video recording.

**General**

* `Preferences…` >
    * `General` > `Units` > `Units System:` Imperial decimal (in/lb) | _Standard (mm/kg/s/degree) [default]_  
    * `General` > `General` > `Main Window`
        * `Style sheet:` _No style sheet [default]_
        * `Size of toolbar icons:` Small (16 px) | _Medium (24px)_ | Large (32px) [default] | Extra large [4K display]
        * `Default font size` [realthunder only]: 0 [default]
        * `Tree view icon size` [realthunder only]: 0 [default], 25 [tutorial]
        * `Tree view font size` [realthunder only]: 0 [default], 15 [tutorial]
        * `Tree view item spacing` [realthunder only]: 0 [default]
    * `General` > `General` > `Start up`
        * `Auto load module after start up:` `OpenSCAD` | _`Part Design` [tutorial]_ | `Sketcher` | `Start` [default]
    * `Display` > `3D View` > `Rendering` >
        * `Anti-Aliasing:` `None` [default] | `Line` Smoothing | `MSAA 4x` | `MSAA 8x` [tutorial]
        * `Marker size:` 9px [default] | 15px [4k screen]
        * `Pick radius (px):` 5.0 [default] | 10.0 [4k screen]
    * `Display` > `Navigation` > `Navigation` >
       * `3D Navigation` > _`Blender` [tutorial]_ | `CAD` [default] | `Gesture` (good for trackpad/pen/MacBook)
           * Click the `Mouse…` button to see Selection, Panning, Rotation and Zooming actions for the current `3D Navigation` selection.
       * `Orbit Style` > `Trackball` (default) | `Turntable`
       * `Rotation Mode` > `Drag at cursor` (default) | _`Object center`_ | `Window center`
       * `Disable touchscreen tilt gesture` [✓] checked (default) | _unchecked_
   * `Display` > `Colors`: Background colors

**Part Design Workbench Preferences**

* `Preferences…` > `Part design` >
    * `General`
        * Automatically check model after boolean operation: unchecked [default]
        * Automatically refine model shape of features in Part workbench [realthunder]: unchecked [default]
        * Automatically refine model after boolean operation [v20]: unchecked [default]
        * Automatically refine model shape of features in PartDesign workbench [realthunder]: unchecked [default]
        * Automatically refine model shape after sketch-based operation [v20]: unchecked [default]
    * `Shape view`
        * `Maximum angular deflection` 28.50° [default] | 6.00° [tutorial]
    * `Shape appearance`
        * `Line width` 2px [default] | 6px [4K display,  tutorial]
        * `Vertex size` 2px [default] | 9px [4K display, tutorial]

* `Preferences…` > `Sketcher` >
    * `General` > `Show grid` disabled [default] | _enabled_
    * `Display`
        * `Font size` 17px [default] | 25px | 30px | 40px [tutorial]
        * `View scale ratio` 1.00 [default] | 2.00 [tutorial]
    * `Colors`

## Workbenches <a id="workbenches-"></a><sup>[▴](#contents)</sup>

A [FreeCAD Workbench ⇗](https://wiki.freecadweb.org/Workbenches) provides a tool set for a particular task. [External workbenches ⇗](https://wiki.freecadweb.org/External_workbenches) are available from the community.

Workbenches used or mentioned in this tutorial series:

* [**Start Center**](https://wiki.freecadweb.org/Start_Workbench) provides a first launch screen with links for documentation, example, and recent projects.  The start center contents can be replaced with a user customized HTML page. (Start view is empty in v20 daily build.)
* [**Part Design Workbench**](https://wiki.freecadweb.org/PartDesign_Workbench)
* [**Sketcher Workbench**](https://wiki.freecadweb.org/Sketcher_Workbench)
    * Creates 2D geometries for use in workbenches e.g. PartDesign Workbench, Arch Workbench
    * Once sketches are used to generate a solid feature, they are automatically hidden.
    * Constraints are only visible in Sketch edit mode.
    * Not intended for 2D blueprints. Use [Draft Workbench](https://wiki.freecadweb.org/Draft_Workbench) instead.
    * [Draft Draft2Sketch](https://wiki.freecadweb.org/Draft_Draft2Sketch) can convert between Draft object and Sketches.

## First Document <a id="first-document-"></a><sup>[▴](#contents)</sup>

* File > New <kbd>^</kbd> | <kbd>⌘</kbd> <kbd>N</kbd>
* File > Save <kbd>^</kbd> | <kbd>⌘</kbd> <kbd>S</kbd>

**Toolbars**

Part Design Extra

**Panels**

`View` > `Panels` > `Combo View` Model Tree View & Task view

1. Create a `Std Part` **Part** container
2. Add a `PartDesign Body` **3D Body** to the Part.
3. Select the Body Origin001 show|hide

_Note: Do not confuse the `PartDesign Body` with the `Std Part`._


<table style="width:100%; font-family: monospace;">
<tr>
    <th>[Std Part](https://wiki.freecadweb.org/Std_Part)</th>
    <th>[PartDesign Body](https://wiki.freecadweb.org/PartDesign_Body)</th>
</tr>
<tr>
    <td><strong>Part</strong></td>
    <td><strong>Body</strong></td>
</tr>
<tr>
    <td><img src="01_FreeCAD_Setup_Basics_files/Std_Part.svg"/></td>
    <td><img src="01_FreeCAD_Setup_Basics_files/PartDesign_Body.svg"/></td>
</tr>
<tr>
    <td>Base system grouping container</td>
    <td>PartDesign Workbench object</td>
</tr>
<tr>
    <td>Intended to create assemblies.<br>Can arrange different objects in space.</td>
    <td>Models a _single_ contiguous solid.</td>
</tr>
<tr>
    <td>Can contain multiple 3d Bodies and other Std Parts.</td>
    <td>Is a singular 3D model body.</td>
</tr>
</table>

> Difference Note: view selection (e.g. top, right, back, …) does not show as a menubar.  Use View menu or View menubar dropdown.  Or use the orientation cube.

**Standard Views**

![](01_FreeCAD_Setup_Basics_files/ViewMenu.png)

![](01_FreeCAD_Setup_Basics_files/ViewToolbarDropDown.png)

* <img src="01_FreeCAD_Setup_Basics_files/Std_ViewFitAll.svg" height="18"/> [**Std ViewFitAll**](https://wiki.freecadweb.org/Std_ViewFitAll) <kbd>V</kbd> <kbd>F</kbd> View Fit

**Modeling Approaches**

* **Direct Modeling** - every number is entered and modified manually.
* **Parametric Modeling** - define variables e.g. wall_thickness, bolt_diameter

## First Sketch <a id="first-sketch-"></a><sup>[▴](#contents)</sup>

<table style="width:100%; font-family: monospace;">
<tr>
    <th>[Sketcher SketchObject](01_FreeCAD_Setup_Basics_files/Workbench_Sketcher.svg)</th>
</tr>
<tr>
    <td><strong>SketchObject</strong></td>
</tr>
<tr>
    <td><img src="01_FreeCAD_Setup_Basics_files/Workbench_Sketcher.svg"/></td>
</tr>
<tr>
    <td>Base element to create 2D objects with the [Sketcher Workbench](https://wiki.freecadweb.org/Sketcher_Workbench). </td>
</tr>
<tr>
    <td>PartDesign New Sketch <img src="01_FreeCAD_Setup_Basics_files/PartDesign_NewSketch.svg" height="24"/></td>
</tr>
</table>

1. Select and hide `Part`/`Body`/`Origin001`, if visible
    * Press <kbd>space</kbd>
    * Use `View` > `Visibility` menu
    * Click visibility <img src="01_FreeCAD_Setup_Basics_files/Std_ToggleVisibility.svg" height="16"/> icon, if available
2. Select `Part`/`Body`. Activate, if inactive (bold font indicates active)
    * Model Tree View Body `Context Menu` > `Toggle active body`
    * Model Tree View Body <kbd>double-left-click</kbd>
3. Create sketch (via toolbar button or Sketch menu)
    * Reference the XY_Plane
        * [RealThunder]`Selecting…` left-click to select `XY_Plane001` in the 3D view.
        * [v20] Select `XY_Plane001 (Base Plane)` from list or left-click to select `XY_Plane001` in the 3D view.
    * Click `OK`

> Note: Sketcher Workbench menu and toolbar now appear. Move toolbar as needed..

**[Sketch Workbench Tools](https://wiki.freecadweb.org/Sketcher_Workbench#Tools)** (short list)

* <img src="01_FreeCAD_Setup_Basics_files/Sketcher_ViewSketch.svg" height="20"/> **View Sketch** Sets camera perpendicular to sketch.
* **2D Geometry**
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_CreateCircle.svg" height="20"/> **Create Circle** (<kbd>R</kbd>) Draws a circle by picking two points: the center, and a point along the radius. Right click to cancel tool
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_CreateRectangle.svg" height="20"/> **Create Rectangle** (<kbd>R</kbd>) Draws a rectangle from two opposing points.
        * `Sketch` > `Sketcher geometries` > `Create rectangle`
        * defined by two left clicks
        * right click to cancel tool
* **Geometric Constraint**
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_ConstrainCoincident.svg" height="20"/> **Create Coincident Constraint** (<kbd>C</kbd>) Makes the two points coincident (as-one-point).
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_ConstrainHorizontal.svg" height="20"/> **Contrain Horizontal** (<kbd>H</kbd>) Forces a selected line or lines to be parallel to the horizontal sketch axis.
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_ConstrainVertical.svg" height="20"/> **Constrain Vertical** (<kbd>V</kbd>) Forces a selected line or lines to be parallel to the vertical sketch axis.
* **Dimensional Constraint**
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_ConstrainDistanceX.svg" height="20"/> **Contrain Horizontal Distance** (<kbd>shift</kbd> <kbd>H</kbd>) Fixes the horizontal distance between 2 points or line ends. If only one point is selected, the distance is set to the sketch origin.
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_ConstrainDistanceY.svg" height="20"/> **Constrain Vertical Distance** (<kbd>shift</kbd> <kbd>V</kbd>) Fixes the vertical distance between 2 points or line ends. If only one point is selected, the distance is set to the sketch origin.
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_ConstrainRadius.svg" height="20"/> **Constrain Radius** (<kbd>shift</kbd> <kbd>V</kbd>) Fixes the vertical distance between 2 points or line ends. If only one point is selected, the distance is set to the sketch origin.
    * <img src="01_FreeCAD_Setup_Basics_files/Sketcher_ConstrainDiameter.svg" height="20"/> **Constrain Diameter** (<kbd>shift</kbd> <kbd>V</kbd>) Fixes the vertical distance between 2 points or line ends. If only one point is selected, the distance is set to the sketch origin.
* **View Sketch*** **Pan** with arrow keys.
* **Zoom** <kbd>cmd</kbd> <kbd>+</kbd>, <kbd>cmd</kbd> <kbd>-</kbd>

**[PartDesign Workbench Tools](https://wiki.freecadweb.org/PartDesign_Workbench)** (short list)

* **2D to 3D Conversion**
    * <img src="01_FreeCAD_Setup_Basics_files/PartDesign_Pad.svg" height="20"/> **Pad** Extrudes a sketch or a face of a solid along a straight path.
    * <img src="01_FreeCAD_Setup_Basics_files/PartDesign_Pocket.svg" height="20"/> **Pocket** Cuts solids by extruding a sketch or a face of a solid along a straight path.
        * Type: Dimension | Through all | To first | Up to (selected) face | Two dimensions
    * <img src="01_FreeCAD_Setup_Basics_files/PartDesign_Hole.svg" height="20"/> **Hole** Creates one or more holes from a selected sketch's circles and arcs. Requires closed contours. Many parameters can be set such as threading and size, fit, hole type (countersink, counterbore, straight) and more.

Constraints can be selected and activated/deactivated.

Constraint types: coincident, vertical, horizontal, etc. Degrees of freedom increases as constraints decrease.

* Add a coincident constraint by click selecting two points and clicking the coincident constraint button.
* Add a dimensional constraint. Select edge. Click horizontal constraint button.

**Add Constraints**

1. Make lower left corner of box coincident with the origin.
    * Click origin. Click lower left box corner. Click create constrain coincident.
2. Add a 50 mm horizontal dimensional constraint to the rectangle bottom.
3. Click _under constrained_ 1 degree of freedom (aka 1 DoF) to select and see the unconstrained points.
4. Click right side corner points and add a 50 mm vertical dimensional contraint.
5. Notice that the sketch has turned completely green to indicate the sketch is now fully constrained.
6. Click `Close`. The edit Sketch toolbar will also disappear.

**Extrude/Pad 2D to 3D**

1. Select `Sketch` in the tree view.
2. Pad (extrude) the `Sketch`.
3. Set the pad/extrusion length to 50 mm.
4. Click OK. [RealThunder: 3D View updated after rotate|pan.]

The Sketch is hidden as it is relocated to be subordinate to the Pad object in the document tree. The Sketch remains editable within the Pad object.

**Create Hole**

1. Select top surface.
2. Click create sketch. [v20: Closed & reopened to see top surface in sketch]
3. Add a horizontal constraint and a vertical constraint to lock the circle center to the surface origin.
4. Add a radius or diameter constraint to the circle.
5. Create a pocket with the circle sketch.
    * Set the pocket type to `Up to face`.
    * Select the opposing face

## Time Stamps <a id="time-stamps-"></a><sup>[▴](#contents)</sup>

* 00:00 - Intro.
* 00:30 - What This Video Is All About.
* 02:00 - Installing Realthunders Branch FreeCAD & Useful Links.
* 03:11 - First Time Running & Preference Settings To Change.
* 08:38 - Thangs Sponsor Segment.
* 09:13 - Restarting freeCAD, Understanding Workbenches.
* 10:47 - Understanding FreeCAD & Files.
* 12:05 - FreeCAD UI, Panels & Toolbar.
* 13:28 - Creating A Part.
* 15:05 - Navigation.
* 17:25 - Understanding CAD Style Modeling.
* 20:38 - Creating A Sketch.
* 22:00 - Quick Overview Of Sketcher & Navigation.
* 23:06 - Quick Overview Of Sketcher, Constrains & Dimensions.
* 28:43 - Extruding / Padding The Sketch.
* 29:38 - Understanding Operations & The Tree View.
* 30:06 - Direct Modeling Number Changing.
* 31:01 - Adding A Hole Creating Another Sketch.
* 32:46 - Pocketing The Sketch.
* 34:00 - Closing Notes.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [2017.09.09 TutorialField ~ CAD Style Navigation ⇗](https://www.youtube.com/watch?v=DpTB_R-_gD0)
* [2021.08.06 Maker Tales ~ How To Use FreeCAD The Realthunder Branch For Beginners | Setup & Basics | For 3D Printing | P. 1 ⇗](https://www.youtube.com/watch?v=p_ZEry2wTfg&list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN)
* [2017.08.12 Maker Tales ~ CAD Style Navigation for FreeCAD ⇗](https://www.youtube.com/watch?v=Ir9PWoeqLZE)
* [FreeCAD: weekly builds ⇗](https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds)
    * FreeCAD_weekly-builds-26498-OSX-x86_64-conda.dmg
* [GitHub/realthunder: FreeCAD releases ⇗](https://github.com/realthunder/FreeCAD_assembly3/releases)
RealThunder
    * Stable: Mostly fixes. In sync with but yet merged into upstream FreeCAD
        * FreeCAD-asm3-Stable-OSX-Conda-Py3-Qt5-yyyyMMdd-x86_64.dmg _(before mac OS X Big Sur)_
        * FreeCAD-asm3-Stable-BigSur-OSX-Py3-Qt5-yyyyMMdd-x86_64.dmg _(macOS BigSur)_
    * Daily: Used for testing fixes. Updates `Topo Naming` internals to provide more efficient performance. Bypasses Coin3D with with experimental rendering: `Preference -> Display -> Render cache -> Experimental`.
* [GitLab: 621-FreeCAD-Notes ⇗](https://gitlab.com/mc_qref/621-freecad-notes)
* [Thangs: MarkerTales ⇗](https://thangs.com/MakerTales) / [README.md ⇗](https://gitlab.com/mc_qref/621-freecad-notes/-/blob/main/README.md)
    * [Blog: What file types are supported on Thangs.com? ⇗](https://blog.thangs.com/what-file-types-are-supported-on-thangs-com-96f256238a1f) … currently not `.scad` or FreeCAD uploads. Can
    * [Learn about Thangs](https://blog.thangs.com/learn/home)
    * [Thangs FAQ](https://blog.thangs.com/faq/home)
    * Thangs search engine includes results from:
        * [Makerbot Thingiverse](https://www.thingiverse.com)
        * [PrusaPrinters](https://www.prusaprinters.org)
        * [YouMagine](https://www.youmagine.com/)
