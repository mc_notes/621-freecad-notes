# Maker Tales: [How To Use FreeCAD][t]<br>_Sketcher Workbench_<br>Lines, Tools & References (part 2/5)
[t]:https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN

[<< Return to 621-freecad-notes/README.md](README.md)

_The [Maker Tales "Sketcher Workbench | Lines, Tools & References" video covers](https://www.youtube.com/watch?v=LIDak5PHO5I) covers the FreeCAD [Sketcher Workbench](https://wiki.freecadweb.org/Sketcher_Workbench) lines, referencing and tools used for technical designs._

> _NOTE: The ["How To Use FreeCAD For Beginners … For 3D Printing" series](https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN) is based on the _Stable_ version of the FreeCAD RealThunder branch. The notes below have been checked on macOS 11.6 (Big Sur) with the FreeCAD pre-release 0.20.26498. See ["Resources" section](#resources-) below._

## Contents <a id="contents"></a>
[Sketcher Geometries](#sketcher-geometries-) •
[Sketcher Tools](#sketcher-tools-) •
[Sketcher External Geometry](#sketcher-external-geometry-) •
[Construction Geometry](#constructionr-geometry-) •
[Time Stamps](#time-stamps-) •
[Resources](#resources-)

Starting point: New Document. Create Part. Create Body. Create Sketch attached to Body XY_Plane.

## Sketcher Geometries <a id="sketcher-geometries-"></a><sup>[▴](#contents)</sup>

The v20 and RealThunder [Sketcher Geometries](https://wiki.freecadweb.org/Sketcher_Workbench#Sketcher_geometries) toolbar have a small number of differences. The Sketcher Geometry Toolbar (v20) adds `Create centered rectangle`, `Create rounded rectangle` and `Split Edge`. The `Split Edge` tool splits an edge into two while preserving constraints.

_Sketcher Geometries Toolbar (v20)_
![](02_FreeCAD_Sketcher_Lines_Tools_Refs_files/SketcherGeometryToolbar_v20.png)

* **Activate Tool** - <kbd>left-click</kbd> or keyboard shortcut _to activate_ a tool
* **Deactivate Tool** - <kbd>right-click</kbd> _to deactivate_ a tool
* **Select** - <kbd>left-click</kbd> individual marker points _to select_ one or more marker points
* **Select Many** - <kbd>left-click-drag</kbd> to create a selection box _to select many_ marker points
* **Deselect** - <kbd>right-click</kbd> background _to deselect all_ marker points
* **Delete** - Select then <kbd>backspace</kbd> _to delete_ marker points
* **Mode Toggle** Press the <kbd>M</kbd> key while drawing a Polyline to toggle between the different polyline modes.

**[Polyline](https://wiki.freecad.org/Sketcher_CreatePolyline)**

| type | previous segment
|:----:|:------|
| line | coincidence           |
| line | perpendicular         |
| line | tangential            |
| arc  | tangential            |
| arc  | perpendicular (left)  |
| arc  | perpendicular (right) |

* <kbd>ctrl</kbd> snap arc to 45° increments (macOS)
* 

Documentation: [Sketcher Geometries ⇗](https://wiki.freecadweb.org/Sketcher_Workbench#Sketcher_geometries)

## Sketcher Tools <a id="sketcher-tools-"></a><sup>[▴](#contents)</sup>

The v20 [Sketcher Tools](https://wiki.freecadweb.org/Sketcher_Workbench#Sketcher_tools) toolbar adds [`Remove axes alignment`](https://wiki.freecadweb.org/Sketcher_RemoveAxesAlignment) which is not present in the RealThunder branch used.  These tools provide multi-geometry actions, special selections and clone/copy/move.

_Sketcher Tools Toolbar_

![](02_FreeCAD_Sketcher_Lines_Tools_Refs_files/SketcherToolsToolbar_v20.png)

* **Last selection** - typically used as a reference for the tool action.
* **Clone** - New constraints are linked to any _existing_ source constraints. If a linked source constraint is changed, then the clone constraint is also changed.
* **Copy** - A new element is created with out linking to any source constraints.
* **Last Use Shortcuts** - Keyboard shortcuts uses to the _last toolbar action_ used associated with particular keyboard character. e.g. <kbd>C</kbd> would activate the last toolbar use of `Clone` or `Copy`.
* **CarbonCopy** - Copy all the geometry and constraints from another sketch (in the same part) into the active sketch.
    * Dimensional constraints which exist before the copy function remain linked to the original sketch's dimensional constraints through expressions.

## Sketcher External Geometry <a id="sketcher-external-geometry-"></a><sup>[▴](#contents)</sup>

> Note: FreeCAD (v20) only provides the Sketcher [`External geometry`](https://wiki.freecadweb.org/Sketcher_External) tool.
> 
> `External geometry` behaves like construction geometry, see [`Sketcher_ToggleConstruction`](https://wiki.freecadweb.org/Sketcher_ToggleConstruction) and [`Sketcher_External`](https://wiki.freecadweb.org/Sketcher_External)
`Defining geometry` is a new feature in the `RealThunder` branch that imports the external geometry to be exposed outside the local sketch just like a regular geometry.
> 
> _Caution: externally linked to generated (solid) geometry can lead to unexpected results due to Topological Naming Problem._

* External geometry - Create a edge linked to an external geometry [(Sketcher_External)](https://wiki.freecadweb.org/Sketcher_External)
    * Use to apply a constraint between sketch geometry and something outside the sketch.
    * Works by inserting a linked construction geometry into the sketch.
* Defining geometry - Create a defining edge connected to an external geometry
* Detach geometry
* Attach geometry
* Toggle freeze
* Sync geometry
* Fix reference

_Defining geometry vs. Reference geometry_

* External (Reference) geometry - Locally visible from an external source
    * only count as construction (reference) lines 
    * cannot be directly used to close a local Sketcher shape
    * cannot be directly converted to local Sketcher geometries
    * _can be used as a reference for constrains within the local sketch_
    * Only edges and vertices from objects from same coordinate system are allowed
    * No circular dependencies are allowed
* Defining geometry - Linked from the external geometry
    * Dimension constraints need to be changed at the external source.

_Default Colors_

* blue - standard non-linked construction geometry
* magenta - externally linked construction reference edges

_Usage_

1. **Activate (local) Sketch** - Create a new sketch, or open an existing sketch.
2. **Activate `External geometry` Tool** - Click Sketcher `External geometry` button
3. **Select Vertex|Edge** Select a visible yet external edge or vertex to link into the local sketch.
4. **Deactivate `External geometry` Tool** Press <kbd>esc</kbd>, or select another tool to stop importing geometry into the sketch.

A new additional sketch can reference a face plane of a visible 3D element (e.g. Pad).

## Construction Geometry <a id="constructionr-geometry-"></a><sup>[▴](#contents)</sup>

The Sketcher [`ToggleConstruction`](https://wiki.freecadweb.org/Sketcher_ToggleConstruction) tool toggles sketch geometry from/to construction mode.  

* Toggles selected Sketch drawing geometry elements 
* Toggles Sketch Geometries toolbar when no drawing elements are selected.

_Construction Geometry Features_

* ignored when applying 3D sketch operations
* visible in Sketcher edit mode
* hidden in 3D view
* can be used as rotation axis by the `PartDesign Revolution` feature
* dimensional constraints can be added to construction lines

Default color: blue

## Time Stamps <a id="time-stamps-"></a><sup>[▴](#contents)</sup>

* 00:00 - Intro.
* 00:17 - Quick Recap &  What This Video Is All About.
* 00:49 - File Setup.
* 01:19 - The Plan.
* 02:07 - How To Use The Tools.
* 02:36 - Point / Marker Tool.
* 02:58 - Overview Of Selection.
* 03:19 - Line Tool.
* 03:45 - Arc Tools.
* 04:22 - Circle Tools.
* 04:48 - Conic & B-Spline.
* 05:28 - Poly Line Tool.
* 06:55 - Rectangle & Shape Tools.
* 07:51 - Thangs Sponsorship Segment.
* 08:35 - Fillet Operations.
* 09:31 - Trim Operation.
* 09:56 - Extending Operation.
* 10:23 - Letting you know what we will do next.
* 11:21 - Simmitry Copy.
* 12:09 - Move, Copy & Clone.
* 15:53 - SETUP - Copying A Sketch To Another Sketch.
* 18:25 - Copying A Sketch To Another Sketch.
* 18:50 - Out Of Scope Coping.
* 20:10 - Geometry Referencing ( Sketches ).
* 25:06 - Geometry Referencing ( Geometry ).
* 28:13 - Construction Lines.
* 29:46 - Closing Notes.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [2021.08.13 How To Use FreeCAD The Realthunder Branch | Sketcher Workbench | Lines, Tools & References | P. 2 ⇗](https://www.youtube.com/watch?v=LIDak5PHO5I)
* [FreeCAD: weekly builds ⇗](https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds)
* [GitHub/realthunder: FreeCAD releases ⇗](https://github.com/realthunder/FreeCAD_assembly3/releases)
* [GitLab: 621-FreeCAD-Notes ⇗](https://gitlab.com/mc_qref/621-freecad-notes) / [README.md ⇗](https://gitlab.com/mc_qref/621-freecad-notes/-/blob/main/README.md)
