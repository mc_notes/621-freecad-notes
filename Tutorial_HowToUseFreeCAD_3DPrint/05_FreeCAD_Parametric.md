# Maker Tales: [How To Use FreeCAD][t]<br>How To Do Parametric Modeling (part 5/5)
[t]:https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN

[<< Return to 621-freecad-notes/README.md](README.md)

The ["How To Do Parametric Modeling In FreeCAD" video](https://www.youtube.com/watch?v=3Wd33_dIEA8) create a first parametric model in FreeCAD using a Parametric Workflow.

Spreadsheet ( Copy & Paste):

> _NOTE: The ["How To Use FreeCAD For Beginners … For 3D Printing" series](https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN) is based on the _Stable_ version of the FreeCAD RealThunder branch. The notes below have been checked on macOS 11.6 (Big Sur) with the FreeCAD pre-release 0.20.26498 and FreeCAD LinkStage3 (RealThunder) branch version: 2021.1015.24301. See ["Resources" section](#resources-) below._

## Contents <a id="contents"></a>
[Spreadsheet Workbench](#spreadsheet-workbench-) •
[Time Stamps](#time-stamps-) •
[Resources](#resources-)

## Spreadsheet Workbench <a id="spreadsheet-workbench-"></a><sup>[▴](#contents)</sup>

**Expression Editor**

```
Wall_Thickness 1.5
Height_Width_Depth 70
Box_Base_Thickness 3
Lid_Thickness 6
Lid_Hole_Percent_Ratio 0.3
Print_Offset 0.4
Global_Chamfer 1
```

**[01:30] Spreadsheet Workbench**

* Switch to Spreadsheet Workbench
* Create a new spreadsheet. Optionally, "Spreadsheet" can be renamed (e.g. "dimensions", "parameters", etc).
* Associate names with numbers
    * Approach: selected number cell > `Properties…` > `Alias`
    * Approach: selected number cell > `Spreadsheet_SetAlias` tag icon (cmd-shift-A) also opens the cell Properties Alias tab
        * paste copied name as alias, or 
        * type in alias name

Move cell selection by cut and paste. Can not move by drag and drop actions.

## Time Stamps <a id="time-stamps-"></a><sup>[▴](#contents)</sup>

* 0:00 - The Parametric Result.
* 01:05 - The Spreadsheet Workbench.
* 03:31 - Quick Note About Parametric Designs.
* 04:00 - First Parametric Dimension.
* 06:54 - Making The Rest Of The Box Parametric.
* 09:54 - Thangs Sponsorship Segment.
* 10:28 - Making The Lid Parametric.
* 14:15 - Tiled Windows Layout.
* 14:51 - Closing Notes.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [2021.09.11 How To Do Parametric Modeling In FreeCAD | The Realthunder Branch | P. 5 ⇗](https://www.youtube.com/watch?v=3Wd33_dIEA8)
* [FreeCAD: weekly builds ⇗](https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds)
* [GitHub/realthunder: FreeCAD releases ⇗](https://github.com/realthunder/FreeCAD_assembly3/releases)
* [GitLab: 621-FreeCAD-Notes ⇗](https://gitlab.com/mc_qref/621-freecad-notes) / [README.md ⇗](https://gitlab.com/mc_qref/621-freecad-notes/-/blob/main/README.md)
