# [FreeCAD 3D View Navigation][t]
[t]:https://wiki.freecadweb.org/Manual:Navigating_in_the_3D_view

[<< Return to 621-freecad-notes/README.md](../README.md)

## Contents <a id="contents"></a>
[Mouse Modes](#mouse-modes-) •
[Mouse Navigation](#mouse-navigation-) •
[Keyboard Navigation](#keyboard-navigation-) •
[Navigation Cube](#navigation-cube-) •
[Object Selection](#object-selection-) •
[Tool Selection](#tool-selection-) •
[Coordinate System](#coordinate-system-) •
[Resources](#resources-)

* Select 

## Mouse Modes <a id="mouse-modes-"></a><sup>[▴](#contents)</sup>

Navigation modes are accessed from the application _Preferences_, or from the _3D view context menu_ (right-click view background). Partial list:

| Navigation Mode  | Main Branch  | RealThunder | |
|:-----------------|:------------:|:-----------:|-----------------------------------------------------------|
| [Blender][]      | ✓ | ✓ | Open source, surface-centric 3D software
| CAD              | ✓ | ✓ | **(default)** 
| Gesture          | ✓ | ✓ | Tailored for touchscreen and pen. Recommended for Apple trackpad.
| MayaGesture      | ✓ | ✓ | Uses <kbd>alt</kbd> and 3-button mouse.
| [OpenCascade][]  | ✓ | ✓ | Free/Paid Windows application suite
| [OpenInventor][] | ✓ | ✓ | Based on OSS SGI software. (Not based on Autodesk Inventor)
| [OpenSCAD][]     | ✓ |   | Open source 3D editor and compiler. Similar to Gesture mouse.
| [Revit][]        | ✓ | ✓ | Autodesk BIM `$2,545/year`
| [TinkerCAD][]    | ✓ |   | Free-of-charge, web-based Autodesk software 
| Touchpad         | ✓ | ✓ | Uses modifier keys with 2-button touchpad.

[Blender]:https://www.blender.org/
[OpenCascade]:https://www.opencascade.com/
[OpenInventor]:https://en.wikipedia.org/wiki/Open_Inventor
[OpenSCAD]:https://openscad.org/
[Revit]:https://www.autodesk.com/products/revit/overview/
[TinkerCAD]:https://www.tinkercad.com/

## Mouse Navigation <a id="mouse-navigation-"></a><sup>[▴](#contents)</sup>

Mouse navigation is shown for a two button mouse with scroll wheel and for an Apple Magic Mouse. Gesture mode looks like a good choice for the Magic Mouse and Magic Trackpad. System dependent modifier keys are shown separated by a vertical line: <kbd>linux/windows</kbd>|<kbd>macos</kbd>.

<table style="width:100%; font-family: monospace;">
<tr>
    <th>Mode</th>
    <th style="text-align: center;">Pan<br>![Cursor_Pan](FreeCAD_3D_Navigation_files/png/Cursor_Pan.png)</th>
    <th style="text-align: center;">Rotate<br>![Cursor_Rotate](FreeCAD_3D_Navigation_files/png/Cursor_Rotate.png)</th>
    <th style="text-align: center;">Zoom<br>![Cursor_Zoom](FreeCAD_3D_Navigation_files/png/Cursor_Zoom.png)</th>
    <th style="text-align: center;">Select<br>![Cursor_Hand_Select](FreeCAD_3D_Navigation_files/png/Cursor_Hand_Select_16px.png)</th>
</tr>
<tr>
    <td>Gesture</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Right_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Left_Drag.svg"/><br>(sketcher)<br><kbd>alt</kbd>|<kbd>opt</kbd><br><br>- (tilt) -<br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Both_Drag.svg"/><br>- (focal) -<br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Click.svg"/><br>or cursor+<kbd>H</kbd></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Left_Click.svg"/></td>
</tr>
<tr>
    <td>Gesture<br>(Magic Mouse)</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Right_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Left_Drag.svg"/><br>- (focal) -<br>cursor+<kbd>H</kbd></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Left_Click.svg"/></td>
</tr>
<tr>
    <td>Gesture<br>(Magic Trackpad)</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Touch_DragTwo.svg"/><br><img src="FreeCAD_3D_Navigation_files/svg/Touch_DragWithDelay.svg"/><br><img src="FreeCAD_3D_Navigation_files/svg/Touch_DragWithForceClick.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Touch_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Touch_SwipeTwoVertical.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Touch_Click.svg"/></td>
</tr>
<tr>
    <td>OpenSCAD (v20)</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Right_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Left_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Roll.svg"/><br><kbd>shift</kbd><br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Right_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Left_Click.svg"/></td>
</tr>
<tr>
    <td>OpenSCAD<br>(v20, Magic Mouse)</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Right_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Left_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Left_Click.svg"/></td>
</tr>
<tr>
    <td>CAD [default]</td>
    <td style="text-align: center;"><kbd>ctrl</kbd>|<kbd>cmd</kbd><br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Right_Click.svg"/><br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Drag.svg"/></td>
    <td style="text-align: center;"><kbd>shift</kbd><br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Right_Click.svg"/><br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle1_Left2_Drag.svg"/><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle1_Right2_Drag.svg"/></td>
    <td style="text-align: center;"><kbd>ctrl-shift</kbd>|<br><kbd>cmd-shift</kbd><br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Right_Click.svg"/><br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Left_Click.svg"/> </td>
</tr>
<tr>
    <td>CAD<br>(Magic Mouse)</td>
    <td style="text-align: center;"><kbd>cmd</kbd><br><img src="FreeCAD_3D_Navigation_files/svg/Magic_Right_Click.svg"/></td>
    <td style="text-align: center;"><kbd>shift</kbd><br><img src="FreeCAD_3D_Navigation_files/svg/Magic_Right_Click.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Left_Click.svg"/></td>
</tr>
<tr>
    <td>Blender</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Both_Drag.svg"/><br><kbd>shift</kbd><br><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Left_Click.svg"/></td>
</tr>
<tr>
    <td>Blender<br>(Magic Mouse)</td>
    <td style="text-align: center;">-</td>
    <td style="text-align: center;">-</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Left_Click.svg"/></td>
</tr>
<tr>
    <td>TinkerCAD (v20)</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Right_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Mouse_Left_Click.svg"/></td>
</tr>
<tr>
    <td>TinkerCAD<br>(v20, Magic Mouse)</td>
    <td style="text-align: center;">-</td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Right_Drag.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Middle_Roll.svg"/></td>
    <td style="text-align: center;"><img src="FreeCAD_3D_Navigation_files/svg/Magic_Left_Click.svg"/></td>
</tr>
</table>

[Legend_Click_Drag]:FreeCAD_3D_Navigation_files/svg/Legend_Click_Drag.svg
[Legend_Click_Release]:FreeCAD_3D_Navigation_files/svg/Legend_Click_Release.svg
[Legend_ForceClick_Drag]:FreeCAD_3D_Navigation_files/svg/Legend_ForceClick_Drag.svg
[Legend_Swipe]:FreeCAD_3D_Navigation_files/svg/Legend_Swipe.svg

Legend: ![Legend_Swipe][] touch, ![Legend_Click_Release][] click-release, ![Legend_Click_Drag][] click-and-drag , ![Legend_ForceClick_Drag][] force-click-and-drag

> _Note: The feature concept of `select` does not apply to the actual OpenSCAD application 3D view._

**Apple vs Linux/Windows Navigation**

* A Linux/Windows keyboard `control-click` is an Apple keyboard `command-click`.
* May need to adjust `Preferences…` > `Display` > `3D View` > `Zoom step`
* CAD mode may need some Mission Control shortcuts to be disabled in System Preferences.
* Use <kbd>ctrl</kbd>|<kbd>cmd</kbd> multiple selections.

**General Navigation Settings**

* `Preferences…` > `Display` > `Navigation`
    * Orbit Style: `Trackball` (default) | `Turntable`
    * Rotation Mode: `Drag at cursor` (default) | _`Object center`_ | `Window center`
    * Disable touchscreen tilt gesture: checked (default) | _unchecked_
* The cursor icon may not update on the Real Thunder branch for pan and rotate.

## Keyboard Navigation <a id="keyboard-navigation-"></a><sup>[▴](#contents)</sup>

Keyboard navigation applies to to all modes.

<table class="mouseTableClass" style="width:100%; font-family: monospace;">
<tr>
    <th>Mode</th>
    <th>Pan ![Cursor_Pan](FreeCAD_3D_Navigation_files/png/Cursor_Pan.png)</th>
    <th>Rotate ![Cursor_Rotate](FreeCAD_3D_Navigation_files/png/Cursor_Rotate.png)</th>
    <th>Zoom ![Cursor_Zoom](FreeCAD_3D_Navigation_files/png/Cursor_Zoom.png)</th>
    <th>Select ![Cursor_Hand_Select](FreeCAD_3D_Navigation_files/png/Cursor_Hand_Select_16px.png)</th>
</tr>

<tr>
    <td>Keyboard</td>
    <td style="text-align: center;"><kbd>◀︎</kbd><kbd>▶︎</kbd> <kbd>▲</kbd><kbd>▼</kbd></td>
    <td style="text-align: center;"><kbd>shift</kbd><kbd>◀︎</kbd><br><kbd>shift</kbd><kbd>▶︎</kbd></td>
    <td style="text-align: center;"><kbd>ctrl</kbd><kbd>+</kbd>|<kbd>cmd</kbd><kbd>+</kbd><br><kbd>ctrl</kbd><kbd>-</kbd>|<kbd>cmd</kbd><kbd>-</kbd></td>
    <td style="text-align: center;"></td>
</tr>
</table>

* <kbd>◀︎</kbd> <kbd>▶︎</kbd> Pan View Left/Right. <kbd>▼</kbd> <kbd>▲</kbd> Pan View Up/Down
* <kbd>shift</kbd> <kbd>◀︎</kbd>, <kbd>shift</kbd> <kbd>▶︎</kbd> Rotate view 90°
* <kbd>ctrl</kbd><kbd>+</kbd>|<kbd>cmd</kbd><kbd>+</kbd> Zoom In, <kbd>ctrl</kbd><kbd>-</kbd>|<kbd>cmd</kbd><kbd>-</kbd> Zoom Out
* Number Keys: Select a standard camera view
    * <kbd>0</kbd> [Isometric](https://wiki.freecadweb.org/Std_ViewIsometric) <img src="FreeCAD_3D_Navigation_files/svg/Std_ViewIsometric.svg" width="24" height="24"/>
    * <kbd>1</kbd> [Front](https://wiki.freecadweb.org/Std_ViewFront) <img src="FreeCAD_3D_Navigation_files/svg/Std_ViewFront.svg" width="24" height="24"/>, 
    <kbd>2</kbd> [Top](https://wiki.freecadweb.org/Std_ViewTop) <img src="FreeCAD_3D_Navigation_files/svg/Std_ViewTop.svg" width="24" height="24"/>, 
    <kbd>3</kbd> [Right](https://wiki.freecadweb.org/Std_ViewRight) <img src="FreeCAD_3D_Navigation_files/svg/Std_ViewRight.svg" width="24" height="24"/>
    * <kbd>4</kbd> [Rear](https://wiki.freecadweb.org/Std_ViewRear) <img src="FreeCAD_3D_Navigation_files/svg/Std_ViewRear.svg" width="24" height="24"/>, 
    <kbd>5</kbd> [Bottom](https://wiki.freecadweb.org/Std_ViewBottom) <img src="FreeCAD_3D_Navigation_files/svg/Std_ViewBottom.svg" width="24" height="24"/>, 
    <kbd>6</kbd> [Left](https://wiki.freecadweb.org/Std_ViewLeft) <img src="FreeCAD_3D_Navigation_files/svg/Std_ViewLeft.svg" width="24" height="24"/>
* <kbd>V</kbd><kbd>O</kbd> Set camera View to [Orthographic](https://wiki.freecadweb.org/Std_OrthographicCamera) <img src="FreeCAD_3D_Navigation_files/svg/View-isometric.svg" width="24" height="24"/>
* <kbd>V</kbd><kbd>P</kbd> Set camera View to [Perspective](https://wiki.freecadweb.org/Std_PerspectiveCamera) <img src="FreeCAD_3D_Navigation_files/svg/View-perspective.svg" width="24" height="24"/>
* <kbd>ctrl</kbd>|<kbd>cmd</kbd> Allow additional objects or elements to be selected

These controls are also available from the [View menu](https://wiki.freecadweb.org/Std_View_Menu "Std View Menu") and some from the View toolbar.

## Navigation Cube <a id="navigation-cube-"></a><sup>[▴](#contents)</sup>

The Navigation Cube (aka Navigation Cluster) can rotate the view a fixed amount, reset to a standard view, and change the display view mode.

![](FreeCAD_3D_Navigation_files/png/NavigationCube.png)

* Click Corner, Edge or Face: rotate viewpoint so that corner, edge or face points outward
* Click Triangle: rotate view 45 degrees
* Click Curved Arrow: rotate view 45 degrees around a line outward from the view

Move the navigation cluster with a mouse left-click & drag from inside the cube itself.

Use the mini-cube menu for switching the viewing modes: Orthogonal, Perspective, Isometric.

Cube Preferences: `Preferences...` → `Display` → `Navigation` → `Navigation cube` 

* Size: 132 [default]

## Object Selection <a id="object-selection-"></a><sup>[▴](#contents)</sup>

Select

* `single-click` selects the object and one of its subcomponents (edge, face, vertex)
* `single-click`, `single-click`, ... selection cycles upwardly through related visible hierarchy
* `double-click` selects the object and all its subcomponents
* `ctrl-click` selects more than one subcomponent, or different subcomponents from different objects
* `click-empty-portion` will deselect everything in the 3D view

Deselect

* `single-click` outside of object in 3D viewing area

The _Selection View Panel_ (available from the View menu) shows what is currently selected. The Selection View is searchable.

![](FreeCAD_3D_Navigation_files/png/Selection_view.png)

## Tool Activation <a id="tool-activation-"></a><sup>[▴](#contents)</sup>

* activate: right-click tool icon
* deactivate: right-click unoccupied 3D viewing space, or press <kbd>esc</kbd> key

## Coordinate System <a id="coordinate-system-"></a><sup>[▴](#contents)</sup>

The FreeCAD 3D view is an [X, Y and Z euclidian space](https://en.wikipedia.org/wiki/Euclidean_space). In the default isometric orientation, the positive increasing X (horizon red) axis points to the right, the positive Y (meadow green) axis to the back, and the positive Z (sky blue) axis upwards.

![](FreeCAD_3D_Navigation_files/jpg/3dspace_coordinates.jpg)

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [Category: Manual(s) ⇗](https://wiki.freecadweb.org/Category:Manual "Category:Manual")

**FreeCAD Web Manual**

![](FreeCAD_3D_Navigation_files/png/FreeCAD_manual_128px.png)

* [Manual Introduction ⇗](https://wiki.freecadweb.org/Manual:Introduction) 
* [3D View Navigation ⇗](https://wiki.freecadweb.org/Manual:Navigating_in_the_3D_view)
    * [Mouse/Trackpad Navigation Modes ⇗](https://wiki.freecadweb.org/Mouse_navigation)
    * [Navigation Cube ⇗](https://wiki.freecadweb.org/Navigation_Cube) … aka Navigation Cluster
    * [New Mouse Navigation (C++ programming) ⇗](https://wiki.freecadweb.org/Adding_a_new_mouse_navigation_option_to_FreeCAD)
* [Document ⇗](https://wiki.freecadweb.org/Manual:The_FreeCAD_document)
* [Interface ⇗](https://wiki.freecadweb.org/Manual:The_FreeCAD_Interface)

**Hardware**

* [Apple Magic Mouse ⇗](https://www.apple.com/shop/product/MK2E3AM/A/magic-mouse)
* [Logitech ⇗](https://www.logitech.com/en-us/products/mice/mx-master-3.html)

**Other Info**

* [Maker Tales: CAD Style Navigation in FreeCAD ⇗](https://www.youtube.com/watch?v=Ir9PWoeqLZE) (video)
* [Selection methods ⇗](https://wiki.freecadweb.org/Selection_methods "Selection methods")
* [Std TransformManip ⇗](https://wiki.freecadweb.org/Std_TransformManip "Std TransformManip") tool to apply rotation increments or translation increments to an object.